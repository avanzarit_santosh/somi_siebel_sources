package som.treas.ivr.integration;

import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import miecc.ivrtest.client.IvrTestRunner;
import miecc.ivrtest.core.TestScript;
import miecc.ivrtest.core.TestStepStatus;
import org.apache.log4j.Logger;
import org.junit.experimental.categories.Category;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import static miecc.ivrtest.core.IvrTestConfiguration.GSON;
import static miecc.ivrtest.core.IvrTestConfiguration.GSON_PRETTY;
import static org.assertj.core.api.Java6Assertions.assertThat;

@Category({IntegrationTest.class, IvrTest.class})
public class IvrTest {

    private static final Logger logger = Logger.getLogger(IvrTest.class);

    public void testMainMenu() {
        String dialNumber = System.getenv("PHONE_NUMBER");
        String ivrTestUrl = System.getenv("IVRTEST_URL");

        assertThat(dialNumber).isNotBlank();
        assertThat(ivrTestUrl).isNotBlank();

        TestScript testScript = Observable.just(IvrTest.class.getResourceAsStream("/ivrtest/main-menu-test.json"))
                .map(new Function<InputStream, Reader>() {
                    @Override
                    public Reader apply(InputStream inputStream) throws Exception {
                        return new InputStreamReader(inputStream);
                    }
                })
                .map(new Function<Reader, TestScript>() {

                    @Override
                    public TestScript apply(Reader reader) throws Exception {
                        return GSON.fromJson(reader, TestScript.class);
                    }
                })
                .blockingSingle();

        assertThat(testScript).isNotNull();

        TestStepStatus status = IvrTestRunner.newBuilder()
                .withServerEndpoint(ivrTestUrl)
                .withDialNumber(dialNumber)
                .withTestScript(testScript)
                .build()
                .runRealtimeObservable()
                .doOnEach(new Consumer<Notification<TestStepStatus>>() {
                    @Override
                    public void accept(Notification<TestStepStatus> notification) throws Exception {
                        logger.info(GSON_PRETTY.toJson(notification.getValue()));
                    }
                })
                .blockingLast();


        assertThat(status.isLast()).isTrue();
        assertThat(status.isSuccess()).isTrue();
    }
}

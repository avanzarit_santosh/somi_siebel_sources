package som.treas.ivr.integration;

import org.apache.commons.lang.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

import static org.assertj.core.api.Assertions.assertThat;

@Category({IntegrationTest.class, VxmlTest.class})
public class VxmlTest {

    private static String serverUrl;
    private static RestTemplate restTemplate;

    @BeforeClass
    public static void setup() {

        String tomcatUrl = System.getenv("TOMCAT_URL");
        String tomcatPath = System.getenv("TOMCAT_PATH");
        serverUrl = StringUtils.substringBefore(tomcatUrl, "/manager")
                .concat(tomcatPath);

        restTemplate = new RestTemplateBuilder()
                .rootUri(serverUrl)
                .build();

    }

    @Test
    public void testJSPPages() throws ParserConfigurationException, IOException, SAXException {
        String[] pages = new String[]{
                "START.jsp"
                //,"AdminInfoPlayRecorded.jsp"
                ,"AdminPassCdInput.jsp"
                //,"AdminPlayRecordedEm.jsp"
                //,"AdminPlayRecordedSpMsg.jsp"
                ,"AuthenticateReportData.jsp"
                ,"AuthReportData.jsp"
                ,"AuthRoutine.jsp"
                ,"EstListPymnts.jsp"
                ,"EstPaymentReportData.jsp"
                ,"EstPymntReportData.jsp"
                ,"FormOrderProcess.jsp"
                ,"FormRequestVerify.jsp"
                ,"InitIVR.jsp"
                ,"OrderFormReportData.jsp"
                ,"OrderFormsReportsData.jsp"
                ,"OthrCSReportData.jsp"
                ,"OthrOptCorrEstDt.jsp"
                ,"OthrOptCorrMenuVerify.jsp"
                ,"OthrOptendMenuVerify.jsp"
                //,"OthrOptProcCorr.jsp"
                ,"OthrOptSetCorrDate.jsp"
                ,"OthrOptSetOOCallPath.jsp"
                ,"OthrSetCSCallPath.jsp"
                ,"OthrSetTACallPath.jsp"
                ,"OthrSetTPCallPath.jsp"
                ,"OthrTAReportData.jsp"
                ,"OthrTPReportData.jsp"
                ,"PlaySpecialMsg.jsp"
                ,"PlaySpecialMsg2.jsp"
                ,"ReturnGetData.jsp"
                ,"ReturnInfoInit.jsp"
                ,"ReturnInfoReportData.jsp"
                //,"ReturnSetResponse.jsp"
                ,"RtnDateValidate.jsp"
                ,"RtnFileDateSwitch.jsp"
                ,"RtnFileDateValidate.jsp"
                ,"RtnInfoEndMenuVerify.jsp"
                //,"RtnParseDate.jsp"
                ,"SetArabic.jsp"
                ,"SetEnglish.jsp"
                ,"SetSpanish.jsp"
                ,"STOP.jsp"
                ,"SystemDown.jsp"
                ,"TaxReturnReportsData.jsp"
                ,"TransferProcess.jsp"
                ,"TransferReportData.jsp"
                ,"TrnsfrReportData.jsp"
                ,"TrnsfrSetSsnValid.jsp"
                ,"ValidateYearInput.jsp"
                ,"VerifyReturnListMenuChoice.jsp"
                ,"YearValidate.jsp"
        };

        for(String page : pages) {

            System.out.println("validating page " + page);
            String vxml = restTemplate.getForObject("/" + page, String.class);

            assertThat(vxml).isNotBlank();

            Document doc = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new StringReader(vxml)));

            assertThat(doc).isNotNull();
            assertThat(doc.getDocumentElement().getTagName())
                    .isIn("vxml", "XMLPage");

        }
    }
}


package som.treas.ivr.unit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gov.mi.state.treas.ivr.IvrUtils;
import gov.mi.state.treas.ivr.ReturnAssetProxy;
import gov.mi.state.treas.ivr.TreasWebServiceException;
import gov.mi.state.treas.ivr.ws.asset.AssetMgmtAsset;
import gov.mi.state.treas.ivr.ws.asset.MIIvrAssetManagementQueryByExampleInput;
import gov.mi.state.treas.ivr.ws.asset.MIIvrAssetManagementQueryByExampleOutput;
import gov.mi.state.treas.ivr.ws.asset.TransactionAssetMgmtAsset;
import org.junit.experimental.categories.Category;

import javax.xml.bind.JAXBException;
import java.util.*;


@Category({UnitTest.class})
public class EstPaymentTest {

    private final String endPoint;
    private final String account;
    private final String taxYear;

    private final ReturnAssetProxy assetProxy;
    private final Gson gson;



    public void test() throws TreasWebServiceException {

        MIIvrAssetManagementQueryByExampleInput input = assetProxy.createRequest();
        input.getListOfMiIvrAssetManagement().getAssetMgmtAsset().get(0).setAccountName(account);

        MIIvrAssetManagementQueryByExampleOutput output = assetProxy.sendRequest(input);

        assert output != null;

        String voxFileDir = "/Voxfiles/en-US";
        String addStuff = IvrUtils.getVXMLScript();


        AssetMgmtAsset assetForYear = null;
        for(AssetMgmtAsset asset : output.getListOfMiIvrAssetManagement().getAssetMgmtAsset()) {
            if(taxYear.equals(asset.getTaxYear())) {
                assetForYear = asset;
                break;
            }
        }

        List<TransactionAssetMgmtAsset> paymentList = new ArrayList<TransactionAssetMgmtAsset>();

        for(TransactionAssetMgmtAsset transaction : assetForYear.getListOfTransactionAssetMgmtAsset().getTransactionAssetMgmtAsset()) {
            System.out.println(gson.toJson(transaction));

            if("Estimate Payment".equals(transaction.getName()) || "Extension".equals(transaction.getName())) {
                paymentList.add(transaction);
            }
            else if("Credit Forward".equals(transaction.getName())) {
                float val = Float.parseFloat(transaction.getAssetValue2());
                if(val > 0.001f) {
                    paymentList.add(transaction);
                }
            }
        }

        if(paymentList.size() > 0) {

            Collections.sort(paymentList, new Comparator<TransactionAssetMgmtAsset>() {
                @Override
                public int compare(TransactionAssetMgmtAsset t1, TransactionAssetMgmtAsset t2) {
                    Date d1 = IvrUtils.parseDate(t1.getInstallDate());
                    Date d2 = IvrUtils.parseDate(t2.getInstallDate());
                    if(d1 == null)
                        return -1;
                    else if(d2 == null)
                        return 1;
                    else
                        return d1.compareTo(d2);
                }
            });

            for(TransactionAssetMgmtAsset transaction : paymentList) {
                if("Void".equals(transaction.getStatus())) {
                    continue;
                }

                if("Estimate Payment".equals(transaction.getName()) || "Extenstion".equals(transaction.getName())) {
                    addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/505_EstPymt_Date_Amt01.vox");
                    addStuff += IvrUtils.getVXMLCurrency(transaction.getAssetValue2());
                    addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/505_EstPymt_Date_Amt02.vox");
                    addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(transaction.getInstallDate()));
                }
                else if("Credit Forward".equals(transaction.getName())) {

                    addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/506_Credit_Forward01.vox");
                    addStuff += IvrUtils.getVXMLCurrency(transaction.getAssetValue2());
                    addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/506_Credit_Forward02.vox");
                    addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(transaction.getInstallDate()));
                }
            }
        }
        else {
            //play no payment

        }

        System.out.println(addStuff);
    }

    public static void main(String... args) throws Exception {
//        EstPaymentTest.newBuilder()
//                .withEndPoint("http://10.42.58.20/eai_enu/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&Username=IVRIIT&Password=IVRIIT")
//                .withAccount("")
//                .withTaxYear("")
//                .build()
//                .test();
    }

    private EstPaymentTest(Builder builder) throws JAXBException {
        endPoint = builder.endPoint;
        account = builder.account;
        taxYear = builder.taxYear;

        gson = new GsonBuilder().setPrettyPrinting().create();

        assetProxy = new ReturnAssetProxy(endPoint);
    }


    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(EstPaymentTest copy) {
        Builder builder = new Builder();
        builder.endPoint = copy.endPoint;
        builder.account = copy.account;
        builder.taxYear = copy.taxYear;
        return builder;
    }


    public static final class Builder {
        private String endPoint;
        private String account;
        private String taxYear;

        private Builder() {
        }

        public Builder withEndPoint(String val) {
            endPoint = val;
            return this;
        }

        public Builder withAccount(String val) {
            account = val;
            return this;
        }

        public Builder withTaxYear(String val) {
            taxYear = val;
            return this;
        }

        public EstPaymentTest build() throws JAXBException {
            return new EstPaymentTest(this);
        }
    }
}

package som.treas.ivr.unit;

import com.google.gson.Gson;
import gov.mi.state.treas.ivr.ReturnAssetProxy;
import org.junit.experimental.categories.Category;


@Category({UnitTest.class})
public class WSDLTest {

    private static Gson gson;
    private static String endpoint;
    private static ReturnAssetProxy assetProxy;


    //TODO
//    @BeforeClass
//    public static void init() throws JAXBException {
//        gson = new GsonBuilder().setPrettyPrinting().create();
//        String prodEndpoint = "http://10.42.58.20/eai_enu/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&Username=IVRIIT&Password=IVRIIT";
//        endpoint = "http://hcs271seblta903.som.ad.state.mi.us/eai_enu/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&Username=IVRIIT&Password=IVRIIT";
//
//        endpoint = "http://S6084BVL3PD2:8088/mockMIIvrAssetManagement";
//        assetProxy = new ReturnAssetProxy(endpoint);
//    }
//
//    @Test
//    public void testAssetProxy() throws Exception {
//        MIIvrAssetManagementQueryByExampleOutput output = null;
//        final String ssn = "123456789";
//
//        MIIvrAssetManagementQueryByExampleInput input = assetProxy.createRequest();
//        input.getListOfMiIvrAssetManagement()
//                .getAssetMgmtAsset()
//                .get(0)
//                .setAccountName(ssn);
//
//        //input.getListOfMiIvrAssetManagement().getAssetMgmtAsset().get(0).setComments("2013");
//
//
//
//        System.out.println(gson.toJson(input));
//
//        output = assetProxy.sendRequest(input);
//
//        assert output != null;
//
//        System.out.println(gson.toJson(output));
//    }
//
//    @Test
//    public void testEstimatedPayments() throws Exception{
//        final String ssn = "123456789";
//
//        MIIvrAssetManagementQueryByExampleInput input = assetProxy.createRequest();
//        input.getListOfMiIvrAssetManagement().getAssetMgmtAsset().get(0).setAccountName(ssn);
//        //input.getListOfMiIvrAssetManagement().getAssetMgmtAsset().get(0).setComments("2013");
//
//
//        System.out.println(gson.toJson(input));
//        MIIvrAssetManagementQueryByExampleOutput output = assetProxy.sendRequest(input);
//
//        assert output != null;
//
//        String voxFileDir = "/Voxfiles/en-US";
//        String addStuff = IvrUtils.getVXMLScript();
//
//
//        AssetMgmtAsset assetForYear = null;
//        for(AssetMgmtAsset asset : output.getListOfMiIvrAssetManagement().getAssetMgmtAsset()) {
//            if("IIT2013".equals(asset.getTaxYear())) {
//                assetForYear = asset;
//                break;
//            }
//        }
//
//        List<TransactionAssetMgmtAsset> paymentList = new ArrayList<TransactionAssetMgmtAsset>();
//
//        if(assetForYear != null) {
//            for (TransactionAssetMgmtAsset transaction : assetForYear.getListOfTransactionAssetMgmtAsset().getTransactionAssetMgmtAsset()) {
//                System.out.println(gson.toJson(transaction));
//                if ("Estimate Payment".equals(transaction.getName()) || "Extension".equals(transaction.getName())) {
//                    paymentList.add(transaction);
//                } else if ("Credit Forward".equals(transaction.getName())) {
//                    float val = Float.parseFloat(transaction.getAssetValue2());
//                    if (val > 0.001f) {
//                        paymentList.add(transaction);
//                    }
//                }
//            }
//        }
//
//        if(paymentList.size() > 0) {
//
//            Collections.sort(paymentList, new Comparator<TransactionAssetMgmtAsset>() {
//                @Override
//                public int compare(TransactionAssetMgmtAsset t1, TransactionAssetMgmtAsset t2) {
//                    Date d1 = IvrUtils.parseDate(t1.getInstallDate());
//                    Date d2 = IvrUtils.parseDate(t2.getInstallDate());
//                    if(d1 == null)
//                        return -1;
//                    else if(d2 == null)
//                        return 1;
//                    else
//                        return d1.compareTo(d2);
//                }
//            });
//
//            for(TransactionAssetMgmtAsset transaction : paymentList) {
//                if("Estimate Payment".equals(transaction.getName()) || "Extension".equals(transaction.getName())) {
//                    addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/505_EstPymt_Date_Amt01.vox");
//                    addStuff += IvrUtils.getVXMLCurrency(transaction.getAssetValue2());
//                    addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/505_EstPymt_Date_Amt02.vox");
//                    addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(transaction.getInstallDate()));
//                }
//                else if("Credit Forward".equals(transaction.getName())) {
//
//                    addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/506_Credit_Forward01.vox");
//                    addStuff += IvrUtils.getVXMLCurrency(transaction.getAssetValue2());
//                    addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/506_Credit_Forward02.vox");
//                    addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(transaction.getInstallDate()));
//                }
//            }
//        }
//        else {
//            //play no payment
//
//        }
//
//        System.out.println(addStuff);
//    }
}

//
// Generated By:JAX-WS RI IBM 2.1.1 in JDK 6 (JAXB RI IBM JAXB 2.1.3 in JDK 1.6)
//


package gov.mi.state.treas.ivr.ws.error;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.mi.state.treas.ivr.ws.error package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListOfMiIvrErrorcodeReturnDate_QNAME = new QName("http://www.siebel.com/xml/MI%20IVR%20ErrorCode%20Return%20Date", "ListOfMiIvrErrorcodeReturnDate");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.mi.state.treas.ivr.ws.error
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MIIvrErrorCodeReturnDateQueryByExampleOutput }
     * 
     */
    public MIIvrErrorCodeReturnDateQueryByExampleOutput createMIIvrErrorCodeReturnDateQueryByExampleOutput() {
        return new MIIvrErrorCodeReturnDateQueryByExampleOutput();
    }

    /**
     * Create an instance of {@link MIIvrErrorCodeReturnDateQueryByExampleInput }
     * 
     */
    public MIIvrErrorCodeReturnDateQueryByExampleInput createMIIvrErrorCodeReturnDateQueryByExampleInput() {
        return new MIIvrErrorCodeReturnDateQueryByExampleInput();
    }

    /**
     * Create an instance of {@link ListOfMiIvrErrorcodeReturnDateTopElmt }
     * 
     */
    public ListOfMiIvrErrorcodeReturnDateTopElmt createListOfMiIvrErrorcodeReturnDateTopElmt() {
        return new ListOfMiIvrErrorcodeReturnDateTopElmt();
    }

    /**
     * Create an instance of {@link ListOfMiIvrErrorcodeReturnDate }
     * 
     */
    public ListOfMiIvrErrorcodeReturnDate createListOfMiIvrErrorcodeReturnDate() {
        return new ListOfMiIvrErrorcodeReturnDate();
    }

    /**
     * Create an instance of {@link MiIitCompletionDate }
     * 
     */
    public MiIitCompletionDate createMiIitCompletionDate() {
        return new MiIitCompletionDate();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOfMiIvrErrorcodeReturnDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.siebel.com/xml/MI%20IVR%20ErrorCode%20Return%20Date", name = "ListOfMiIvrErrorcodeReturnDate")
    public JAXBElement<ListOfMiIvrErrorcodeReturnDate> createListOfMiIvrErrorcodeReturnDate(ListOfMiIvrErrorcodeReturnDate value) {
        return new JAXBElement<ListOfMiIvrErrorcodeReturnDate>(_ListOfMiIvrErrorcodeReturnDate_QNAME, ListOfMiIvrErrorcodeReturnDate.class, null, value);
    }

}

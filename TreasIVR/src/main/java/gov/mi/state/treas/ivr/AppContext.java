package gov.mi.state.treas.ivr;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gov.mi.state.treas.ivr.config.IvrConfigDAO;
import gov.mi.state.treas.ivr.config.IvrConfigDAOImpl;
import gov.mi.state.treas.ivr.config.IvrConfigItem;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static org.springframework.jdbc.datasource.init.ScriptUtils.*;

public enum AppContext {

	INSTANCE;

    final public static Logger logger = Logger.getLogger(AppContext.class);

    public static final String IVR_NAME = "TREAS_IIT_IVR";

	final Properties properties;
	final IvrConfigDAO ivrConfigDAO;

    final Set<String> passcodeSet;

    final Gson gson;

    final JdbcTemplate jdbcTemplate;


	private final Cache<String, Object> cache = CacheBuilder.newBuilder()
			.expireAfterWrite(1, TimeUnit.MINUTES)
			.build();

	private AppContext() {
		this.properties = new Properties();
		try {
			InputStream in = getClass().getResourceAsStream("/application.properties");
			properties.load(in);
			in.close();

            BasicDataSource ds = new BasicDataSource();
            ds.setDriverClassName(properties.getProperty("admin.db.driver"));
            ds.setUrl(properties.getProperty("admin.db.url"));
            ds.setUsername(properties.getProperty("admin.db.user"));
            ds.setPassword(properties.getProperty("admin.db.pass"));


            jdbcTemplate = new JdbcTemplate(ds);
            ivrConfigDAO = new IvrConfigDAOImpl(jdbcTemplate);

            String passcodes = properties.getProperty("admin.passcodes");
            passcodeSet = new ImmutableSet.Builder<String>()
                    .addAll(Arrays.asList(StringUtils.split(passcodes, ",")))
                    .build();

            gson = new GsonBuilder().setPrettyPrinting().create();

            executeSqlScript(ds.getConnection()
                    , new EncodedResource(new InputStreamResource(this.getClass().getResourceAsStream("/db/init.sql")), Charset.defaultCharset())
                    , false, false
                    , DEFAULT_COMMENT_PREFIX
                    , "/"
                    , DEFAULT_BLOCK_COMMENT_START_DELIMITER
                    , DEFAULT_BLOCK_COMMENT_END_DELIMITER
            );

		} catch (IOException e) {
			e.printStackTrace();
			throw new IllegalArgumentException(e);
		} catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException(e);
        }
    }

	public Properties getProperties() {
		return properties;
	}

    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    public IvrConfigDAO getIvrConfigDAO() {
        return ivrConfigDAO;
    }


    public static final String DEFAULT_VRP = "IIT_VRP";

    public Gson getGson() {
        return gson;
    }

    public String getVrpFromHost(String host) {

        String name = StringUtils.substringBefore(StringUtils.lowerCase(host), ".");
        String key = "vrp." + StringUtils.defaultIfBlank(name, "default");
        String vrp = properties.getProperty(key);

        return StringUtils.defaultIfBlank(vrp, DEFAULT_VRP);
    }

    public List<IvrConfigItem> getAllConfigItems() {
        String cacheKey = "allconfigitems";

        List<IvrConfigItem> configItems = (List<IvrConfigItem>)cache.getIfPresent(cacheKey);
        if(configItems != null) {
            return configItems;
        }

        configItems = ivrConfigDAO.getAllConfigItems();
        cache.put(cacheKey, configItems);
        return ivrConfigDAO.getAllConfigItems();
    }

    public IvrConfigItem getConfigItem(final String name) {
        String cacheKey = "configitem." + name;
        IvrConfigItem configItem = (IvrConfigItem)cache.getIfPresent(cacheKey);
        if(configItem != null) {
            return configItem;
        }

        configItem = ivrConfigDAO.getConfigItem(name);
        if(configItem != null) {
            cache.put(cacheKey, configItem);
        }
        return configItem;
    }

    public void putConfigItem(final String name, final IvrConfigItem configItem) {
        String cacheKey = "configitem." + name;

        int count = ivrConfigDAO.updateConfigItem(name, configItem);
        if(count == 0) {
            ivrConfigDAO.insertConfigItem(name, configItem);
        }
        cache.put(cacheKey, configItem);
        cache.invalidate("allconfigitems");
        if("EMRG".equals(name) || "CLOSE".equals(name)) {
            IvrConfigItem bizConfigItem = IvrConfigItem.newBuilder(configItem)
                    .withIvrName("TREAS_BIZ_IVR")
                    .build();
            count = ivrConfigDAO.updateConfigItem(name, bizConfigItem);
            if(count == 0) {
                ivrConfigDAO.insertConfigItem(name, bizConfigItem);
            }
        }
    }

    public boolean authPasscode(String passcode) {
        return passcodeSet.contains(passcode);
    }

}

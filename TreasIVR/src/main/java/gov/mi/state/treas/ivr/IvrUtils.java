package gov.mi.state.treas.ivr;

import gov.mi.state.treas.ivr.config.TreasIVRConfig;
import gov.mi.state.treas.ivr.config.TreasIVRConfig.Databases.Database;
import gov.mi.state.treas.ivr.config.TreasIVRConfig.WebServices.WebService;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.servlet.ServletContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

public class IvrUtils {

	final public static DateFormat IVR_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	final public static DateFormat IVR_DATE_FORMAT_SHORT = new SimpleDateFormat("MM/dd/yyyy");

	public static final java.lang.String CONNECT_TIMEOUT = "com.sun.xml.internal.ws.connect.timeout";
	public static final java.lang.String REQUEST_TIMEOUT = "com.sun.xml.internal.ws.request.timeout";


	public static Node getXpathNode(Node context, String expr) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			return (Node) xpath.evaluate(expr, context, XPathConstants.NODE);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public static void setXpathValue(Node context, String expr, String val) {
		Node node = getXpathNode(context, expr);
		if(node != null) {
			while(node.hasChildNodes()) {
				node.removeChild(node.getFirstChild());
			}
			node.appendChild(node.getOwnerDocument().createTextNode(val));
		}
	}

	public static Object evalXpath(Node context, String expr, QName qName) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			return xpath.evaluate(expr, context, qName);
		} catch (XPathExpressionException e) {
			//e.printStackTrace();
			//System.out.println("Xpath error: " + context + " " + expr);
		}

		return null;
	}

	public static String getXpathValue(Node context, String expr) {
		XPath xpath = XPathFactory.newInstance().newXPath();
		try {
			return (String) xpath.evaluate(expr, context, XPathConstants.STRING);
		} catch (XPathExpressionException e) {
			//e.printStackTrace();
			//System.out.println("Xpath error: " + context + " " + expr);
		}

		return null;
	}

	public static int parseInt(String str) {
		try {
			return Integer.parseInt(str);
		}catch(Exception e) {
			return 0;
		}
	}

	public static String getDateString(Date date) {
		if(date == null)
			return null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);

		return String.valueOf(cal.get(Calendar.YEAR)) +
				(month < 10 ? "0" + String.valueOf(month) : String.valueOf(month)) +
				(day < 10 ? "0" + String.valueOf(day) : String.valueOf(day));
	}

	public static Date parseDate(String str) {
		try {
			return new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(str);
		}
		catch(Exception e) {
			return null;
		}
	}

	public static Date parseDate(String str, Date defaultDate) {
		try {
			return new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(str);
		}
		catch(Exception e) {
			return defaultDate;
		}
	}

	public static Date parseDate(String str, String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		try {
			return dateFormat.parse(str);
		}
		catch(Exception e) {
			return null;
		}
	}

	public static Date parseDate(String str, String format, Date defaultDate) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		try {
			return dateFormat.parse(str);
		}
		catch(Exception e) {
			return defaultDate;
		}
	}

	public static String nodeToString(Node node) {
		try {
			StringWriter sw = new StringWriter();
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.transform(new DOMSource(node), new StreamResult(sw));
			return sw.toString();
		} catch(Exception e) {
			return null;
		}
	}

	public static String getVXMLScript() {
		return "";
	}

	public static String getVXMLPrompt(String voxfile) {
		return "{\"type\":\"audio\", \"value\":'" + voxfile + "'}|";
	}


	public static String getVXMLDate(Date date) {
		return "{\"type\":\"date\", \"value\":'" + getDateString(date) + "'}|";
	}

	public static String getVXMLNumber(String number) {
		int num = Math.abs(parseInt(number));
		return "{\"type\":\"number\", \"value\":'" + num + "'}|";
	}

	public static String getTimeString(String numMinutes) {
		String result;

		try {
			int mins = Integer.parseInt(numMinutes);
			int hrs = mins / 60;
			mins = mins % 60;
			if(hrs > 9) {
				result = String.valueOf(hrs);
			}
			else if(hrs > 0){
				result = "0" + String.valueOf(hrs);
			}
			else {
				result = "00";
			}
			if(mins > 9) {
				result += String.valueOf(mins);
			}
			else if(mins > 0){
				result += "0" + String.valueOf(mins);
			}
			else {
				result += "00";
			}
			return result;

		}catch(Exception e) {
			return "0002";
		}
	}

	public static String getVXMLCurrency(String amount) {
		int amt = Math.abs(parseInt(amount));
		return "{\"type\":\"currency\", \"value\":'" + amt + "'}|";
	}

	public static String getVXMLCurrency(int amount) {
		return getVXMLCurrency(amount);
	}


	static public String addPrefix(String path) {
		StringTokenizer st = new StringTokenizer(path, "/", true);
		String result = "";
		String prefix = "mi";
		while(st.hasMoreTokens()) {
			String token = st.nextToken();
			if("/".equals(token)) {
				result += token;
			}
			else if(token.contains(":")){
				result += token;
			}
			else {
				result += prefix + ":" + token;
			}
		}

		return result;
	}


	final static Map<String, TreasIVRConfig> configMap = new HashMap<String, TreasIVRConfig>();
	final static Map<String, Long> configTimeMap = new HashMap<String, Long>();

	static String configPath;

	static public synchronized TreasIVRConfig getConfigInstance(String path) throws IvrException {
		configPath = path;
		TreasIVRConfig config = configMap.get(path);
		Long configFileTime = configTimeMap.get(path);
		File configFile = new File(path);

		if( configFileTime != null
				&& configFile != null
				&& configFileTime >= configFile.lastModified() ) {
			return config;
		}
		else {
			try {
				JAXBContext jc = JAXBContext.newInstance("gov.mi.state.treas.ivr.config");
				Unmarshaller u = jc.createUnmarshaller();
				config = (TreasIVRConfig) u.unmarshal(configFile);
				// put config into map
				if(config != null) {
					configMap.put(path, config);
					configTimeMap.put(path, configFile.lastModified());
				}
				return config;
			} catch (Exception e) {
				e.printStackTrace();
				throw new IvrException(e);
			}
		}
	}

	static public TreasIVRConfig getConfigInstance() throws IvrException {
		return getConfigInstance(configPath);
	}

	static public Database getDatabaseConfig(TreasIVRConfig config, String name) {
		try {
			List<Database> dbs = config.getDatabases().getDatabase();
			Iterator<Database> itor = dbs.iterator();
			while(itor.hasNext()) {
				Database db = itor.next();
				if(name.equalsIgnoreCase(db.getName())) {
					return db;
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	static public WebService getWebServiceConfig(TreasIVRConfig config, String name) {
		try {
			List<WebService> services = config.getWebServices().getWebService();
			Iterator<WebService> itor = services.iterator();
			while(itor.hasNext()) {
				WebService service = itor.next();
				if(name.equalsIgnoreCase(service.getName())) {
					return service;
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	static public Connection getConnection(String uri, String user, String password) {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			return DriverManager.getConnection(uri, user, password);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	static public Node removeNameSpace(Node node) {
		try {
			StringWriter inputSw = new StringWriter();
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.transform(new DOMSource(node), new StreamResult(inputSw));


			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = null;

			doc= db.parse(new InputSource(new StringReader(inputSw.toString())));
			return doc;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	static public String callPathAppend(String path, String node) {
		if(path == null || "".equals(path)) {
			return node;
		}
		else if(!path.contains(":")) {
			return path + ":" + node;
		}
		StringTokenizer st = new StringTokenizer(path, ":");
		String token = null;
		StringBuffer resultPath = new StringBuffer();
		resultPath.append(st.nextToken());
		while(st.hasMoreTokens()) {
			token = st.nextToken();
			resultPath.append(":").append(token);
			if(token.equals(node)) {
				return resultPath.toString();
			}
		}
		resultPath.append(":").append(node);

		return resultPath.toString();
	}

	static public String appendCallPath(String path, String node) {
		return callPathAppend(path, node);
	}

	static public String trimCallPath(String path, String node) {
		String result = appendCallPath(path, node);
		if(result != null && result.contains(":")) {
			result = result.substring(0, result.lastIndexOf(":"));
		}
		return result;
	}

	static public boolean isEmpty(String str) {
		return (str == null || "".equals(str));
	}

	static public String getEmergencyMessage(ServletContext sc, String voxFileDir,
											 String voxFile, String defaultVox) throws IvrException {

		Connection con = null;
		String addStuff = "";
		String voxPath = voxFileDir + "/" + voxFile;
		String realPath = sc.getRealPath(voxPath);

		try {
			TreasIVRConfig config = IvrUtils.getConfigInstance();
			System.out.println(config);
			TreasIVRConfig.Databases.Database db =
					IvrUtils.getDatabaseConfig(config, "TreasIvrAdmin");
			System.out.println(db);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection(db.getConnectionString(), db.getUser(), db.getPassword());
			PreparedStatement s=con.prepareStatement("select emergency_msg,"
					+ " emergency_msg_updated_date "
					+ " from treas_ivr_admin where ivr_name='TREAS_IIT_IVR'");

			ResultSet rs = s.executeQuery();
			if(rs.next()) {
				// write data from blob to file
				Blob blob = rs.getBlob(1);
				java.util.Date bt = (java.util.Date)rs.getTimestamp(2);
				System.out.println("blob date: " + bt + " " + bt.getTime());

				File file = new File(realPath);
				java.util.Date fd = new java.util.Date(file.lastModified());
				System.out.println("file date: " + fd + " " + fd.getTime());

				if(bt == null || fd == null || bt.after(fd)) {
					InputStream is = blob.getBinaryStream();
					OutputStream os = new FileOutputStream(file);

					int len = 0;
					byte[] buf = new byte[1024];
					while((len = is.read(buf)) != -1) {
						os.write(buf, 0, len);
					}

					is.close();
					os.close();
				}

			}
			else {
				addStuff = IvrUtils.getVXMLPrompt(voxFileDir + "/" + defaultVox);
			}

			addStuff = IvrUtils.getVXMLPrompt(voxPath);

		}catch(Exception e) {
			throw new IvrException(e);
		}finally {
			try {con.close();} catch(Exception e) {};
		}

		return addStuff;

	}

	public static String getCurrentTaxYear() {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		return "IIT" + Integer.toString(year - 1);
	}
}

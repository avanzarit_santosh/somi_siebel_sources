/**
 * 
 */
package gov.mi.state.treas.ivr;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;

/**
 * @author NguyenH10
 *
 */
public class DOMObject {

	
	protected Node node;

	public DOMObject(Document doc) {
		this.node = doc;
	}
	
	public DOMObject(Node node) {
		this.node = node;
	}
	
	public DOMObject() {	
	}
	
	public DOMObject(InputStream is) throws Exception {
		parseXml(is);
	}
	
	public DOMObject(InputSource source) throws Exception {
		Document document;
		DocumentBuilder builder;			
		builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		document = builder.parse(source);
		this.node = document;
	}
			
	public String getXpathValue(String expr) {
		return IvrUtils.getXpathValue(node, expr);
	}
	
	public void setXpathValue(String expr, String val) {
		IvrUtils.setXpathValue(node, expr, val);
	}
	
	public Node getXpathNode(String expr) {
		return IvrUtils.getXpathNode(this.node, expr);
	}
	
	public Object evalXpath(String expr, QName qName) {
		return IvrUtils.evalXpath(this.node, expr, qName);
	}
	
	protected void setDocument(Document document) {
		this.node = document;
	}

	public Node getNode() {
		return this.node;
	}
	
	public String toString() {
		return IvrUtils.nodeToString(node);
	}
	
	
	public void parseXml(InputStream is) throws Exception {
		DocumentBuilder builder;			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(false);
			builder = dbf.newDocumentBuilder();
			node = builder.parse(is);
	}
	

}

/**
 * 
 */
package gov.mi.state.treas.ivr;

/**
 * @author NguyenH10
 *
 */
public class IvrConstants {
	final public static String CONTACT_REASON = "ContactReason";
	final public static String CONTACT_RESOLUTION = "ContactResolution";
	final public static String CALL_PATH = "CallPath";
	final public static String VAR_REPORT_RESULT = "ReportItem_Application Result";
	final public static String VAR_REPORT_RESULT_REASON = "ReportItem_Application Result Reason";
	
	
	public class CallPathConstants {
		final public static String MAIN_IIT = "IIT";
		final public static String RESPONSE_1040 = "R";
		final public static String RESPONSE_1040X = "RX";
		final public static String RESPONSE_1040CR = "RCR";
		final public static String RESPONSE_1040CR7 = "RCR7";

		final public static String RESPONSE_ADDITIONAL = "RA";
		final public static String RETURN_STATUS = "RS";
		final public static String RETURN_PROCESSING_TIME = "RPT";
		final public static String PAPER_RETURN_NOT_PROCESSED = "RNP";
		final public static String EFILE_RETURN_NOT_PROCESSED = "RNE";
		
		final public static String ORDER_FORMS = "OF";
		final public static String ESTIMATE_PAYMENTS = "EP";
		
		final public static String TRANSFER_CALL = "TC";
		final public static String SPANISH_CALL = "SC";
		final public static String EXECUTIVE_CALL = "EO";
		
		final public static String QUEUE_CREDIT = "QC";
		final public static String QUEUE_SPANISH = "QS";
		final public static String QUEUE_ARABIC = "QA";
		final public static String QUEUE_EXECUTIVE = "QE";
		final public static String QUEUE_DEFAULT = "QD";
		final public static String QUEUE_FORCE_DISCONNECT = "QFD";
		final public static String QUEUE_DEFAULT_DISCONNECT = "QDD";
		final public static String QUEUE_CREDIT_DISCONNECT = "QCD";
		
		final public static String OTHER_OPTION = "OO";
		final public static String TAX_PREPARATION = "TP";
		final public static String TAX_ASSESSMENT = "TA";
		final public static String CORE_STATUS = "CS";
		final public static String CORE_FOUND = "CF";
		final public static String CORE_NOT_FOUND = "CNF";
		
		final public static String AUTH_ROUTINE_CHECK = "ARC";
		final public static String ADMIN_MODULE = "AM";
		final public static String CRM_DOWN = "CRMD";
	}
	
	public class ContactLogConstants {
		
	}
}

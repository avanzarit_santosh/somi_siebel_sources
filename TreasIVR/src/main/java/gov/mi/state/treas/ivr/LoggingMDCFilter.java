package gov.mi.state.treas.ivr;

import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class LoggingMDCFilter implements Filter {
    private static final Logger logger = Logger.getLogger(LoggingMDCFilter.class);

    private static final Gson gson = new Gson();


    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;


        MDC.put("RequestURI", httpRequest.getRequestURI());


        MDC.put("SessionID", StringUtils.defaultIfBlank((String)httpRequest.getSession().getAttribute("SessionID"), ""));
        MDC.put("ConnectionID", StringUtils.defaultIfBlank((String)httpRequest.getSession().getAttribute("ConnectionID"), ""));

//        MDC.put("ANI", StringUtils.defaultIfBlank((String)httpRequest.getSession().getAttribute("ANI"), ""));
//        MDC.put("DNIS", StringUtils.defaultIfBlank((String)httpRequest.getSession().getAttribute("DNIS"), ""));
//        MDC.put("APPID", StringUtils.defaultIfBlank((String)httpRequest.getSession().getAttribute("APPID"), ""));

        logger.debug("parameters: " + gson.toJson(httpRequest.getParameterMap()));


        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Initialize filter name
    }


    @Override
    public void destroy() {

    }

}
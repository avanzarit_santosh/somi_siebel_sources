package gov.mi.state.treas.ivr.config;

import java.util.List;


public interface IvrConfigDAO {


    IvrConfigItem getConfigItem(String msgName);


    int updateConfigItem(String messageName, IvrConfigItem configItem);

    void insertConfigItem(String messageName, IvrConfigItem configItem);

    void deleteConfigItem(String msgName);

    //query all config items
    List<IvrConfigItem> getAllConfigItems();

    /**
     * close with no args is used to close the connection
     */
    void close();
}
/**
 * 
 */
package gov.mi.state.treas.ivr.form;

import gov.mi.state.treas.ivr.config.TreasIVRConfig;
import gov.mi.state.treas.ivr.config.TreasIVRConfig.Databases.Database;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;

/**
 * @author NguyenH10
 *
 */
public class Forms2Me {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	    try {
	    	
	    	// unmarshal xml config
	    	JAXBContext jc = JAXBContext.newInstance("gov.mi.state.treas.ivr.config");
	    	Unmarshaller u = jc.createUnmarshaller();
	    	TreasIVRConfig config = (TreasIVRConfig) u.unmarshal(new File("config.xml"));
	    	List<Database> dbList = config.getDatabases().getDatabase();
	    	Iterator<Database> itor = dbList.iterator();
	    	Database db = null;
	    	while(itor.hasNext()) {
	    		db = itor.next();
	    		if("Forms2Me".equalsIgnoreCase(db.getName()))
	    			break;
	    	}
	    	
	    	
	    	
	        Connection con=null;
	        Class.forName("oracle.jdbc.driver.OracleDriver");
            con = DriverManager.getConnection(db.getConnectionString(), db.getUser(), db.getPassword());
	        Statement s=con.createStatement();
	        ResultSet rs = s.executeQuery("select * from forms_2_me");
	        while(rs.next()) {
	        	System.out.println(rs.getString(1));
	        }

	        s.close();
	        con.close();
	     } catch(Exception e){e.printStackTrace();}


	}
	
	public void submitFormOrder(String formName) {
		
	}

}

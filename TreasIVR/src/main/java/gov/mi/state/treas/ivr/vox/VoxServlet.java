package gov.mi.state.treas.ivr.vox;

import gov.mi.state.treas.ivr.AppContext;
import gov.mi.state.treas.ivr.config.IvrConfigItem;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by nguyenh10 on 11/24/2015.
 */
public class VoxServlet extends HttpServlet {

    private static Logger logger = Logger.getLogger(VoxServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String message = StringUtils.upperCase(req.getParameter("m"));
        String checkFlag = StringUtils.upperCase(req.getParameter("cf"));
        String filename = new StringBuilder()
                .append(StringUtils.defaultIfEmpty(message, "voxservlet"))
                .append(".vox")
                .toString();

        if(StringUtils.isBlank(message)) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        byte[] bytes = null;
        IvrConfigItem configItem = null;


        configItem = AppContext.INSTANCE.getConfigItem(message);

        boolean getData = false;
        if(configItem != null) {
            logger.debug("playing message " + message);
            getData = !"Y".equals(checkFlag)
                    || "Y".equals(configItem.getFlag());
        }

        if(getData) {
            bytes = configItem.getData();
        }


        resp.setContentType("audio/x-vox");

        resp.setHeader("Content-disposition", "attachment; filename=" + filename);

        if(bytes != null) {
            resp.getOutputStream().write(bytes);
        }

        resp.getOutputStream().close();
    }


}

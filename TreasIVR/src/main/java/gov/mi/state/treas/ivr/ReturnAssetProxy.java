/**
 * 
 */
package gov.mi.state.treas.ivr;

import gov.mi.state.treas.ivr.ws.asset.*;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPFaultException;
import javax.xml.xpath.XPathConstants;
import java.net.URL;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author NguyenH10
 * 
 */
public class ReturnAssetProxy extends BaseWebServiceProxy {
	public final static String ACCOUNT_NAME_XPATH = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset/AccountName";
	public final static String DEFAULT_END_POINT = "http://10.42.141.245/eai_enu/start.swe?SWEExtSource=WebService&amp;SWEExtCmd=Execute&amp;Username=IVRIIT&amp;Password=IVRIIT";
	final static String PACKAGE_NAME = "gov.mi.state.treas.ivr.ws.asset";
	
	
	MIIvrAssetManagement_Service assetService;
	MIIvrAssetManagement assetPort;
	BindingProvider bindingProvider;

	public ReturnAssetProxy(String endPoint) throws JAXBException {
		super(endPoint);

		URL wsdlLocation = ReturnAssetProxy.class.getResource("MIIvrAssetManagement.wsdl");

		QName serviceQName = new QName("http://siebel.com/CustomUI", "MIIvrAssetManagement");
		jaxbContext = JAXBContext.newInstance(MIIvrAssetManagement.class.getPackage().getName());

		assetService = new MIIvrAssetManagement_Service();
		assetPort = assetService.getMIIvrAssetManagement();
		// Timeout
		//15s for connection
		bindingProvider = (BindingProvider)assetPort;
		bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
		bindingProvider.getRequestContext().put(IvrUtils.CONNECT_TIMEOUT, 15 * 1000);
		bindingProvider.getRequestContext().put(IvrUtils.REQUEST_TIMEOUT, 15 * 1000);


	}

	public ReturnAssetProxy() throws JAXBException {
		this(DEFAULT_END_POINT);
	}
	
	public Marshaller getMarshaller() throws JAXBException {
		return jaxbContext.createMarshaller();

	}
	
	public Unmarshaller getUnmarshaller() throws JAXBException {
		return jaxbContext.createUnmarshaller();
	}

	public MIIvrAssetManagementQueryByExampleInput createRequest() {
		ObjectFactory of = new ObjectFactory();
		
		MIIvrAssetManagementQueryByExampleInput input = of.createMIIvrAssetManagementQueryByExampleInput();
		ListOfMiIvrAssetManagement liam = of.createListOfMiIvrAssetManagement();
		AssetMgmtAsset ama = of.createAssetMgmtAsset();
		ama.setAccountLocation("SN");
		ama.setAccountName("000000000");
		liam.getAssetMgmtAsset().add(ama);
		input.setListOfMiIvrAssetManagement(liam);
		return input;
	}
	
	public Node getXpathNode(Node node, String path) {
		return (Node) evaluateXpath(path, node, XPathConstants.NODE);
	}

	public String getXpathValue(Node node, String path) {
		return (String) evaluateXpath(path, node, XPathConstants.STRING);
	}


	public Object evaluateXpath(String path, Object object) {
		return evaluateXpath(path, object, XPathConstants.STRING);
	}
	
	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
		BindingProvider bp = (BindingProvider)assetPort;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
		super.setEndPoint(endPoint);
	}

	private static Map<String, Long> activeServiceMap = Collections.synchronizedMap(new HashMap<String, Long>()); 
	
	public boolean isServiceAvailable() {
		Long servicePingTime = activeServiceMap.get(endPoint);
		long curTime = Calendar.getInstance().getTimeInMillis();
		boolean serviceUp = false;
		MIIvrAssetManagementQueryByExampleOutput output = null;
		if(servicePingTime == null || curTime > servicePingTime + 30000) {
	        MIIvrAssetManagementQueryByExampleInput input = createRequest();
	        input.getListOfMiIvrAssetManagement().getAssetMgmtAsset().get(0).setAccountName("000000000");
	        try {
	        	output = sendRequest(input);
	        } catch(Exception e) {
	        	output = null;
	        }
	        return output != null;
		}
		else if(servicePingTime != null) {
			return servicePingTime > 0;
		}
		
        return serviceUp;		
	}
	
	public MIIvrAssetManagementQueryByExampleOutput sendRequest(
			MIIvrAssetManagementQueryByExampleInput req) throws TreasWebServiceException {
		
		try {
			MIIvrAssetManagementQueryByExampleOutput res = assetPort.miIvrAssetManagementQueryByExample(req);
			
			if(res != null) {
				activeServiceMap.put(endPoint, Calendar.getInstance().getTimeInMillis());
			}
			else {
				activeServiceMap.remove(endPoint);
			}
			return res;
			
		} catch (Exception e) {

			activeServiceMap.remove(endPoint);
			throw new TreasWebServiceException("Error accessing Treasury Asset WS", e);
		} 

	}

}

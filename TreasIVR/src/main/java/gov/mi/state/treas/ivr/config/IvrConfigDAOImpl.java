package gov.mi.state.treas.ivr.config;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class IvrConfigDAOImpl implements IvrConfigDAO {
    private JdbcTemplate jdbcTemplate;

    public IvrConfigDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public IvrConfigItem getConfigItem(String msgName) {
        List<IvrConfigItem> configs = jdbcTemplate.query("SELECT * FROM TREAS_IVR_MSG where " +
                        "ivr_name='TREAS_IIT_IVR' and msg_name=?"
                , new IvrConfigMapper()
                , msgName);
        return configs.isEmpty() ? null : configs.get(0);
    }

    @Override
    public int updateConfigItem(String messageName, IvrConfigItem configItem) {
        return jdbcTemplate.update("UPDATE TREAS_IVR_MSG set msg_flg=?, msg_data=?, " +
                "last_modified=sysdate " +
                "WHERE ivr_name=? and msg_name=?"
                , configItem.getFlag(), configItem.getData()
                , configItem.getIvrName(), messageName
        );
    }

    @Override
    public void insertConfigItem(String messageName, IvrConfigItem configItem) {
        jdbcTemplate.update("INSERT INTO TREAS_IVR_MSG(ivr_name, msg_name, msg_flg, msg_data, last_modified)" +
                " VALUES(?, ?, ?, ?, sysdate)"
                , configItem.getIvrName(), messageName, configItem.getFlag(), configItem.getData()
        );
    }

    @Override
    public void deleteConfigItem(String msgName) {
        jdbcTemplate.update("DELETE FROM TREAS_IVR_MSG WHERE ivr_name='TREAS_IIT_IVR' and msg_name=?"
                , msgName
        );
    }

    @Override
    public List<IvrConfigItem> getAllConfigItems() {
        return jdbcTemplate.query("select * FROM TREAS_IVR_MSG WHERE ivr_name='TREAS_IIT_IVR'"
                , new IvrConfigMapper());
    }

    @Override
    public void close() {

    }
}

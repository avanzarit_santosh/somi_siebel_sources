package gov.mi.state.treas.ivr.config;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * Created by nguyenh10 on 11/6/2015.
 */
public class IvrConfigItem implements Serializable {
    public final static byte[] EMPTY_MESSAGE = new byte[]{};


    private final String ivrName;
    private final String messageName;
    private final String flag;
    private final byte[] data;

    private Date lastModified;

    private IvrConfigItem(Builder builder) {
        ivrName = builder.ivrName;
        messageName = builder.messageName;
        flag = builder.flag;
        data = builder.data;
        lastModified = builder.lastModified;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(IvrConfigItem copy) {
        Builder builder = new Builder();

        if(copy == null) {
            return builder;
        }

        builder.ivrName = copy.ivrName;
        builder.messageName = copy.messageName;
        builder.flag = copy.flag;
        builder.data = copy.data;
        builder.lastModified = copy.lastModified;
        return builder;
    }

    public String getIvrName() {
        return ivrName;
    }

    public String getMessageName() {
        return messageName;
    }

    public String getFlag() {
        return flag;
    }

    public byte[] getData() {
        return data;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public String getChecksum() {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        try {
            dos.writeUTF(ivrName);
            dos.writeUTF(messageName);
            dos.writeUTF(StringUtils.defaultIfBlank(flag, "N"));
            if(data != null) {
                dos.write(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return md5(baos.toByteArray());
    }

    private static String md5(byte[] bytes) {

        MessageDigest m=null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }

        m.update(bytes);
        return new BigInteger(1,m.digest()).toString(16);
    }


    public static final class Builder {
        private String ivrName;
        private String messageName;
        private String flag;
        private byte[] data;
        private Date lastModified;

        private Builder() {
        }

        public Builder withIvrName(String val) {
            ivrName = val;
            return this;
        }

        public Builder withMessageName(String val) {
            messageName = val;
            return this;
        }

        public Builder withFlag(String val) {
            flag = val;
            return this;
        }

        public Builder withData(byte[] val) {
            data = val;
            return this;
        }

        public Builder withLastModified(Date val) {
            lastModified = val;
            return this;
        }

        public IvrConfigItem build() {
            return new IvrConfigItem(this);
        }
    }
}

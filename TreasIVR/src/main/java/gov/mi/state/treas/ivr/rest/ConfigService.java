package gov.mi.state.treas.ivr.rest;

import gov.mi.state.treas.ivr.AppContext;
import gov.mi.state.treas.ivr.config.IvrConfigItem;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/configs")
public class ConfigService {
    private final static AppContext appContext = AppContext.INSTANCE;
    private final static Logger logger = Logger.getLogger(ConfigService.class);

	@GET
	@Path("/")
	public List<IvrConfigItem> listConfigs() {
        List<IvrConfigItem> configs = appContext.getAllConfigItems();
        logger.debug("configs size: " + configs.size());

        return configs;

	}

    @GET
    @Path("{name}")
    public IvrConfigItem getConfig(final @PathParam("name") String name) {
        IvrConfigItem config = appContext.getConfigItem(name);
        return config;
    }

    @PUT
    @Path("{name}")
    public Response putConfig(final @PathParam("name") String name, IvrConfigItem config) {

        IvrConfigItem oldConfig = appContext.getConfigItem(name);
        if(oldConfig == null) {
            return Response.noContent().build();
        }


        IvrConfigItem newConfig = IvrConfigItem.newBuilder(config)
                .withMessageName(name)
                .withLastModified(DateTime.now().toDate())
                .withIvrName(AppContext.IVR_NAME)
                .build();

        appContext.putConfigItem(name, newConfig);

        return Response.ok()
                .entity(newConfig)
                .build();
    }
 
}
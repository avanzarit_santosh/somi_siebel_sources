package gov.mi.state.treas.ivr.ws.asset;

import gov.mi.state.treas.ivr.ws.asset.AssetMgmtAsset;
import gov.mi.state.treas.ivr.ws.asset.ListOfMiIvrAssetManagement;
import gov.mi.state.treas.ivr.ws.asset.MIIvrAssetManagementQueryByExampleInput;
import gov.mi.state.treas.ivr.ws.asset.ObjectFactory;

/**
 * Created by nguyenh10 on 9/21/2016.
 */
public class MIIvrAssetManagementQueryByExampleInputBuilder {

    private String accountLocation;
    private String accountName;


    private MIIvrAssetManagementQueryByExampleInputBuilder() {
    }

    public MIIvrAssetManagementQueryByExampleInputBuilder withAccountLocation(String val) {
        accountLocation = val;
        return this;
    }

    public MIIvrAssetManagementQueryByExampleInputBuilder withAccountName(String val) {
        accountName = val;
        return this;
    }

    public MIIvrAssetManagementQueryByExampleInput build() {
        MIIvrAssetManagementQueryByExampleInput input = createRequest();
        return input;
    }

    public static MIIvrAssetManagementQueryByExampleInputBuilder newBuilder() {
        return new MIIvrAssetManagementQueryByExampleInputBuilder();
    }

    private MIIvrAssetManagementQueryByExampleInput createRequest() {
        ObjectFactory of = new ObjectFactory();

        MIIvrAssetManagementQueryByExampleInput input = of.createMIIvrAssetManagementQueryByExampleInput();
        ListOfMiIvrAssetManagement liam = of.createListOfMiIvrAssetManagement();
        AssetMgmtAsset ama = of.createAssetMgmtAsset();
        ama.setAccountLocation(accountLocation);
        ama.setAccountName(accountName);
        liam.getAssetMgmtAsset().add(ama);
        input.setListOfMiIvrAssetManagement(liam);
        return input;
    }

}
/**
 * 
 */
package gov.mi.state.treas.ivr.ws.activity;


import gov.mi.state.treas.ivr.IvrUtils;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.ws.BindingProvider;

/**
 * @author NguyenH10
 *
 */
public class InsertActivityProxy {

	private static JAXBContext jc;

	MIIvrInsertActivity_Service activityService;
	MIIvrInsertActivity activityPort;
	String endPoint;
	ObjectFactory of = new ObjectFactory();
	BindingProvider bindingProvider;

	private final static Logger logger = Logger.getLogger(InsertActivityProxy.class);
	

	public InsertActivityProxy() throws JAXBException {
			

			jc = JAXBContext.newInstance(MIIvrInsertActivity.class.getPackage().getName());

			
			activityService = new MIIvrInsertActivity_Service();
			activityPort = activityService.getPort(MIIvrInsertActivity.class);
			
			// Timeout
			//15s for connection
			bindingProvider = (BindingProvider)activityPort;
			bindingProvider.getRequestContext().put(IvrUtils.CONNECT_TIMEOUT, 15 * 1000);
			bindingProvider.getRequestContext().put(IvrUtils.REQUEST_TIMEOUT, 15 * 1000);

	}

	public InsertActivityProxy(String endPoint) throws JAXBException {
		this();
		setEndPoint(endPoint);
	}
	
	public MIIvrInsertActivityInsertOrUpdateInput createRequest() {

		MIIvrInsertActivityInsertOrUpdateInput req = of.createMIIvrInsertActivityInsertOrUpdateInput();
		ListOfMiIvrCreateActivity actionList = of.createListOfMiIvrCreateActivity();
		actionList.getMiCallAction().add(
                MiCallActionBuilder.newBuilder().build());
		req.setListOfMiIvrCreateActivity(actionList);
		
		return req;
	}

	public MIIvrInsertActivityQueryByExampleInput createQueryRequest() {
		MIIvrInsertActivityQueryByExampleInput req = of.createMIIvrInsertActivityQueryByExampleInput();
		MiCallAction action = of.createMiCallAction();
		action.setType("Correspondence - Outbound");
		ListOfMiIvrCreateActivity actionList = of.createListOfMiIvrCreateActivity();
		actionList.getMiCallAction().add(action);
		req.setListOfMiIvrCreateActivity(actionList);
		
		return req;
	}
	
	public MIIvrInsertActivityInsertOrUpdateOutput sendRequest(MIIvrInsertActivityInsertOrUpdateInput req) {
		MIIvrInsertActivityInsertOrUpdateOutput res = activityPort.miIvrInsertActivityInsertOrUpdate(req);

		return res;
	}
	
	public MIIvrInsertActivityQueryByExampleOutput sendQueryRequest(MIIvrInsertActivityQueryByExampleInput input) {
//        final Gson gson = new GsonBuilder().setPrettyPrinting().create();
//        logger.debug("input: " + gson.toJson(input));
		MIIvrInsertActivityQueryByExampleOutput output = activityPort.miIvrInsertActivityQueryByExample(input);

		return output;
	}
	
	public Marshaller getMarshaller() throws JAXBException {
		return jc.createMarshaller();
	}
	
	public JAXBContext getJAXBContext() {
		return jc;
	}
	
	public Unmarshaller getUnmarshaller() throws JAXBException {
		return jc.createUnmarshaller();
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
		BindingProvider bp = (BindingProvider)activityPort;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
	}

}

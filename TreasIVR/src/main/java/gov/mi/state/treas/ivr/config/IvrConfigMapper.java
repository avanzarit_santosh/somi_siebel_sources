package gov.mi.state.treas.ivr.config;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by nguyenh10 on 11/25/2015.
 */
public class IvrConfigMapper implements RowMapper<IvrConfigItem> {
    @Override
    public IvrConfigItem mapRow(ResultSet resultSet, int i) throws SQLException {
        return IvrConfigItem.newBuilder()
                .withIvrName(resultSet.getString("ivr_name"))
                .withMessageName(resultSet.getString("msg_name"))
                .withFlag(resultSet.getString("msg_flg"))
                .withData(resultSet.getBytes("msg_data"))
                .withLastModified(resultSet.getTime("last_modified"))
                .build();
    }
}

package gov.mi.state.treas.ivr.ws.activity;

/**
 * Created by nguyenh10 on 2/11/2016.
 */
public class MiCallActionBuilder {


    private String accountLocation;
    private String accountName;
    private String description;
    private String status;
    private String taxType;
    private String type;


    public static MiCallActionBuilder newBuilder() {
        return new MiCallActionBuilder()
                .withAccountLocation("SN")
                .withAccountName("000000000")
                .withDescription("test")
                .withStatus("Complete")
                .withTaxType("IIT")
                .withType("Incoming Call CRIS");
    }

    public static MiCallActionBuilder newBuilder(MiCallAction copy) {
        MiCallActionBuilder builder = new MiCallActionBuilder();

        if(copy != null) {
            builder.accountLocation = copy.accountLocation;
            builder.accountName = copy.accountName;
            builder.description = copy.description;
            builder.status = copy.status;
            builder.taxType = copy.taxType;
            builder.type = copy.type;
        }
        return builder;
    }


    private MiCallActionBuilder() {
    }

    public MiCallActionBuilder withAccountLocation(String val) {
        accountLocation = val;
        return this;
    }

    public MiCallActionBuilder withAccountName(String val) {
        accountName = val;
        return this;
    }

    public MiCallActionBuilder withDescription(String val) {
        description = val;
        return this;
    }

    public MiCallActionBuilder withStatus(String val) {
        status = val;
        return this;
    }

    public MiCallActionBuilder withTaxType(String val) {
        taxType = val;
        return this;
    }

    public MiCallActionBuilder withType(String val) {
        type = val;
        return this;
    }

    public MiCallAction build() {

        MiCallAction miCallAction = new MiCallAction();
        miCallAction.setAccountLocation(accountLocation);
        miCallAction.setAccountName(accountName);
        miCallAction.setDescription(description);
        miCallAction.setStatus(status);
        miCallAction.setTaxType(taxType);
        miCallAction.setType(type);
        return miCallAction;
    }

}

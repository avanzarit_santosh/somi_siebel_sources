/**
 * 
 */
package gov.mi.state.treas.ivr;

import com.google.common.collect.ImmutableSet;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPathConstants;
import java.util.*;

/**
 * @author NguyenH10
 *
 */
public class ReturnAsset {

	public final static String FORM_1040 = "MI-1040";
	public final static String FORM_1040CR = "MI-1040CR";
	public final static String FORM_1040CR2 = "MI-1040CR2";
	public final static String FORM_1040CR7 = "MI-1040CR7";
	public final static String FORM_1040X = "MI-1040X";

	private final static Set<String> EXCLUDE_CODES = ImmutableSet.of("0731", "0909", "0910");

	private final static Set<String> DISCOVERY_ERROR_CODES = ImmutableSet.of("12", "14", "15"
			, "22", "26", "43", "62", "90", "91", "92", "99");

	
	private DOMObject responseObject;
    private String taxYear;
    private String formName;
    private String voxFileDir;
    private String addStuff;

	private Node formNode = null;
	private Node assetNode = null;
	private Node refund = null;
	private Node refund1040 = null;
	private Node refundableCreditCr = null;
	private Node refundableCreditCr7 = null;
	private Node refundOffset = null;
	private Node creditForward = null;
	private Node paymentWithReturn = null;
	private Node paymentWithReturnApplied = null;
	private Node energyDraft = null;

	private String warrantNumber = null;

	private String auditedBalance;
	private String dueAmount;
	private String refundAmount;
	private String sacar;
	private String thirdPartyFlag;
	private String creditCaller;
	private String filingStatus;
	private String estimatedReceivedDateString;
	private String estimatedCompletionDateString;
	private Date estimatedCompletionDate;
	private Date receivedDate;

	private String contactReason = "Where is My Refund";
	private String contactResolution;

	private boolean returnStatusCompleted;
	private boolean estimatedCompletionDatePassed;
	private String additionalMessage;
	private String adjustedMessage;

	private String returnStatus;
	private String errorCode;   // CRM Error Description
	private boolean checkContactLogs = false;

	public String getSsn() {
		return responseObject.getXpathValue("//AccountName");
	}
	
	public String getFilingStatus(){
		return filingStatus;
	}
	
	public boolean hasAdjustedMessage() {
		return adjustedMessage != null;
	}
	
	public String getAdjustedMessage() {
		return adjustedMessage;
	}
	
	public boolean hasAdditionalMessage() {
		return additionalMessage != null;
	}
	
	public String getAdditionalMessage() {
		return additionalMessage;
	}
	
	public String getAdditionalResponse() {
		return additionalMessage;
	}
	
	public String getEstimatedReceivedDateString() {
		return estimatedReceivedDateString;
	}
	
	public String getEstimatedCompletionDateString() {
		return estimatedCompletionDateString;
	}
	
	public Date getEstimatedCompletionDate() {
		return estimatedCompletionDate;
	}
	
	public Date getReceivedDate() {
		return receivedDate;
	}

	public String getContactReason() {
		return contactReason;
	}

	public String getContactResolution() {
		return contactResolution;
	}

	
	public ReturnAsset(Node node) {
		this.responseObject = new DOMObject(node);
	}

	
	public String getCreditCaller() {
		if(creditCaller != null)
			return creditCaller;

		Node fn = null;
		// set credit caller status
		String assetPath = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset";
		String formPath = assetPath + "/ListOfFormAssetMgmt-Asset/FormAssetMgmt-Asset[AgeOver65='1' or Paraplegic='1' or BlindDeaf='1' or Disabled='1']";
		fn = responseObject.getXpathNode(formPath);
		
		creditCaller = (fn != null) ? "YES" : "NO";
		return creditCaller;
	}
	
	public boolean authenticateAgiOrHhi(String authYear, String agi) {
		if(authYear == null || "".equals(agi) || "0".equals(agi))
			return false;
		if(!authYear.startsWith("IIT"))
			authYear = "IIT" + authYear;
		String expr = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset[TaxYear='" + authYear + "']"
			+ "/ListOfFormAssetMgmt-Asset/FormAssetMgmt-Asset[GrossReceiptsReduction='" 
			+ agi + "' or OriginalCost='"+ agi +"' or GrossReceiptsReduction='-"+ agi + "' or OriginalCost='-"+ agi +"']";
		Node node = (Node)responseObject.evalXpath(expr, XPathConstants.NODE);
		return node != null;
	}
	
	public boolean isCreditCaller() {
		return "YES".equalsIgnoreCase(getCreditCaller());
	}
	
	public String getReturnStatus() {
		return returnStatus;
	}
	
	public String getAddStuff() {
		return addStuff;
	}

	public Node getRefund() {
		return refund;
	}

	public Node getRefundOffset() {
		return refundOffset;
	}

	public Node getCreditForward() {
		return creditForward;
	}

	public Node getPaymentWithReturn() {
		return paymentWithReturn;
	}

	public String getWarrantNumber() {
		return warrantNumber;
	}

	public String getAuditedBalance() {
		return auditedBalance;
	}

	public String getDueAmount() {
		return dueAmount;
	}

	public String getRefundAmount() {
		return refundAmount;
	}

	public String getSacar() {
		return sacar;
	}

	public String getThirdPartyFlag() {
		return thirdPartyFlag;
	}

	public boolean isReturnStatusCompleted() {
		return returnStatusCompleted;
	}

	public DOMObject getResponseObject() {
		return responseObject;
	}


	public String getTaxYear() {
		return taxYear;
	}

	public void setTaxYear(String taxYear) {
		this.taxYear = taxYear;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public boolean isEstimatedCompletionDatePassed() {
		return estimatedCompletionDatePassed;
	}
	
	public boolean isPendingReview() {
		return !returnStatusCompleted;
	}

	public String getVoxFileDir() {
		return voxFileDir;
	}

	public void setVoxFileDir(String voxFileDir) {
		this.voxFileDir = voxFileDir;
	}

	
	public ReturnAsset(DOMObject res, String taxYear, String formName) {
		this.responseObject = res;
		this.taxYear = taxYear;
		this.formName = formName;
		estimatedCompletionDatePassed = false;
		additionalMessage = null;
	}

	public ReturnAsset(DOMObject res, String taxYear, String formName, String voxFileDir) {
		this(res, taxYear, formName);
		this.voxFileDir = voxFileDir;
	}
	
	public ReturnAsset(DOMObject res) {
		this.responseObject =res;
	}
	
	public ReturnAsset(DOMObject res, String taxYear) {
		this.responseObject = res;
		this.taxYear = taxYear;		
		estimatedCompletionDatePassed = false;
		additionalMessage = null;
	}

	String returnVox;
	
	public String getReturnVox() {
		return returnVox;
	}
	
	public String getResponse() {
		return getResponse(taxYear, formName, voxFileDir);
	}

	
	@Deprecated
	private Node getRefundTransactionWithAmount(String refundAmount) {
		String assetPath = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset" + "[TaxYear='" + taxYear + "']";
		NodeList transList = (NodeList)responseObject.evalXpath(assetPath 
				+ "/ListOfTransactionAssetMgmt-Asset/TransactionAssetMgmt-Asset", 
				XPathConstants.NODESET);
		String amount = null;
		String name = null;
		String type = null;
		String installDate = null;
		Node resultNode = null;
		
		Node node = null;
		for(int i = 0; i < transList.getLength(); i++) {
			node = transList.item(i);
			amount = IvrUtils.getXpathValue(node, "AssetValue2");
			name = IvrUtils.getXpathValue(node, "Name");
			installDate = IvrUtils.getXpathValue(node, "InstallDate");
			type = IvrUtils.getXpathValue(node, "Type");

			if("Refund".equalsIgnoreCase(name) 
					&& !IvrUtils.isEmpty(refundAmount) 
					&& refundAmount.equals(amount)) 
			{
				resultNode = node;
			}
		}
		return resultNode;		
	}
	
	@Deprecated
	private Node getCrRefund() {
		Node crNode = getCrTransaction();
		String crAmount = IvrUtils.getXpathValue(crNode, "AssetValue2");

		return getRefundTransactionWithAmount(crAmount);
	}

	@Deprecated
	private Node getCr7Refund() {
		Node cr7Node = getCr7Transaction();
		String cr7Amount = IvrUtils.getXpathValue(cr7Node, "AssetValue2");

		return getRefundTransactionWithAmount(cr7Amount);
	}

	private Node getCrTransaction() {
		String assetPath = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset" + "[TaxYear='" + taxYear + "']";
		NodeList transList = (NodeList)responseObject.evalXpath(assetPath 
				+ "/ListOfTransactionAssetMgmt-Asset/TransactionAssetMgmt-Asset", 
				XPathConstants.NODESET);
		String amount = null;
		String name = null;
		String type = null;
		String installDate = null;
		Date maxDate = new Date(0);
		Node resultNode = null;

		Node node = null;
		for(int i = 0; i < transList.getLength(); i++) {
			node = transList.item(i);
			amount = IvrUtils.getXpathValue(node, "AssetValue2");
			name = IvrUtils.getXpathValue(node, "Name");
			installDate = IvrUtils.getXpathValue(node, "InstallDate");
			type = IvrUtils.getXpathValue(node, "Type");

			if("Refundable Credit".equalsIgnoreCase(name) && ("CR".equals(type)|| "CR2".equals(type))) {
				Date dt = IvrUtils.parseDate(installDate);
				if(dt != null && maxDate.before(dt)) {
					maxDate = dt;
					resultNode = node;
				}

			}
		}
		return resultNode;
	}
	
	private Node getCr7Transaction() {
		String assetPath = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset" + "[TaxYear='" + taxYear + "']";
		NodeList transList = (NodeList)responseObject.evalXpath(assetPath 
				+ "/ListOfTransactionAssetMgmt-Asset/TransactionAssetMgmt-Asset", 
				XPathConstants.NODESET);
		String amount = null;
		String name = null;
		String type = null;
		String installDate = null;
		Date maxDate = new Date(0);
		Node resultNode = null;

		Node node = null;
		String refundAmount = null;
		for(int i = 0; i < transList.getLength(); i++) {
			node = transList.item(i);
			amount = IvrUtils.getXpathValue(node, "AssetValue2");
			name = IvrUtils.getXpathValue(node, "Name");
			installDate = IvrUtils.getXpathValue(node, "InstallDate");
			type = IvrUtils.getXpathValue(node, "Type");

			if("Refundable Credit".equalsIgnoreCase(name) && "CR7".equals(type)) {
				Date dt = IvrUtils.parseDate(installDate);
				if(dt != null && maxDate.before(dt)) {
					maxDate = dt;
					resultNode = node;
				}
			}
		}
		return resultNode;
	}

	private Node getTransByAmount(String transName, String amt) {

		Node resultNode = null;
		String assetPath = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset" + "[TaxYear='" + taxYear + "']";
		NodeList transList = (NodeList)responseObject.evalXpath(assetPath 
				+ "/ListOfTransactionAssetMgmt-Asset/TransactionAssetMgmt-Asset", 
				XPathConstants.NODESET);

		String amount = null;
		String name = null;
		String type = null;
		Node node = null;
		Date maxDate = new Date(0);
		String installDate = null;

		for(int i = 0; i < transList.getLength(); i++) {
			node = transList.item(i);
			amount = IvrUtils.getXpathValue(node, "AssetValue2");
			name = IvrUtils.getXpathValue(node, "Name");
			type = IvrUtils.getXpathValue(node, "Type");
			installDate = IvrUtils.getXpathValue(node, "InstallDate");
			if(transName.equalsIgnoreCase(name) && amt.equals(amount)) {
				Date dt = IvrUtils.parseDate(installDate);
				if(dt != null && maxDate.before(dt)) {
					maxDate = dt;
					resultNode = node;
				}
			}
		}
		return resultNode;
	}

	private Node getTransByType(String transName, String transType) {
		Node resultNode = null;
		String assetPath = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset" + "[TaxYear='" + taxYear + "']";
		NodeList transList = (NodeList)responseObject.evalXpath(assetPath 
				+ "/ListOfTransactionAssetMgmt-Asset/TransactionAssetMgmt-Asset", 
				XPathConstants.NODESET);

		String amount = null;
		String name = null;
		String type = null;
		Node node = null;
		Date maxDate = new Date(0);
		String installDate = null;

		for(int i = 0; i < transList.getLength(); i++) {
			node = transList.item(i);
			amount = IvrUtils.getXpathValue(node, "AssetValue2");
			name = IvrUtils.getXpathValue(node, "Name");
			type = IvrUtils.getXpathValue(node, "Type");
			installDate = IvrUtils.getXpathValue(node, "InstallDate");
			if(transName.equalsIgnoreCase(name) && (transType == null || transType.equals(type))) {
				Date dt = IvrUtils.parseDate(installDate);
				if(dt != null && maxDate.before(dt)) {
					maxDate = dt;
					resultNode = node;
				}
			}
		}
		return resultNode;
	}

	private Node getTransByName(String transName) {
		return getTransByType(transName, null);
	}

	public Node getRecentForm(String year, String form) {
		String assetPath = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset" + "[TaxYear='" + year + "']";
		String formPath = assetPath + "/ListOfFormAssetMgmt-Asset/FormAssetMgmt-Asset" + "[Name='" + form + "']";
		NodeList formList = (NodeList)responseObject.evalXpath(formPath, XPathConstants.NODESET);
		Node node = null;
		Node resultNode = null;
		Date recDate = null;
		Date maxDate = new Date(0);
		String status = null;
		for(int i = 0; i < formList.getLength(); i++) {
			node = formList.item(i);
			recDate = IvrUtils.parseDate(IvrUtils.getXpathValue(node, "DateProcessed"));
			status = IvrUtils.getXpathValue(node, "Status");
			if(!"Not Ret of Rec".equalsIgnoreCase(status) && recDate != null && recDate.after(maxDate)) {
				resultNode = node;
				maxDate = recDate;
			}
		}

		return resultNode;
	}
	
	private Node get1040RefundTransaction() {
		String assetPath = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset" + "[TaxYear='" + taxYear + "']";
		NodeList transList = (NodeList)responseObject.evalXpath(assetPath 
				+ "/ListOfTransactionAssetMgmt-Asset/TransactionAssetMgmt-Asset", 
				XPathConstants.NODESET);
		
		HashSet<String> excludeAmounts = new HashSet<String>();
		
		String amount = null;
		String name = null;
		String type = null;
		Node refundNode = null;
		Node node = null;
		String refundAmount = null;
		for(int i = 0; i < transList.getLength(); i++) {
			node = transList.item(i);
			amount = IvrUtils.getXpathValue(node, "AssetValue2");
			name = IvrUtils.getXpathValue(node, "Name");
			type = IvrUtils.getXpathValue(node, "Type");
			if("Refundable Credit".equalsIgnoreCase(name) && type != null && type.startsWith("CR")) {
				excludeAmounts.add(amount);
			}
		}
		for(int i = 0; i < transList.getLength(); i++) {
			node = transList.item(i);
			amount = IvrUtils.getXpathValue(node, "AssetValue2");
			name = IvrUtils.getXpathValue(node, "Name");
			type = IvrUtils.getXpathValue(node, "Type");
			if("Refund".equalsIgnoreCase(name) 
					&& !excludeAmounts.contains(amount) 
					&& refundNode == null) {
				refundNode = node;
				refundAmount = amount;
			}
		}
		return refundNode;
	}
	
	public String getResponse(String taxYear, String formName, String voxFileDir){
		this.taxYear = taxYear;
		this.formName = formName;
		this.voxFileDir = voxFileDir;
		estimatedCompletionDatePassed = false;
		additionalMessage = null;
		adjustedMessage = null;

		String assetPath = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset" + "[TaxYear='" + taxYear + "']";
		assetNode = responseObject.getXpathNode(assetPath);
		String formPath = assetPath + "/ListOfFormAssetMgmt-Asset/FormAssetMgmt-Asset" + "[Name='" + formName + "']";
		formNode = getRecentForm(taxYear, formName);

		if("MI-1040CR".equals(formName) && formNode == null) {
			this.formName = formName = "MI-1040CR2";
			formPath = assetPath + "/ListOfFormAssetMgmt-Asset/FormAssetMgmt-Asset" + "[Name='" + formName + "']";
			formNode = getRecentForm(taxYear, formName);
		}

		errorCode = IvrUtils.getXpathValue(formNode, "Version");

		//return status
		returnStatus = IvrUtils.getXpathValue(formNode, "Status");
		
		// get filing status
		filingStatus = IvrUtils.getXpathValue(formNode, "FilingStatus");
		
		
		// get estimated received date
		//estimatedReceivedDate = IvrUtils.getXpathValue(formNode, "EstimatedReceivedDate");
		String receivedDateString = IvrUtils.getXpathValue(formNode, "ReceivedDate");
		receivedDate = IvrUtils.parseDate(receivedDateString);
		estimatedCompletionDateString = IvrUtils.getXpathValue(formNode, "EstimatedReceivedDate");
		estimatedCompletionDate = IvrUtils.parseDate(estimatedCompletionDateString, "MM/dd/yyyy");
		Date curDate = Calendar.getInstance().getTime();
		estimatedCompletionDatePassed = (estimatedCompletionDate != null && curDate.after(estimatedCompletionDate)) 
					? true : false;

		// default response
		returnVox = "014_1040_1040X Zero.vox";
		addStuff = IvrUtils.getVXMLScript();
		addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/014_1040_1040X Zero.vox");
		
		final int GRACE_AMOUNT = 25;
		refund = null;
		refund1040 = null;
		refundableCreditCr = null;
		refundableCreditCr7 = null;
		refundOffset = null;
		creditForward = null;
		paymentWithReturn = null;
		warrantNumber = null;
		energyDraft = null;
		auditedBalance = (String)IvrUtils.getXpathValue(formNode, "AuditedBalance");

		// paid due amount
		dueAmount = (String)IvrUtils.getXpathValue(formNode, "AdjustedTaxBaseBeforeLoss");
		refundAmount = (String)IvrUtils.getXpathValue(formNode, "AssetValue2");
		sacar = IvrUtils.getXpathValue(formNode, "SACAR");
		
		// garnishment
		// ListOfMiIvrAssetManagement/AssetMgmt-Asset/ListOfFormAssetMgmt-Asset/FormAssetMgmt-Asset/ThirdPartyFlag == Y
		thirdPartyFlag = IvrUtils.getXpathValue(formNode, "ThirdPartyFlag");
		returnStatusCompleted = "Completed".equals(returnStatus);

		String refundMsgCode = null;
		
		// init financial institution
		String financialInst = null;

		Node cr7Refund = null;
		Node crRefund = null;


		if(returnStatusCompleted) {
			if(FORM_1040.equals(formName) || FORM_1040X.equals(formName)) {
				refundAmount = (String)IvrUtils.getXpathValue(formNode, "AssetValue2");
				refund = refund1040 = getTransByAmount("Refund", refundAmount);
			}
			else if(FORM_1040CR.equals(formName) || FORM_1040CR.equals(formName)) {
				refundAmount = (String)IvrUtils.getXpathValue(formNode, "AssetValue2");
				refundableCreditCr = FORM_1040CR.equals(formName) ? getTransByType("Refundable Credit", "CR") 
						: getTransByType("Refundable Credit", "CR2");
				refund = crRefund = getTransByAmount("Refund", refundAmount);
			}
			else if(FORM_1040CR7.equals(formName)) {
				refundableCreditCr7 = getTransByType("Refundable Credit", "CR7");
				refundAmount = (String)IvrUtils.getXpathValue(refundableCreditCr7, "AssetValue2");
				refund = cr7Refund = getTransByAmount("Refund", refundAmount);
			}
			
			if(refund != null) {
				refundMsgCode = IvrUtils.getXpathValue(refund, "RefundMsgCode");
			}
			
			//refund offset
			refundOffset = getTransByName("Refund Offset");

			//credit forward
			//creditForward = responseObject.getXpathNode(assetPath + "/ListOfTransactionAssetMgmt-Asset/TransactionAssetMgmt-Asset[Name='Credit Forward'][AssetValue2<0]");
			creditForward = getTransByName("Credit Forward");

			energyDraft = getTransByName("Energy Draft");

			//payment with a return
			paymentWithReturnApplied = responseObject.getXpathNode(assetPath + "/ListOfTransactionAssetMgmt-Asset/TransactionAssetMgmt-Asset[Name='Payment with a Return'][Status='Applied']");

			paymentWithReturn = getTransByAmount("Payment with a Return", dueAmount);

			//paymentWithReturn = responseObject.getXpathNode(assetPath + "/ListOfTransactionAssetMgmt-Asset/TransactionAssetMgmt-Asset[Name='Payment with a Return']");

			warrantNumber = IvrUtils.getXpathValue(refund, "OwnerAssetNumber");

			
			financialInst = responseObject.getXpathValue(assetPath + 
					"/ListOfTransactionAssetMgmt-Asset/TransactionAssetMgmt-Asset[string-length(FinancialInstitution) > 0]/FinancialInstitution");
		}
		
		
		if(DISCOVERY_ERROR_CODES.contains(errorCode) || "Pending Response".equals(returnStatus)) {
			// check if there is any outbound core contact log
			checkContactLogs = true;
		}


		if("MI-1040".equals(formName) || "MI-1040X".equals(formName)) {
			//select response vox to play
			if( returnStatusCompleted
				&& refund != null 
				&& refundOffset != null 
				&& warrantNumber != null && warrantNumber.startsWith("4") //direct deposit
				&& "Y".equals(thirdPartyFlag))
			{
				returnVox = "001_1040GarnBalanceDirDep.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/001_1040GarnBalanceDirDep01.vox");
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(refund, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/001_1040GarnBalanceDirDep02.vox");
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(refund, "InstallDate")));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/001_1040GarnBalanceDirDep03.vox");
				contactResolution = "Third Party Offset";
			}
			else if( returnStatusCompleted
				&& refund != null 
				&& refundOffset != null 
				&& !IvrUtils.isEmpty(warrantNumber) && !warrantNumber.startsWith("4") //NO direct deposit
				&& "Y".equals(thirdPartyFlag))
			{
				returnVox = "007_1040_1040X Partgarnrfd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/007_1040_1040X Partgarnrfd01.vox");
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(refund, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/007_1040_1040X Partgarnrfd02.vox");
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(refund, "InstallDate")));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/007_1040_1040X Partgarnrfd03.vox");
				contactResolution = "Return Processed";
			}
			else if( returnStatusCompleted
				&& refundOffset != null
				&& creditForward != null 
				&& "Applied".equals(IvrUtils.getXpathValue(creditForward, "Status")) // creditforward or refund transaction?
				&& "Y".equals(sacar))
			{
				returnVox = "051_1040_1040X_Partoffcrfwd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/051_1040_1040X_Partoffcrfwd01.vox");
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(creditForward, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/051_1040_1040X_Partoffcrfwd02.vox");
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(creditForward, "InstallDate")));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/051_1040_1040X_Partoffcrfwd03.vox");
				contactResolution = "SAC/AR Offset";
			}
			else if( returnStatusCompleted
				&& refundOffset != null
				&& creditForward != null 
				&& "Applied".equals(IvrUtils.getXpathValue(creditForward, "Status")) // creditforward or refund transaction?
				&& "Y".equals(thirdPartyFlag))
			{
				returnVox = "045_1040_1040X_Partgarncrfwd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/045_1040_1040X_Partgarncrfwd01.vox");
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(creditForward, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/045_1040_1040X_Partgarncrfwd02.vox");
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(creditForward, "InstallDate")));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/045_1040_1040X_Partgarncrfwd03.vox");
				contactResolution = "Third Party Offset";
			}
			else if( returnStatusCompleted
				&& refund == null 
				&& refundOffset != null 
				&& "Y".equals(thirdPartyFlag)
				)
			{
				returnVox = "002_1040_1040X Garnishment0bal.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/002_1040_1040X Garnishment0bal01.vox");
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(refundOffset, "InstallDate")));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/002_1040_1040X Garnishment0bal02.vox");
				contactResolution = "Third Party Offset";
			}
			else if( returnStatusCompleted
				&& refund == null 
				&& refundOffset != null 
				&& "Y".equals(sacar)
				)
			{
				returnVox = "003_1040_1040X Offset0bal.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/003_1040_1040X Offset0bal.vox");
				contactResolution = "SAC/AR Offset";
			}
			else if( returnStatusCompleted
				&& refund != null 
				&& creditForward != null 
				&& "Applied".equals(IvrUtils.getXpathValue(creditForward, "Status")) // creditforward or refund transaction?
				&& warrantNumber != null && warrantNumber.startsWith("4") //direct deposit
				)
			{
				returnVox = "004_1040_1040X DirDepRfd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/004_1040_1040X DirDepRfd01.vox");
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(creditForward, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/004_1040_1040X DirDepRfd02.vox");
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(refund, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/004_1040_1040X DirDepRfd03.vox");
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(refund, "InstallDate")));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/004_1040_1040X DirDepRfd04.vox");
				contactResolution = "Return Processed";
			}
			else if( returnStatusCompleted
				&& refund != null 
				&& refundOffset != null 
				&& warrantNumber != null && warrantNumber.startsWith("4") //direct deposit
				&& "Y".equals(sacar))
			{
				returnVox = "005_1040Partoffwdirdep.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/005_1040Partoffwdirdep01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(refund, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/005_1040Partoffwdirdep02.vox");
				// status date
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(refund, "InstallDate")));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/005_1040Partoffwdirdep03.vox");
				contactResolution = "Third Party Offset";
			}
			else if( returnStatusCompleted
				&& refund != null
				&& refundOffset != null
				&& !IvrUtils.isEmpty(warrantNumber) && !warrantNumber.startsWith("4") //NO direct deposit
				&& "Y".equals(sacar)
				)
			{
				returnVox = "006_1040_1040X Partoffrfd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/006_1040_1040X Partoffrfd01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(refund, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/006_1040_1040X Partoffrfd02.vox");
				// status date (if refund transaction status = ussued??)
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(refund, "InstallDate")));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/006_1040_1040X Partoffrfd03.vox");
				contactResolution = "SAC/AR Offset";
			}
			else if( returnStatusCompleted
				&& refund != null 
				&& creditForward != null 
				&& "Applied".equals(IvrUtils.getXpathValue(creditForward, "Status")) // creditforward or refund transaction?
				&& !IvrUtils.isEmpty(warrantNumber) && !warrantNumber.startsWith("4") //NO direct deposit
				)
			{
				returnVox = "009_1040Rfd&CrdFwd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/009_1040Rfd&amp;CrdFwd01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(refund, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/009_1040Rfd&amp;CrdFwd02.vox");
				// credit forward amount
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(creditForward, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/009_1040Rfd&amp;CrdFwd03.vox");
				// credit forward status date
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(refund, "InstallDate")));
				contactResolution = "Return Processed";
			}
			else if( returnStatusCompleted
				&& paymentWithReturn != null 
				&& paymentWithReturnApplied != null
				&& IvrUtils.parseInt(auditedBalance) > 0  // ??
				)
			{
				returnVox = "011_1040_1040X PartPd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/011_1040_1040X PartPd01.vox");
				// payment with return amount
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(paymentWithReturn, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/011_1040_1040X PartPd02.vox");
				// audited balance
				addStuff += IvrUtils.getVXMLCurrency(auditedBalance);
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/011_1040_1040X PartPd03.vox");
				// date processed
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(formNode, "DateProcessed")));
				contactResolution = "Paid Information";
			}
			else if( returnStatusCompleted
				&& paymentWithReturn != null 
				&& IvrUtils.parseInt(auditedBalance) < GRACE_AMOUNT 
				&& paymentWithReturnApplied != null
				) 
			{
				returnVox = "012_1040_1040X PdRtn.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/012_1040_1040X PdRtn01.vox");
				// payment with return amount
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(paymentWithReturn, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/012_1040_1040X PdRtn02.vox");
				// current status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(paymentWithReturn, "InstallDate")));
				contactResolution = "Paid Information";
			}
			else if( returnStatusCompleted
				&& creditForward != null 
				&& "Applied".equals(IvrUtils.getXpathValue(creditForward, "Status"))
				)
			{
				returnVox = "008_1040CrdFwd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/008_1040CrdFwd01.vox");
				// credit forward amount
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(creditForward, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/008_1040CrdFwd02.vox");
				// credit forward status date
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(creditForward, "InstallDate")));
				contactResolution = "Return Processed";
			}
			else if( returnStatusCompleted
				&& paymentWithReturn == null 
				&& IvrUtils.parseInt(auditedBalance) > GRACE_AMOUNT
				&& "0".equals(dueAmount)
				&& "0".equals(refundAmount)
				)
			{
				returnVox = "013_1040_1040X Taxdue.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/013_1040_1040X Taxdue01.vox");
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(formNode, "DateProcessed")));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/013_1040_1040X Taxdue02.vox");
				addStuff += IvrUtils.getVXMLCurrency(auditedBalance);
				contactResolution = "Return Processed";
			}
			else if( returnStatusCompleted
				&& refund != null 
				&& !IvrUtils.isEmpty(IvrUtils.getXpathValue(refund, "Status"))
				&& !IvrUtils.isEmpty(warrantNumber) && !warrantNumber.startsWith("4") //NO direct deposit
				)
			{
				returnVox = "015_1040_1040X Rfd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/015_1040_1040X Rfd01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(refund, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/015_1040_1040X Rfd02.vox");
				// status date (if refund transaction status = issued??)
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(refund, "InstallDate")));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/015_1040_1040X Rfd03.vox");
				contactResolution = "Return Processed";
			}
			else if( !returnStatusCompleted //PENDING
				)
			{
				returnVox = "017_1040 PndReview.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/017_1040 PndReview01.vox");
				addStuff += IvrUtils.getVXMLDate(getEstimatedCompletionDate());
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/017_1040 PndReview02.vox");
				contactResolution = "Pending Review";
			}
			else if( returnStatusCompleted
					&& paymentWithReturn == null  // No payment with a return
					&& "0".equals(dueAmount)
					&& "0".equals(refundAmount)
				)
			{
				returnVox = "014_1040_1040X Zero.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/014_1040_1040X Zero.vox");
				// date processed
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(formNode, "DateProcessed")));
				contactResolution = "Return Processed";
			}
			else if( returnStatusCompleted
				&& refund != null 
				&& warrantNumber != null && warrantNumber.startsWith("4")
				) 
			{
				returnVox = "010_1040DirDepRfd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/010_1040DirDepRfd01.vox");
				// payment with return amount
				addStuff += IvrUtils.getVXMLCurrency(IvrUtils.getXpathValue(refund, "AssetValue2"));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/010_1040DirDepRfd02.vox");
				// current status date
				addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(IvrUtils.getXpathValue(refund, "InstallDate")));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/010_1040DirDepRfd03.vox");
				contactResolution = "Return Processed";
			}

		}
		else if("MI-1040CR".equals(formName) || "MI-1040CR2".equals(formName)) {
			String status = crRefund != null ? IvrUtils.getXpathValue(crRefund, "Status") 
				: IvrUtils.getXpathValue(refundableCreditCr, "Status");
			if(returnStatusCompleted
				&& refundableCreditCr != null
				&& refundOffset != null
				&& !IvrUtils.isEmpty(warrantNumber) && warrantNumber.startsWith("4") //direct deposit
				&& "Y".equals(thirdPartyFlag)
				) {
				returnVox = "018_PTCGarnBalDirDep.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/018_PTCGarnBalDirDep01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(refundAmount);
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/018_PTCGarnBalDirDep02.vox");
				// transaction status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(getTransDate(Arrays.asList(refund, refundableCreditCr)));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/018_PTCGarnBalDirDep03.vox");
				contactResolution = "Third Party Offset";
			}
			else if(returnStatusCompleted
				&& refundableCreditCr != null
				&& refundOffset != null
				&& status != null
				&& "Y".equals(sacar)
				) {
				returnVox = "023_PTCPartoffrfd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/023_PTCPartoffrfd01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(refundAmount);
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/023_PTCPartoffrfd02.vox");
				// transaction status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(getTransDate(Arrays.asList(refund, refundableCreditCr)));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/023_PTCPartoffrfd03.vox");
				contactResolution = "SAC/AR Offset";
			}
			else if(returnStatusCompleted
				//&& refund != null //not needed  
				&& refundOffset != null
				&& "Applied".equalsIgnoreCase(status)
				&& "Y".equals(thirdPartyFlag)
				) {
				returnVox = "019_PTCGarn0bal.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/019_PTCGarn0bal.vox");
				contactResolution = "Third Party Offset";
			}
			else if(returnStatusCompleted
				&& refundableCreditCr != null
				&& refundOffset != null
				&& !IvrUtils.isEmpty(status)
				&& "Y".equals(thirdPartyFlag)
				) {
				returnVox = "024_PTCPartgarnrfd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/024_PTCPartgarnrfd01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(refundAmount);
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/024_PTCPartgarnrfd02.vox");
				// transaction status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(getTransDate(Arrays.asList(refund, refundableCreditCr)));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/024_PTCPartgarnrfd03.vox");
				contactResolution = "Third Party Offset";
			}
			else if(returnStatusCompleted
				&& refundableCreditCr != null
				&& warrantNumber != null && warrantNumber.startsWith("4") //direct deposit
				) {
				returnVox = "021_PTCDirDepRfd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/021_PTCDirDepRfd01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(refundAmount);
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/021_PTCDirDepRfd02.vox");
				// transaction status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(getTransDate(Arrays.asList(refund, refundableCreditCr)));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/021_PTCDirDepRfd03.vox");
				contactResolution = "Return Processed";
			}
			else if(returnStatusCompleted
				&& refundableCreditCr != null
				&& refundOffset != null
				&& "Y".equals(sacar)
				) {
				returnVox = "020_PTCOffset0bal.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/020_PTCOffset0bal.vox");
				contactResolution = "SAC/AR Offset";
			}
			else if(returnStatusCompleted
				&& refundableCreditCr != null
				&& refundOffset != null
				&& warrantNumber != null && warrantNumber.startsWith("4") //direct deposit
				&& !IvrUtils.isEmpty(refundAmount)
				&& "Y".equals(sacar)
				) {
				returnVox = "022_PTCPartoffwdirdep.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/022_PTCPartoffwdirdep01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(refundAmount);
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/022_PTCPartoffwdirdep02.vox");
				// transaction status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(getTransDate(Arrays.asList(refund, refundableCreditCr)));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/022_PTCPartoffwdirdep03.vox");
				contactResolution = "SAC/AR Offset";
			}
			else if(refundableCreditCr != null
					&& warrantNumber != null && warrantNumber.startsWith("4") //direct deposit
				) {
				returnVox = "025_PTCDirDepRfd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/025_PTCDirDepRfd01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(refundAmount);
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/025_PTCDirDepRfd02.vox");
				// transaction status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(getTransDate(Arrays.asList(refund, refundableCreditCr)));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/025_PTCDirDepRfd03.vox");
				contactResolution = "Return Processed";
			}
			else if(!returnStatusCompleted // PENDING 
			) {
				returnVox = "027_PTCPndReview.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/027_PTCPndReview.vox");
				// get estimated completion date
				addStuff += IvrUtils.getVXMLDate(getEstimatedCompletionDate());
				contactResolution = "Pending Review";
			}
			else if(returnStatusCompleted
				&& refundableCreditCr != null
				&& !IvrUtils.isEmpty(warrantNumber) && !warrantNumber.startsWith("4") //NO direct deposit
				) {
				returnVox = "026_PTCRfd.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/026_PTCRfd01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(refundAmount);
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/026_PTCRfd02.vox");
				// transaction status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(getTransDate(Arrays.asList(refund, refundableCreditCr)));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/026_PTCRfd03.vox");
				contactResolution = "Return Processed";
			}
			else if(returnStatusCompleted) {
				returnVox = "043_PTC_Cashed.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/043_PTC_Cashed.vox");
				// transaction status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(getTransDate(Arrays.asList(refund, refundableCreditCr)));
				contactResolution = "Return Processed";
			}
		}
		else if("MI-1040CR7".equals(formName)) {
			// set refund amount
			int refundAmountInt = IvrUtils.parseInt(refundAmount);
			Node transactionNode = energyDraft;
			transactionNode = transactionNode != null ? transactionNode : cr7Refund;
			transactionNode = transactionNode != null ? transactionNode : refundableCreditCr7;

			String status = IvrUtils.getXpathValue(transactionNode, "Status");

			if(!returnStatusCompleted // pending review
				) {
				returnVox = "028_HHCPndReview.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/028_HHCPndReview.vox");
				// get estimated completion date
				addStuff += IvrUtils.getVXMLDate(getEstimatedCompletionDate());
				contactResolution = "Pending Review";
			}
			else if(returnStatusCompleted
				&& refundableCreditCr7 != null && refundAmountInt > 0
				&& warrantNumber != null && warrantNumber.startsWith("4") //direct deposit
				) {
				returnVox = "032_HHCDirDep.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/032_HHCDirDep01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(refundAmount);
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/032_HHCDirDep02.vox");
				// transaction status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(getTransDate(Arrays.asList(energyDraft, cr7Refund, refundableCreditCr7)));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/032_HHCDirDep03.vox");
				contactResolution = "Return Processed";
			}
			else if(returnStatusCompleted
				&& energyDraft != null && refundAmountInt > 0
				&& "Paid".equalsIgnoreCase(IvrUtils.getXpathValue(energyDraft, "Status"))
				) {
				returnVox = "046_HHCDirVendored.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/046_HHCDirVendored01.vox");
				// transaction status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(getTransDate(Arrays.asList(energyDraft, cr7Refund, refundableCreditCr7)));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/046_HHCDirVendored02.vox");
				contactResolution = "Direct Vendor";
			}
			else if(returnStatusCompleted
				&& refundableCreditCr7 != null && refundAmountInt > 0
				&& "Issued".equalsIgnoreCase(status)
				&& (!IvrUtils.isEmpty(warrantNumber) && !warrantNumber.startsWith("4")
					 || energyDraft != null) //NO direct deposit
				) {
				returnVox = "031_HHCnonFIA.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/031_HHCnonFIA01.vox");
				// refund amount
				addStuff += IvrUtils.getVXMLCurrency(refundAmount);
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/031_HHCnonFIA02.vox");
				// transaction status date (InstallDate)
				addStuff += IvrUtils.getVXMLDate(getTransDate(Arrays.asList(energyDraft, cr7Refund, refundableCreditCr7)));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/031_HHCnonFIA03.vox");
				contactResolution = "Return Processed";
			}
			else if( returnStatusCompleted
				&& (IvrUtils.isEmpty(status) || "Void".equalsIgnoreCase(status) || "Canceled".equalsIgnoreCase(status)
						|| "Unknown".equalsIgnoreCase(status))
					&& refundAmountInt > 0
				) {
				returnVox = "030_HHCProc.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/030_HHCProc.vox");
				addStuff += IvrUtils.getVXMLDate(
				getTransDate(Arrays.asList(energyDraft, cr7Refund, refundableCreditCr7)));
				contactResolution = "Return Processed";
			}
			else if(returnStatusCompleted
				) {
				returnVox = "050_HHCdenied.vox";
				addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/050_HHCdenied01.vox");
				
				addStuff += IvrUtils.getVXMLDate(
						getTransDate(Arrays.asList(energyDraft, cr7Refund, refundableCreditCr7)));
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/050_HHCdenied02.vox");
				contactResolution = "HHC denied";
			}

			if(returnStatusCompleted
				&& refundableCreditCr7 != null && refundAmountInt > 0
				&& "Stopped".equalsIgnoreCase(status)
				) {
				returnVox = "034_HHCRtnrfd.vox";
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/034_HHCRtnrfd.vox");
				contactResolution = "Return Processed";
			}

		}

		if(!IvrUtils.isEmpty(formName)) {
			/////////////////////////////////
			// adding additional messages
			/////////////////////////////////
			if(returnStatusCompleted
				&& refund != null
				&& "Paid".equalsIgnoreCase(IvrUtils.getXpathValue(refund, "Status"))
				&& !IvrUtils.isEmpty(warrantNumber) && !warrantNumber.startsWith("4") //NO direct deposit
			) {
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/039_Addlmsg_Cashed.vox");
				additionalMessage = "039_Addlmsg_Cashed.vox";
				contactResolution = "Refer to Web";
			}
			else if(returnStatusCompleted
				&& refund != null
				&& "Issued".equalsIgnoreCase(IvrUtils.getXpathValue(refund, "Status"))
				&& !IvrUtils.isEmpty(warrantNumber) && !warrantNumber.startsWith("4") //NO direct deposit
			) {
				boolean pass30Days = false;
				Date issueDate = IvrUtils.parseDate(IvrUtils.getXpathValue(refund, "InstallDate"));
				Calendar issuePlus30 = null;
				if(issueDate != null) {
					issuePlus30 = Calendar.getInstance();
					issuePlus30.setTime(issueDate);
					issuePlus30.add(Calendar.DAY_OF_MONTH, 30);
				}
				
				pass30Days = issueDate != null && Calendar.getInstance().getTime().after(issuePlus30.getTime());
			
				if(pass30Days) 
				{				
					additionalMessage = "040_Addlmsg_NotCashed.vox";
					addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/040_Addlmsg_NotCashed.vox");
					contactResolution = "Refer to Web";
				}
			}
			else if( returnStatusCompleted
				&& refund != null  
				&& !IvrUtils.isEmpty(warrantNumber) && !warrantNumber.startsWith("4") //NO direct deposit
				) 
			{
				additionalMessage = "044_Rfd_Check_Returned.vox";
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/044_Rfd_Check_Returned.vox");
				contactResolution = "Ref Returned";
			}

			if(returnStatusCompleted
				&& refund != null
				&& !isExcludeMsgCode(refundMsgCode)
			) {
				adjustedMessage = "041_Addlmsg_AdjRfd.vox";
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/041_Addlmsg_AdjRfd.vox");
				contactResolution = "Ref Adjusted";

				//additionalMessage = "042_Addlmsg_Copy of Ret.vox";
				//addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/042_Addlmsg_Copy of Ret.vox");
			}
		}
	
		return addStuff;
	}
	
	private Date getTransDate(List<Node> trans) {
		String str = null;
		for(Node t : trans) {
			str = IvrUtils.getXpathValue(t, "InstallDate");
			if(str != null) {
				return IvrUtils.parseDate(str);
			}
		}
		return null;
	}
	
	public boolean isExcludeMsgCode(String msgCode) {
		if(IvrUtils.isEmpty(msgCode))
			return true;
		
		String[] codes = msgCode.split(" ");
		for(String code : codes) {
			if(EXCLUDE_CODES.contains(code))
				return true;
		}
		return false;
	}

	public boolean isCheckContactLogs() {
		return checkContactLogs;
	}

}

package gov.mi.state.treas.ivr;

public class IvrException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9102686772808477954L;

	public IvrException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public IvrException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public IvrException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public IvrException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}

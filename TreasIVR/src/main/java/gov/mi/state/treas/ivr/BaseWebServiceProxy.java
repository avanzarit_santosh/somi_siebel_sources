/**
 * 
 */
package gov.mi.state.treas.ivr;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * @author NguyenH10
 *
 */
public class BaseWebServiceProxy {

	String endPoint;
	
	Unmarshaller unmarshaller;
	Marshaller marshaller;
	JAXBContext jaxbContext;
	
	public BaseWebServiceProxy(String endPoint) {
		this.endPoint = endPoint;

	}
	
	public Unmarshaller getUnmarshaller() throws JAXBException {
		return unmarshaller;
	}

	public Marshaller getMarshaller() throws JAXBException {
		return marshaller;
	}

	
	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint; 
	}
	
	public String getEndPoint() {
		return this.endPoint;
	}
	
	public Node getDomNode(Object obj) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(false);
			DocumentBuilder db = dbf.newDocumentBuilder();
			StringWriter sw = new StringWriter();
			StreamResult sr = new StreamResult(sw);
			
			getMarshaller().marshal(obj, sr);
			
			Document doc = db.parse(new InputSource(new StringReader(sw.toString())));

			return doc.getDocumentElement();	
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return null;
	}
	

	public Node getNode(Object obj) {
		return getDomNode(obj);
	}
	
	public Object evaluateXpath(String path, Object object, QName qName) {
		Node node = null;
		if (object instanceof Node) {
			node = (Node) object;
		}
		else {
			node = getDomNode(object);
		}

		if (node instanceof Node) {
			XPath xpath = XPathFactory.newInstance().newXPath();
			try {
				return xpath.evaluate(path, node, qName);
			} catch (XPathExpressionException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	

}

/**
 * 
 */
package gov.mi.state.treas.ivr;

import gov.mi.state.treas.ivr.ws.rpt.MIIvrCurrReturnProcessDateQueryByExampleInput;
import gov.mi.state.treas.ivr.ws.rpt.MIIvrCurrReturnProcessDateQueryByExampleOutput;
import gov.mi.state.treas.ivr.ws.rpt.MiCurrentReturnProcessDate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

/**
 * @author NguyenH10
 *
 */
public class ReturnProcessDateProxy extends BaseWebServiceProxy {
	public final static String DEFAULT_END_POINT = "http://10.42.141.245/eai_enu/start.swe?SWEExtSource=WebService&amp;SWEExtCmd=Execute&amp;Username=IVRIIT&amp;Password=IVRIIT";

	
	public ReturnProcessDateProxy(String endPoint) {
		super(endPoint);
		try {
			jaxbContext = JAXBContext.newInstance("gov.mi.state.treas.ivr.ws.rpt");
			unmarshaller = jaxbContext.createUnmarshaller();
			marshaller = jaxbContext.createMarshaller();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ReturnProcessDateProxy() {
		this(DEFAULT_END_POINT);
	}
	
	private static long cachedResultTime = 0;
	private static MIIvrCurrReturnProcessDateQueryByExampleOutput cachedResult = null;
	
	public MIIvrCurrReturnProcessDateQueryByExampleOutput  sendRequestCacheResult(MIIvrCurrReturnProcessDateQueryByExampleInput req) {

		MIIvrCurrReturnProcessDateQueryByExampleOutput result = null;
		long currentTime = Calendar.getInstance().getTimeInMillis();
		if(cachedResult == null || currentTime > cachedResultTime + (30 * 60000)) {
			// send request 
			result = sendRequest(req);
			
			// cache result
			if(result != null) {
				cachedResult = result;
				cachedResultTime = currentTime;
			}
		}
		
		return cachedResult;
	}
	
	public MIIvrCurrReturnProcessDateQueryByExampleOutput sendRequest(MIIvrCurrReturnProcessDateQueryByExampleInput req) {
		try {
			URL wsdlLocation = this.getClass().getResource("MIIvrCurrReturnProcessDate.wsdl");

			QName serviceQName = new QName("http://siebel.com/CustomUI",
					"MIIvrCurrReturnProcessDate");
			QName portQName = new QName("http://siebel.com/CustomUI",
					"MIIvrCurrReturnProcessDate");

			Service service = Service.create(wsdlLocation, serviceQName);

			Dispatch<Source> dispatch = service.createDispatch(portQName,
					Source.class, Service.Mode.PAYLOAD);
			Map<String, Object> map = dispatch.getRequestContext();
			map.put(BindingProvider.SOAPACTION_USE_PROPERTY, Boolean.TRUE);
			map
					.put(BindingProvider.SOAPACTION_URI_PROPERTY,
							"document/http://siebel.com/CustomUI:MIIvrCurrReturnProcessDateQueryByExample");
			map.put( BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
			map.put(IvrUtils.CONNECT_TIMEOUT, 15 * 1000);
			map.put(IvrUtils.REQUEST_TIMEOUT, 15 * 1000);

			map.put("com.sun.xml.ws.connect.timeout", 15 * 1000); 
			map.put("com.sun.xml.ws.request.timeout", 15 * 1000);
			DOMResult dr = new DOMResult();
			marshaller.marshal(req, dr);

			Source response = dispatch.invoke(new DOMSource(dr.getNode()));

			MIIvrCurrReturnProcessDateQueryByExampleOutput output = (MIIvrCurrReturnProcessDateQueryByExampleOutput)unmarshaller.unmarshal(response);
			return output;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	

	
	public Date getEfileReturnProcessDate(MIIvrCurrReturnProcessDateQueryByExampleOutput output) {
		Date efileRPT = null;
		List<MiCurrentReturnProcessDate> dateList = new ArrayList<MiCurrentReturnProcessDate>();
		dateList = output.getListOfMicurrentreturnprocessdate().getMiCurrentReturnProcessDate();
		Iterator<MiCurrentReturnProcessDate> itor = dateList.iterator();
		MiCurrentReturnProcessDate rpd = null;
		while(itor.hasNext()) {
			rpd = itor.next();
			if("E-File Returns".equalsIgnoreCase(rpd.getAbstract())) {
				efileRPT = IvrUtils.parseDate(rpd.getMessageBody(), "MM/dd/yyyy", efileRPT);
				return efileRPT;
			}
		}
		return efileRPT;		
	}
	
	public Date getPaperReturnProcessDate(MIIvrCurrReturnProcessDateQueryByExampleOutput output) {
		Date paperRPT = null;
		List<MiCurrentReturnProcessDate> dateList = new ArrayList<MiCurrentReturnProcessDate>();
		dateList = output.getListOfMicurrentreturnprocessdate().getMiCurrentReturnProcessDate();
		Iterator<MiCurrentReturnProcessDate> itor = dateList.iterator();
		MiCurrentReturnProcessDate rpd = null;
		while(itor.hasNext()) {
			rpd = itor.next();
			if("Paper Returns".equalsIgnoreCase(rpd.getAbstract())) {
				paperRPT = IvrUtils.parseDate(rpd.getMessageBody(), "MM/dd/yyyy", paperRPT);
				return paperRPT;
			}
		}
		return paperRPT;		
	}
	
	public MIIvrCurrReturnProcessDateQueryByExampleInput createRequest() {
		try {
			InputStream is = this.getClass().getResourceAsStream("ReturnProcessDateRequest.xml");
			
			return (MIIvrCurrReturnProcessDateQueryByExampleInput)unmarshaller.unmarshal(is);
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}

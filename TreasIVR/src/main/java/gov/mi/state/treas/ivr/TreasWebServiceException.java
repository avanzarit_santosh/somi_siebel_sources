package gov.mi.state.treas.ivr;

public class TreasWebServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9102686772808477954L;

	public TreasWebServiceException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TreasWebServiceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public TreasWebServiceException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public TreasWebServiceException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

}

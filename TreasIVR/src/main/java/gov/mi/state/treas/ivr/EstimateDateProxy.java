/**
 * 
 */
package gov.mi.state.treas.ivr;


import gov.mi.state.treas.ivr.ws.error.MIIvrErrorCodeReturnDateQueryByExampleInput;
import gov.mi.state.treas.ivr.ws.error.MIIvrErrorCodeReturnDateQueryByExampleOutput;
import gov.mi.state.treas.ivr.ws.error.MiIitCompletionDate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * @author NguyenH10
 *
 */
public class EstimateDateProxy extends BaseWebServiceProxy {
	public final static String DEFAULT_END_POINT = "http://10.42.141.245/eai_enu/start.swe?SWEExtSource=WebService&amp;SWEExtCmd=Execute&amp;Username=IVRIIT&amp;Password=IVRIIT";

	
	public EstimateDateProxy(String endPoint) {
		super(endPoint);
		try {
			jaxbContext = JAXBContext.newInstance("gov.mi.state.treas.ivr.ws.error");
			unmarshaller = jaxbContext.createUnmarshaller();
			marshaller = jaxbContext.createMarshaller();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public EstimateDateProxy() {
		this(DEFAULT_END_POINT);
	}
	
	public MIIvrErrorCodeReturnDateQueryByExampleOutput sendRequest(MIIvrErrorCodeReturnDateQueryByExampleInput req) {
		try {
			URL wsdlLocation = this.getClass().getResource("MIIvrErrorCodeReturnDate.wsdl");

			QName serviceQName = new QName("http://siebel.com/CustomUI",
					"MIIvrErrorCodeReturnDate");
			QName portQName = new QName("http://siebel.com/CustomUI",
					"MIIvrErrorCodeReturnDate");

			Service service = Service.create(wsdlLocation, serviceQName);

			Dispatch<Source> dispatch = service.createDispatch(portQName,
					Source.class, Service.Mode.PAYLOAD);
			Map<String, Object> map = dispatch.getRequestContext();
			map.put(BindingProvider.SOAPACTION_USE_PROPERTY, Boolean.TRUE);
			map
					.put(BindingProvider.SOAPACTION_URI_PROPERTY,
							"document/http://siebel.com/CustomUI:MIIvrErrorCodeReturnDateQueryByExample");
			map.put( BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
			map.put(IvrUtils.CONNECT_TIMEOUT, 15 * 1000);
			map.put(IvrUtils.REQUEST_TIMEOUT, 15 * 1000);

			map.put("com.sun.xml.ws.connect.timeout", 15 * 1000); 
			map.put("com.sun.xml.ws.request.timeout", 15 * 1000);
			
			DOMResult dr = new DOMResult();
			marshaller.marshal(req, dr);

			Source response = dispatch.invoke(new DOMSource(dr.getNode()));

			MIIvrErrorCodeReturnDateQueryByExampleOutput output = (MIIvrErrorCodeReturnDateQueryByExampleOutput)unmarshaller.unmarshal(response);
			return output;
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	
	public MIIvrErrorCodeReturnDateQueryByExampleInput createRequest() {
		try {
			InputStream is = this.getClass().getResourceAsStream("ErrorRequest.xml");
			
			return (MIIvrErrorCodeReturnDateQueryByExampleInput)unmarshaller.unmarshal(is);
			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}
	
	public Date getEstimateCompletionDate(MIIvrErrorCodeReturnDateQueryByExampleOutput res, Date fileDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fileDate);
		cal.add(Calendar.WEEK_OF_MONTH, getNumWeeks( res, fileDate));
		return cal.getTime();
	}
	
	int getNumWeeks(MIIvrErrorCodeReturnDateQueryByExampleOutput res, Date fileDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fileDate);
		int month = cal.get(Calendar.MONTH);
		String str = "10";
		try {
			switch(month) {
			case Calendar.JANUARY:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getJanuary();
				break;
			case Calendar.FEBRUARY:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getFebruary();
				break;
			case Calendar.MARCH:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getMarch();
				break;
			case Calendar.APRIL:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getApril();
				break;
			case Calendar.MAY:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getMay();
				break;
			case Calendar.JUNE:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getJune();
				break;
			case Calendar.JULY:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getJuly();
				break;
			case Calendar.AUGUST:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getAugust();
				break;
			case Calendar.SEPTEMBER:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getSeptember();
				break;
			case Calendar.OCTOBER:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getOctober();
				break;
			case Calendar.NOVEMBER:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getNovember();
				break;
			case Calendar.DECEMBER:
				str = res.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0).getDecember();
				break;
			default:
				str = "0";
			}
			return Integer.parseInt(str);
		}catch(Exception e) {
			return 10;
		}

	}
	
	
	
	public static void main(String args[]) throws Exception {
		EstimateDateProxy proxy = new EstimateDateProxy("http://10.42.141.246/eai_enu/start.swe?SWEExtSource=WebService&amp;SWEExtCmd=Execute&amp;Username=IVRIIT&amp;Password=IVRIIT");
		MIIvrErrorCodeReturnDateQueryByExampleInput req = proxy.createRequest();
		MiIitCompletionDate cd = req.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0);
		
		cd.setType("Efile ER");
		cd.setErrorCode("10");
		
		
		MIIvrErrorCodeReturnDateQueryByExampleOutput res = proxy.sendRequest(req);
		Marshaller m = proxy.getMarshaller();
		m.marshal(res, System.out);
	}
}

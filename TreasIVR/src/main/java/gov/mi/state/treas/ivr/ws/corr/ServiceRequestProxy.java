/**
 * 
 */
package gov.mi.state.treas.ivr.ws.corr;


import gov.mi.state.treas.ivr.IvrUtils;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.BindingProvider;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.Date;
import java.util.List;

/**
 * @author NguyenH10
 *
 */
public class ServiceRequestProxy {
	
	public final static String TEST_END_POINT = "http://10.42.141.245/eai_enu/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&Username=IVRIIT&Password=IVRIIT";
	public final static String DEFAULT_END_POINT = "http://10.42.141.245/eai_enu/start.swe?SWEExtSource=WebService&SWEExtCmd=Execute&Username=IVRIIT&Password=IVRIIT";
	
	private static JAXBContext jaxbContext;

	MIIvrServiceRequest_Service srService;
	MIIvrServiceRequest srPort;
	String endPoint;
	ObjectFactory of;
	BindingProvider bindingProvider;

	

	public ServiceRequestProxy() throws JAXBException {
		
		of  = new ObjectFactory();
		URL wsdlLocation = this.getClass().getResource("MIIvrServiceRequest.wsdl");

		QName serviceQName = new QName("http://siebel.com/CustomUI", "MIIvrServiceRequest");
		jaxbContext = JAXBContext.newInstance(MIIvrServiceRequest.class.getPackage().getName());
		
		srService = new MIIvrServiceRequest_Service(wsdlLocation, serviceQName);
		srPort = srService.getMIIvrServiceRequest();
		// Timeout
		//15s for connection
		bindingProvider = (BindingProvider)srPort;
		bindingProvider.getRequestContext().put(IvrUtils.CONNECT_TIMEOUT, 15 * 1000);
		bindingProvider.getRequestContext().put(IvrUtils.REQUEST_TIMEOUT, 15 * 1000);


	}

	public ServiceRequestProxy(String endPoint) throws JAXBException {
		this();
		setEndPoint(endPoint);
	}

	public MIIvrServiceRequestQueryByExampleInput createQueryRequest() {
		MIIvrServiceRequestQueryByExampleInput input = of.createMIIvrServiceRequestQueryByExampleInput();
		ListOfMiservicerequest lsr = of.createListOfMiservicerequest();
		lsr.getServiceRequest().add(of.createServiceRequest());
		input.setListOfMiservicerequest(lsr);
		return input;
	}
	

	public MIIvrServiceRequestQueryByExampleOutput sendQueryRequest(MIIvrServiceRequestQueryByExampleInput input) {
		MIIvrServiceRequestQueryByExampleOutput output = srPort.miIvrServiceRequestQueryByExample(input);

		return output;
	}
	
	public Marshaller getMarshaller() throws JAXBException {
		return jaxbContext.createMarshaller();
	}
	
	public JAXBContext getJAXBContext() {
		return jaxbContext;
	}
	
	public Unmarshaller getUnmarshaller() throws JAXBException {
		return jaxbContext.createUnmarshaller();
	}



	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
		BindingProvider bp = (BindingProvider)srPort;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
	}
	
	public String serialize(ServiceRequest sr) throws JAXBException {
		StringWriter sw = new StringWriter();
		
		getMarshaller().marshal(new JAXBElement<ServiceRequest>(
				  new QName("uri","local"), ServiceRequest.class, sr), sw);

		return sw.toString();
	}
	
	public ServiceRequest deserialize(String str) throws JAXBException {
		
		JAXBElement<ServiceRequest> sr = null;

		sr = getUnmarshaller().unmarshal(new StreamSource(new StringReader(str)), ServiceRequest.class);
		return sr.getValue();
	}
	
    public ServiceRequest getMostRecentSR(MIIvrServiceRequestQueryByExampleOutput srResponse) {
    	List<ServiceRequest> srList = srResponse.getListOfMiservicerequest().getServiceRequest();
    	Date maxDate = new Date(0);
    	Date closeDate = null;
    	Date createdDate = null;
    	ServiceRequest resultSr = null;
    	for(ServiceRequest sr : srList) {
            closeDate = IvrUtils.parseDate(sr.getClosedDate());
            createdDate = IvrUtils.parseDate(sr.getCreated());
            
            if(createdDate != null && createdDate.after(maxDate)) {
                maxDate = createdDate;
                resultSr = sr;
            }    		
    	}
    	return resultSr;
    }
}

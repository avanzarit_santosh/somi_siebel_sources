<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"ReturnSetResponse.jsp","ReturnListMenu.jsp","TaxInfoMainMenu.jsp","TaxInfoTransfer.jsp","RtnInfoEndMenu.jsp","TaxInfoGoodBye.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.     
	//                    Can be used o Access the HTTP Objects 
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext) {

	}

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.              
	//See the help file for more details        
	public void processAnywhere(PageContext localPageContext) {
		final String SILENCE_VOX = "Silence.vox";
		final String IC_KEY = "RtnInfoEndMenuIC";
		final int PREV_MSG_PORT = 1;
		final int ANOTHER_RETURN_PORT = 2;
		final int MAIN_MENU_PORT = 3;
		final int CSR_TRANSFER_PORT = 4;
		final int REPEAT_PORT = 5;
		final int INVALID_PORT = 6;

		String voxFileDir = lookUp(localPageContext, "VOXFILEDIR");
		String choice = lookUp(localPageContext, "RtnInfoEndMenu");

		int resultPort = PREV_MSG_PORT;
		boolean choiceValid = false;

		if ("*".equals(choice)) {
			// reset IC
			setPageItem(localPageContext, IC_KEY, "0");
			choiceValid = true;
			resultPort = REPEAT_PORT;
		} else if ("1".equals(choice) || "2".equals(choice) || "3".equals(choice)) {
			choiceValid = true;
			setPageItem(localPageContext, IC_KEY, "0");
			resultPort = Integer.parseInt(choice);
		} else if ("4".equals(choice)) {
			choiceValid = SILENCE_VOX.equals(lookUp(localPageContext,
					"CSRMenuItem")) ? false : true;
            resultPort = Integer.parseInt(choice);
		} else {
			choiceValid = false;
		}

		if (!choiceValid) {
			String ICString = lookUp(localPageContext, IC_KEY);
			int IC = (ICString == null) ? 0 : IvrUtils.parseInt(ICString);
			IC++;
			if (IC > 2) {
				setPageItem(localPageContext, IC_KEY, "0");
				resultPort = INVALID_PORT;
			} else {
				setPageItem(localPageContext, IC_KEY, String.valueOf(IC));
				resultPort = REPEAT_PORT;
				String invalidMsg = ("".equals(choice)) ? "NoInput.vox"
						: "InvalidInput.vox";
				String addStuff = IvrUtils.getVXMLScript();
				addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/"
						+ invalidMsg);
				localPageContext.setAttribute("addStuff", addStuff,
						PageContext.PAGE_SCOPE);
			}
		}
		
		setPageItem(localPageContext, "RtnInfoEndMenu", "");

		localPageContext.setAttribute("nAllowType", new Integer(resultPort),
				PageContext.PAGE_SCOPE);
	}

	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext) {

	}
%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
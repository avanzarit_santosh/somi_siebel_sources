<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"RtnInfoEndMenu.jsp","RtnInfoMailAdjMenu.jsp","TaxInfo_CoreCheck.jsp","TaxInfoSystemDown.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.      
	//                    Can be used o Access the HTTP Objects 
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext){
	}

	Logger logger = Logger.getLogger(getClass());


	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.                
	//See the help file for more details        
	public void processAnywhere(PageContext localPageContext){

        final int REGULAR_RESPONSE = 1;
        final int ADJUSTMENT_RESPONSE = 2;
		final int CORE_CHECK = 3;
		final int EXCEPTION = 4;

        int resultPort = REGULAR_RESPONSE;
        
        String voxFileDir = lookUp(localPageContext, "VOXFILEDIR");
        String taxYear = lookUp(localPageContext, "TaxYear");
        String callPath = lookUp(localPageContext, "CallPath");
		String callerSsn = lookUp(localPageContext, "CallerSsn");


        setPageItem(localPageContext, "EstimatedCompletionDate", "");
        
        String addStuff = IvrUtils.getVXMLScript();
        addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/014_1040_1040X Zero.vox");
        
        String returnResponse = "043_PTC_Cashed.vox";
        String formChoice = lookUp(localPageContext, "ReturnListMenu");
        String formName = "MI-1040";
        ReturnAsset returnAsset = null;
       
        
        //get document from session
        setPageItem(localPageContext, "CSRMenuItem", "Silence.vox");
        logger.debug("Tax Year: " + taxYear);
        String responseXml = lookUp(localPageContext, "ResponseDataXml");
        DOMObject res = null;
        
        if("1".equals(formChoice)) {
            formName = "MI-1040";
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.RESPONSE_1040);
            setPageItem(localPageContext, "ContactNotes", "MI-1040");
	
        }
        else if("2".equals(formChoice)) {
            formName = "MI-1040CR";
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.RESPONSE_1040CR);
            setPageItem(localPageContext, "ContactNotes", "MI-1040CR");            
        }
        else if("3".equals(formChoice)) {
            formName = "MI-1040CR7";
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.RESPONSE_1040CR7);
            setPageItem(localPageContext, "ContactNotes", "MI-1040CR7");  
        }
        else if("4".equals(formChoice)) {
            formName = "MI-1040X";
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.RESPONSE_1040X);
            setPageItem(localPageContext, "ContactNotes", "MI-1040X"); 
        }
        
        try {
            
            res = new DOMObject(new InputSource(new StringReader(responseXml)));
            returnAsset = new ReturnAsset(res);
            returnAsset.getResponse(taxYear, formName, voxFileDir);
              
			ServiceRequest inboundCore = null;
			MiCallAction outboundContactLog = null;
			if(returnAsset.isCheckContactLogs()){
				// check if there is "outbound Correspondence" Contact Log with date after return received date
                // this line is commented out as a work-around for the
				// Siebel error causing IVR to default with CRM_DOWN call type

				//outboundContactLog = getOutboundContactLog(localPageContext, callerSsn, returnAsset.getReceivedDate());
				if(outboundContactLog != null) {
					// check if there are inbound core with date after contact log.
					inboundCore = getInboundCore(localPageContext, outboundContactLog);
					if(inboundCore != null) {
						resultPort = CORE_CHECK;
						addStuff = "";
					}
					else {
						// play new message
						addStuff = IvrUtils.getVXMLScript();
						addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/E201_PendResRtn.vox");
					}
				}
			}

			if(!returnAsset.isCheckContactLogs() || outboundContactLog == null)
			{
				if(returnAsset.isPendingReview()) {
					// retrieve estimate completion date from web service
					Date estDate = returnAsset.getEstimatedCompletionDate();
					if(estDate != null) {
						setPageItem(localPageContext, "EstimatedCompletionDate", IvrUtils.IVR_DATE_FORMAT_SHORT.format(estDate)); 
					}
					if(returnAsset.isEstimatedCompletionDatePassed()) {
						setPageItem(localPageContext, "CSRMenuItem", "235_CSR_Speak.vox");
						setPageItem(localPageContext, "IVR_CALL_TYPE", "PENDING_REVIEW");
						setPageItem(localPageContext, "IvrCallType", "PENDING_REVIEW");
					}
				}
				
				if(returnAsset.hasAdditionalMessage()) {
					callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.RESPONSE_ADDITIONAL);
					setPageItem(localPageContext, "AdditionalResponse", returnAsset.getAdditionalMessage());
				}
				else {
					setPageItem(localPageContext, "AdditionalResponse", "");                
				}
				
				if(returnAsset.hasAdjustedMessage()) {
					resultPort = ADJUSTMENT_RESPONSE;
				}
				
				addStuff = returnAsset.getAddStuff();
			}
            setPageItem(localPageContext, "ReturnResponse", returnAsset.getReturnVox());
        }catch(Exception e) {
            logger.error("Error retrieving return response. " + e.getMessage(), e);
            addStuff = "";
			resultPort = EXCEPTION;
        }
        
        
        // set report call path
        setPageItem(localPageContext, "CallPath", callPath);
        setPageItem(localPageContext, "ReportItem_Application Result", "SUCCESS");  
        setPageItem(localPageContext, "ReportItem_Application Result Reason", callPath);

        
        // set contact log
        setPageItem(localPageContext, "ContactReason", "Where is My Refund");
        setPageItem(localPageContext, "ContactResolution", returnAsset.getContactResolution());

        if(!IvrUtils.isEmpty(addStuff)) {
        	localPageContext.setAttribute("addStuff", addStuff, PageContext.PAGE_SCOPE);
        }
        localPageContext.setAttribute("nAllowType", new Integer(resultPort),
                    PageContext.PAGE_SCOPE);
    }

	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext){


	}

	private MiCallAction getOutboundContactLog(PageContext localPageContext, String account, Date receivedDate) throws Exception {
        String endPoint = AppContext.INSTANCE.getProperty("siebel.webservice.endpoint");
		InsertActivityProxy proxy = new InsertActivityProxy(endPoint);
		MIIvrInsertActivityQueryByExampleInput input = proxy.createQueryRequest();
		MiCallAction inputAction = input.getListOfMiIvrCreateActivity().getMiCallAction().get(0);
		inputAction.setAccountLocation("SN");
		inputAction.setAccountName(account);
		inputAction.setType("Correspondence - Outbound");
		MIIvrInsertActivityQueryByExampleOutput output = proxy.sendQueryRequest(input);
		
		MiCallAction result = null;
		Date minDate = receivedDate;
		for(MiCallAction action : output.getListOfMiIvrCreateActivity().getMiCallAction()) {
			Date dt = IvrUtils.parseDate(action.getCreated());
			if(dt != null && minDate.before(dt)) {
				result = action;
				minDate = dt;
			}
		}

		return result;
	}

	private ServiceRequest getInboundCore(PageContext localPageContext, MiCallAction contactLog) throws Exception {
        String endPoint = AppContext.INSTANCE.getProperty("siebel.webservice.endpoint");
		ServiceRequestProxy proxy = new ServiceRequestProxy(endPoint);
		MIIvrServiceRequestQueryByExampleInput input = proxy.createQueryRequest();
		ServiceRequest inputSr = input.getListOfMiservicerequest().getServiceRequest().get(0);
		inputSr.setAccount(contactLog.getAccountName());
		inputSr.setSource("Correspondence - Inbound");
		MIIvrServiceRequestQueryByExampleOutput output = proxy.sendQueryRequest(input);
		
		ServiceRequest result = null;
		if(output != null) {
			Date minDate = IvrUtils.parseDate(contactLog.getCreated());		
			for(ServiceRequest sr  : output.getListOfMiservicerequest().getServiceRequest()) {
				Date dt = IvrUtils.parseDate(sr.getCreated());
				if(dt != null && minDate.before(dt)) {
					result = sr;
					minDate = dt;
				}
			}
		}
		return result;
	}


	%>
<%@ page import="java.io.*"%>
<%@ page import="java.text.*"%>
<%@ page import="org.xml.sax.*"%>
<%@ page import="org.w3c.dom.*"%>
<%@ page import="org.apache.log4j.Logger"%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.IvrConstants.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.*"%>
<%@ page import="gov.mi.state.treas.ivr.ws.corr.*"%>
<%@ page import="gov.mi.state.treas.ivr.ws.corr.time.*"%>
<%@ page import="gov.mi.state.treas.ivr.ws.activity.*"%>
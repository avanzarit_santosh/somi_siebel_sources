<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"RtnInfoEndMenu.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.	  
	//                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext) {

	}

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.				
	//See the help file for more details		
	public void processAnywhere(PageContext localPageContext) {
        String contactReason = "Copy of Return";
        String contactResolution = "Sent to Process";
        
        String callerSsn = lookUp(localPageContext, "CallerSsn");
        String appId = lookUp(localPageContext, "APPID");
        
        InsertActivityProxy activityProxy = null;
        MIIvrInsertActivityInsertOrUpdateInput input = null;


        // create contact log
        try {
            String endpoint = AppContext.INSTANCE.getProperty("siebel.webservice.endpoint");
            activityProxy = new InsertActivityProxy(endpoint);

            input = activityProxy.createRequest();
            MiCallAction callAction = input.getListOfMiIvrCreateActivity().getMiCallAction().get(0);
            callAction.setAccountName(callerSsn);
            if(appId == null) {
                callAction.setDescription("test " + Calendar.getInstance().getTimeInMillis());              
            }
            else if(appId.endsWith("IVR1")) {
                callAction.setDescription("GVP1 " + Calendar.getInstance().getTimeInMillis());
            }
            else if(appId.endsWith("IVR2")) {
                callAction.setDescription("GVP2 " + Calendar.getInstance().getTimeInMillis());
            }
            else if(appId.endsWith("IVR3")) {
                callAction.setDescription("GVP3 " + Calendar.getInstance().getTimeInMillis());
            }
            else if(appId.endsWith("IVR4")) {
                callAction.setDescription("GVP4 " + Calendar.getInstance().getTimeInMillis());
            }
            else {
                callAction.setDescription("test " + Calendar.getInstance().getTimeInMillis());
            }
            callAction.setCallReason(contactReason);
            callAction.setStatus("Pending Service Request");
          
            callAction.setResolutionCode(contactResolution);

            if (input != null) {
                MIIvrInsertActivityInsertOrUpdateOutput output = activityProxy
                        .sendRequest(input);
            }

        } catch (Exception e) {
            String msg = "Error creating Copy of Return request. " + e.getMessage();
            setLog(localPageContext, 3, msg);

        }
	}

	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext) {

	}%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.IvrConstants.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.*"%>
<%@ page import="gov.mi.state.treas.ivr.ws.activity.*"%>
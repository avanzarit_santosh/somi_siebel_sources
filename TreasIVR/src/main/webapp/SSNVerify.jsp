<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*"%>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();
    String label = "";
    String subDlgReturn = "";
    String query="";
    StringBuffer data = new StringBuffer();

    session.setAttribute("PAGEID" ,"SSNVerify");

    headerVxml.append(getASRLangVxmlHeader(pageContext));

    
	
	String scriptLang= "";
	
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
		scriptLang = "en-US";
	}
	else
	{
		scriptLang = lookUp(pageContext, "APP_LANGUAGE");
	}
	
    
		headerVxml.append("<script src='Languages/");
    
		headerVxml.append(scriptLang);
		headerVxml.append("/PlayBuiltinType.js'/");
    

		headerVxml.append(">");
        out.print(headerVxml);
        headerVxml.setLength(0);
    
%>
    <script>
		var f = new Format();var pb; var i;
		</script>
<form>
   <block name="Flush">
      <prompt bargein="false">
         <audio src="StudioIncludes/silence10ms.wav"/>
      </prompt>
   </block><block name="SSNVerifyP0" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("306_Auth_Verify_AGIHHI01.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><block name="SSNVerifyP1" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
		data.append("<foreach item='thePrompt' array='PlayBuiltinType(\"");
        data.append(encodeForBuiltinPlay(getActualValue(pageContext,"GCT:SSNInput")));
        data.append("\",\"");
        data.append("alphanumeric");
	
		data.append("\")'>");
	    
	out.print(data);
  	data.setLength(0);
	%><audio expr="thePrompt"/></foreach></prompt></block><block name="SSNVerifyP2" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("306_Auth_Verify_AGIHHI02.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><var name="retryCount" expr="1"/><field  name="SSNVerify" ><prompt bargein="true" timeout="3s"/>
   <option dtmf="1"/>
   <option dtmf="2"/>
   <option dtmf="*"/></field><filled mode="all" namelist="SSNVerify">
      <if cond="SSNVerify == 1">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AGIHHIInput.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='SSNVerify SSNVerify$.inputmode SSNVerify$.confidence SSNVerify$.utterance SSNVerify$.markname SSNVerify$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="SSNVerify == 2">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("SSNInput.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='SSNVerify SSNVerify$.inputmode SSNVerify$.confidence SSNVerify$.utterance SSNVerify$.markname SSNVerify$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="SSNVerify == '*'">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AuthMenuRp1.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='SSNVerify SSNVerify$.inputmode SSNVerify$.confidence SSNVerify$.utterance SSNVerify$.markname SSNVerify$.marktime '" );

out.print( "/>" );
%>
</if>
   </filled>
   <catch event="noinput" cond="retryCount == 1">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("616_Info_Not Processed.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="SSNVerifyP0 SSNVerifyP1 SSNVerifyP2 " /></catch>
   <catch event="nomatch" cond="retryCount == 1">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("615_Info_Incomplete_Invalid_Try Again.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="SSNVerifyP0 SSNVerifyP1 SSNVerifyP2 " /></catch>
   <catch event="noinput" cond="retryCount == 2">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("616_Info_Not Processed.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="SSNVerifyP0 SSNVerifyP1 SSNVerifyP2 " /></catch>
   <catch event="nomatch" cond="retryCount == 2">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("615_Info_Incomplete_Invalid_Try Again.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="SSNVerifyP0 SSNVerifyP1 SSNVerifyP2 " /></catch>
   <catch event="nomatch" cond="retryCount == 3">
      <throw event="com.genesys.studio.toomanynomatches"/>
   </catch>
   <catch event="noinput" cond="retryCount == 3">
      <throw event="com.genesys.studio.toomanynoinputs"/>
   </catch>
</form>
<catch event="com.genesys.studio.toomanynoinputs">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("GetBetterInfoMenu.jsp"));

out.print( "'/>" );
%>
</catch>
<catch event="com.genesys.studio.toomanynomatches">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("GetBetterInfoMenu.jsp"));

out.print( "'/>" );
%>
</catch>

<%
    processAnywhere(pageContext);

    out.print(getVxmlFooter());
    
    processEnd(pageContext);

%>
<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
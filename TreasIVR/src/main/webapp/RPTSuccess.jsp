<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*"%>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();
    String label = "";
    StringBuffer data = new StringBuffer();

    session.setAttribute("PAGEID", "RPTSuccess");

    headerVxml.append(getVxmlHeader(pageContext));
	
    
	
	String scriptLang= "";
	
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
		scriptLang = "en-US";
	}
	else
	{
		scriptLang = lookUp(pageContext, "APP_LANGUAGE");
	}
	
    
		headerVxml.append("<script src='Languages/");
    
		headerVxml.append(scriptLang);
		headerVxml.append("/PlayBuiltinType.js'/");
    

		headerVxml.append(">");
        out.print(headerVxml);
        headerVxml.setLength(0);
    
%>
    <script>
		var f = new Format();var pb; var i;
		</script>
<form>
   <grammar mode="dtmf" version="1.0" root="fundX" type="application/srgs+xml" xml:lang="en-US">
      <rule scope="public" id="fundX">
         <one-of>
            <item>0 0 0 0 0 0 1</item>
         </one-of>
      </rule>
   </grammar><block name="P0" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("212_TaxRtnInfoPress1_01.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><block name="P1" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
		data.append("<foreach item='thePrompt' array='PlayBuiltinType(\"");
        data.append(encodeForBuiltinPlay(getActualValue(pageContext,"GCT:EfileReturnProcessDate")));
        data.append("\",\"");
        data.append("date");
	
		data.append("\",");
		data.append("f.SPEAK_DAY | f.SPEAK_MONTH | f.SPEAK_YEAR");
		
		data.append(")'>");
	    
	out.print(data);
  	data.setLength(0);
	%><audio expr="thePrompt"/></foreach></prompt></block><block name="P2" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("212_TaxRtnInfoPress1_02.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><block name="P3" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
		data.append("<foreach item='thePrompt' array='PlayBuiltinType(\"");
        data.append(encodeForBuiltinPlay(getActualValue(pageContext,"GCT:PaperReturnProcessDate")));
        data.append("\",\"");
        data.append("date");
	
		data.append("\",");
		data.append("f.SPEAK_DAY | f.SPEAK_MONTH | f.SPEAK_YEAR");
		
		data.append(")'>");
	    
	out.print(data);
  	data.setLength(0);
	%><audio expr="thePrompt"/></foreach></prompt></block><block name="P4" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("212_TaxRtnInfoPress1_03.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><field  name="TmpFieldName" ><prompt timeout="0s" xml:lang="en-US"/></field><catch event="nomatch noinput">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("TaxRtnInfoAuthSub.jsp"));

out.print( "'/>" );
%>
</catch>
</form>
<%
    processAnywhere(pageContext);

    out.print(getVxmlFooter());

    processEnd(pageContext);

%>
<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
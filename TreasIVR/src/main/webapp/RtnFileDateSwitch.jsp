<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"RtnEfileNotComplete.jsp","RtnPaperFileNotComplete.jsp","TaxInfoTransfer.jsp","TaxInfoTransfer.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.	  
	//                    Can be used o Access the HTTP Objects 
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext) {

	}

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.				
	//See the help file for more details		
	public void processAnywhere(PageContext localPageContext) {
        final int EFILE_RETURN_NOT_COMPLETE = 1;
        final int PAPER_RETURN_NOT_COMPLETE = 2;
		final int EST_DATE_PASSED = 3;
        final int EXCEPTION = 4;
        
		final int DEFAULT_ESTIMATE = 8;
		String dateStr = lookUp(localPageContext, "RtnFileDateInput");

        Date curDate = Calendar.getInstance().getTime();
        Date estCompleteDate = null;
        Calendar cal = Calendar.getInstance();
		
        String callPath = lookUp(localPageContext, "CallPath");
        String ssn = lookUp(localPageContext, "CallerSsn");
		String fileChoice = lookUp(localPageContext, "ReturnMethodMenu");
		String fileMethod = "1".equals(fileChoice) ? "Efile" : "Paper";
        int resultPort = "1".equals(fileChoice) ? EFILE_RETURN_NOT_COMPLETE : PAPER_RETURN_NOT_COMPLETE;
        setPageItem(localPageContext, "ContactResolution", "No Record of Return"); 

        boolean estimateDatePassed = false;
        String locator = "";
        Date fileDate = null;

        try {
        	locator = "parsing input file date";
            cal.set(Calendar.MONTH, Integer.parseInt(dateStr.substring(0, 2)) - 1);
            cal.set(Calendar.DATE, Integer.parseInt(dateStr.substring(2, 4)));
            fileDate = cal.getTime();
            
            locator = "retrieving estimated completion date";            

            String endpoint = AppContext.INSTANCE.getProperty("siebel.webservice.endpoint");
            CorrTimeFrameProxy timeProxy = new CorrTimeFrameProxy(endpoint);
            MIIvrCorrTimeFrameQueryByExampleInput timeReq = timeProxy.createRequest();
            MiIitCompletionDate cd = timeReq.getListOfMiIvrErrorcodeReturnDate().getMiIitCompletionDate().get(0);
            cd.setType("Error Free Return");
            cd.setReturnType(fileMethod);
            cd.setErrorCode("0");
            MIIvrCorrTimeFrameQueryByExampleOutput timeRes = timeProxy.sendRequest(timeReq);
            estCompleteDate = timeProxy.getEstimateReceiveDate(timeRes, fileDate);
        } catch(Exception e) {
            setLog(localPageContext, 0, "Warning " + locator + ". " + e.getMessage());
            resultPort = EXCEPTION;
            setPageItem(localPageContext, "IvrCallType", "NO_RECORD");
            setPageItem(localPageContext, "ContactReason", "");
            setPageItem(localPageContext, "ContactResolution", "");
        }
        
        if(estCompleteDate == null) {               
            cal.add(Calendar.WEEK_OF_MONTH, DEFAULT_ESTIMATE);
            estCompleteDate = cal.getTime();
        }
        
        if(curDate.after(estCompleteDate)) {
            estimateDatePassed = true;
            resultPort = EST_DATE_PASSED;
            setPageItem(localPageContext, "IvrCallType", "NO_RECORD");
            setPageItem(localPageContext, "ContactReason", "");
            setPageItem(localPageContext, "ContactResolution", "");
        } 
        
        // report call path
        if(resultPort == EFILE_RETURN_NOT_COMPLETE) {
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.EFILE_RETURN_NOT_PROCESSED);	
            setPageItem(localPageContext, "CallPath", callPath);
            setPageItem(localPageContext, "ReportItem_Application Result", "SUCCESS");  
            setPageItem(localPageContext, "ReportItem_Application Result Reason", callPath);        
        }
        else if(resultPort == PAPER_RETURN_NOT_COMPLETE) {
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.EFILE_RETURN_NOT_PROCESSED);           	
            setPageItem(localPageContext, "CallPath", callPath);
            setPageItem(localPageContext, "ReportItem_Application Result", "SUCCESS");  
            setPageItem(localPageContext, "ReportItem_Application Result Reason", callPath);        
        }
                                                        
        		
        setPageItem(localPageContext, "EstimatedCompleteDate", IvrUtils.getDateString(estCompleteDate));
        localPageContext.setAttribute("nAllowType", new Integer(resultPort), PageContext.PAGE_SCOPE);
	}

	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext) {

	}
%>
<%@ page import="java.text.*"%>
<%@ page import="org.xml.sax.*"%>
<%@ page import="org.w3c.dom.*"%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.IvrConstants.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.*"%>
<%@ page import="gov.mi.state.treas.ivr.ws.corr.time.*"%>
<%@ page import="gov.mi.state.treas.ivr.ws.asset.*"%>
<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output omit-xml-declaration="yes" method="xml"/>
<xsl:param name="application"></xsl:param>
<xsl:param name="nextURI">stop</xsl:param>
<xsl:param name="fileType">.asp</xsl:param>
<xsl:param name="language">en-US</xsl:param>
<xsl:param name="voxDir"></xsl:param>
<xsl:param name="allowColumnNames">TRUE</xsl:param>
<xsl:param name="allowBargein">true</xsl:param>
<xsl:param name="addBookmark">FALSE</xsl:param>
<xsl:param name="pauseTime">500</xsl:param>
<xsl:param name="useTTS">FALSE</xsl:param>

<xsl:template match="/">
<vxml version='2.1' xmlns='http://www.w3.org/2001/vxml'>
<xsl:attribute name="application"><xsl:value-of select="$application"/></xsl:attribute>
<xsl:attribute name="xml:lang"><xsl:value-of select="$language"/></xsl:attribute>

<!-- put PlayConstantsTTS.js if useTTS is TRUE -->

<xsl:if test="$useTTS = 'TRUE'">
<script>
<xsl:attribute name="src">
<xsl:value-of select="'Languages/'"/>
<xsl:value-of select="$language"/>
<xsl:value-of select="'/PlayConstantsTTS.js'"/>
</xsl:attribute>
</script>
</xsl:if>

<!-- put PlayBuiltinTypeTTS.js if useTTS is TRUE -->
<!-- put PlayBuiltinType.js if useTTS is FALSE -->

<script>
<xsl:attribute name="src">
<xsl:value-of select="'Languages/'"/>
<xsl:value-of select="$language"/>
<xsl:choose>
<xsl:when test="$useTTS = 'TRUE'">
<xsl:value-of select="'/PlayBuiltinTypeTTS.js'"/>
</xsl:when>
<xsl:otherwise>
<xsl:value-of select="'/PlayBuiltinType.js'"/>
</xsl:otherwise>
</xsl:choose>
</xsl:attribute>
</script>

<form>
        <block>
		 <prompt>
		 <xsl:attribute name="bargein"><xsl:value-of select="$allowBargein"/></xsl:attribute>
		
		 <!-- get the first column name -->

		 <xsl:variable name="firstColName" select="name(/RECORDSET/RECORD/*[position() = 1])"/>
		 		
         <xsl:for-each select="/RECORDSET/RECORD/*">
			
		   <xsl:variable name="colName" select="name(.)"/>
			
		   <!-- check say column name flag -->

		   <xsl:if test="$allowColumnNames = 'TRUE'">
	          <xsl:value-of select="$colName"/>
			  <xsl:call-template name="createBreakTag">
			    <xsl:with-param name="pauseTime" select="$pauseTime"/>
			  </xsl:call-template>
 		   </xsl:if>

		   <!-- check bookmark flag -->
		   <!-- Put bookmark tag after each record as first columnvalue and only if it is a numeric value -->
		   <!-- At present Mark is only supported for Nuance SWMS and must be numeric -->

		   <xsl:if test="($addBookmark = 'TRUE') and ($colName = $firstColName) and (string(number(.)) != 'NaN')">
		   <mark>
		   <xsl:attribute name="name"><xsl:value-of select="."/></xsl:attribute>
		   </mark>
		   </xsl:if>

		   <!-- ********************* -->
		   <!-- this is for text type -->
		   <!-- ********************* -->
		   <xsl:if test="@TYPE='text'">
	     	         <xsl:choose>
						<xsl:when test="contains(.,'.vox') or contains(.,'.wav')">
 								<audio src="{$voxDir}/{.}"/>       							
						</xsl:when>
						<xsl:otherwise>
					            <xsl:value-of select='.'/>
						</xsl:otherwise>
                 </xsl:choose>
 		   </xsl:if>
		   <!-- *********************** -->
		   <!-- this is for number type -->
		   <!-- *********************** -->
		   <xsl:if test="@TYPE='number'">
			   <foreach item='thePrompt' array='PlayBuiltinType("{.}","number")'>
			   		<xsl:call-template name="createAudioORValueTag">
			   		<xsl:with-param name="useTTS" select="$useTTS"/>
			   		</xsl:call-template>
			   </foreach>
		   </xsl:if>
		   <!-- ************************* -->
		   <!-- this is for currency type -->
		   <!-- ************************* -->
		   <xsl:if test="@TYPE='currency'">
			   <foreach item='thePrompt' array='PlayBuiltinType("{.}","currency")'>
			   		<xsl:call-template name="createAudioORValueTag">
			   		<xsl:with-param name="useTTS" select="$useTTS"/>
			   		</xsl:call-template>
			   </foreach>
		   </xsl:if>
		   <!-- ********************* -->
		   <!-- this is for date type -->
		   <!-- ********************* -->
		   <xsl:if test="@TYPE='date'">
			<xsl:call-template name="extractDate">
			<xsl:with-param name="data" select="."/>
			</xsl:call-template>
		   </xsl:if>
		   <!-- ********************* -->
		   <!-- this is for time type -->
		   <!-- ********************* -->
		   <xsl:if test="@TYPE='time'">
			<xsl:call-template name="extractTime">
			<xsl:with-param name="data" select="."/>
			</xsl:call-template>
		   </xsl:if>
		   <!-- ********************* -->
		   <!-- this is for time type -->
		   <!-- ********************* -->
		   <xsl:if test="@TYPE='datetime'">
			<xsl:call-template name="extractDate">
			<xsl:with-param name="data" select="substring-before(.,' ')"/>
			</xsl:call-template>	
			<xsl:call-template name="extractTime">
			<xsl:with-param name="data" select="substring-after(.,' ')"/>
			</xsl:call-template>
		   </xsl:if>

			<!-- break tag after each column -->
			
			<xsl:call-template name="createBreakTag">
			<xsl:with-param name="pauseTime" select="$pauseTime"/>
			</xsl:call-template>

       	</xsl:for-each>
   	    </prompt>
   		<goto>
                 <xsl:attribute name="next">
                 <xsl:value-of select="$nextURI"/><xsl:value-of select="$fileType"/>
                </xsl:attribute>
     	</goto>
    </block>
</form>
</vxml>
</xsl:template>

<!-- extract Date -->

<xsl:template name="extractDate">
<xsl:param name="data"/>
<xsl:variable name="normalizedDate">
<xsl:choose>
<xsl:when test="contains($data,'/')">
	<xsl:call-template name="normalizeDate">
	<xsl:with-param name="data" select="$data"/>
	<xsl:with-param name="token">/</xsl:with-param>
	</xsl:call-template>
</xsl:when>
<xsl:when test="contains($data,'-')">
	<xsl:call-template name="normalizeDate">
	<xsl:with-param name="data" select="$data"/>
	<xsl:with-param name="token">-</xsl:with-param>
	</xsl:call-template>
</xsl:when>
<xsl:otherwise>
	<xsl:value-of select="$data"/>
</xsl:otherwise>
</xsl:choose>
</xsl:variable>
<foreach item='thePrompt' array='PlayBuiltinType("{normalize-space($normalizedDate)}","date",f.SPEAK_YEAR | f.SPEAK_DAY | f.SPEAK_MONTH)'>
	<xsl:call-template name="createAudioORValueTag">
	<xsl:with-param name="useTTS" select="$useTTS"/>
	</xsl:call-template>
</foreach>
</xsl:template>

<!-- normalize Date -->

<xsl:template name="normalizeDate">
<xsl:param name="data"/>
<xsl:param name="token"/>
    <xsl:choose>
    <!-- set the year -->
	<xsl:when test="substring-before($data,'200') != ''">
	200<xsl:value-of select="substring-after($data,'200')"/>
	<!-- set the month with zero adjustment -->
	<xsl:if test="string-length(substring-before(substring-before($data,'200'),$token)) = 1">0</xsl:if><xsl:value-of select="substring-before(substring-before($data,'200'),$token)"/>
	<!-- set the day with zero adjustment -->
	<xsl:if test="string-length(translate(substring-after(substring-before($data,'200'),$token),$token,'')) = 1">0</xsl:if><xsl:value-of select="translate(substring-after(substring-before($data,'200'),$token),$token,'')"/>
	</xsl:when>
	<xsl:otherwise>
	<xsl:value-of select="translate($data,$token,'')"/>
	</xsl:otherwise>
   </xsl:choose>
</xsl:template>

<!-- extract Time -->

<xsl:template name="extractTime">
<xsl:param name="data"/>
			   <xsl:variable name="tempTime3">
			   <xsl:value-of select="translate($data,' ','')"/>
			   </xsl:variable>
			   <xsl:variable name="tempTime">
			   <xsl:value-of select="translate($tempTime3,':','')"/>
			   </xsl:variable>
		   	   <xsl:variable name="tempTime2">
				<xsl:choose>
				<xsl:when test="contains($tempTime,'AM')">
				<xsl:if test="string-length(translate($tempTime,'AM','a')) != 7">0</xsl:if><xsl:value-of select="translate($tempTime,'AM','a')"/>
				</xsl:when>
				<xsl:when test="contains($tempTime,'PM')">
				<xsl:if test="string-length(translate($tempTime,'PM','a')) != 7">0</xsl:if><xsl:value-of select="translate($tempTime,'PM','p')"/>
				</xsl:when>
				<xsl:otherwise>
				<xsl:choose>
				<xsl:when test="string-length($tempTime) != 6">
				0<xsl:value-of select="substring($tempTime,0,4)"/>?
				</xsl:when>
				<xsl:otherwise>
				<xsl:value-of select="substring($tempTime,0,5)"/>?
				</xsl:otherwise>
				</xsl:choose>
				<!--<xsl:if test="string-length($tempTime) != 6">0</xsl:if><xsl:value-of select="substring($tempTime,0,5)"/>?-->
				</xsl:otherwise>
				</xsl:choose>
		   	   </xsl:variable>
			   
               <foreach item='thePrompt' array='PlayBuiltinType("{normalize-space($tempTime2)}","time")'>
			   	<xsl:call-template name="createAudioORValueTag">
			   	<xsl:with-param name="useTTS" select="$useTTS"/>
			   	</xsl:call-template>
			   </foreach>
</xsl:template>

<!--   Create break tag -->

<xsl:template name="createBreakTag">
<xsl:param name="pauseTime"/>
	<xsl:if test="$pauseTime != '0'">
	<xsl:text disable-output-escaping="yes">&lt;break time='</xsl:text>
	<xsl:value-of select="$pauseTime"/>
	<xsl:text disable-output-escaping="yes">ms'/&gt;</xsl:text>
    </xsl:if>
</xsl:template>

<!--   Create Audio or Value tag -->

<xsl:template name="createAudioORValueTag">
<xsl:param name="useTTS"/>
	<xsl:choose>
	<xsl:when test="$useTTS = 'TRUE'">
	<xsl:text disable-output-escaping="yes">&lt;value expr='thePrompt'/&gt;</xsl:text>
	</xsl:when>
	<xsl:otherwise>
	<xsl:text disable-output-escaping="yes">&lt;audio expr='thePrompt'/&gt;</xsl:text>
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>
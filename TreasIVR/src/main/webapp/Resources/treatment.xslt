<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="utf-8" omit-xml-declaration="yes"/>
<xsl:param name="application"></xsl:param>
<xsl:param name="scriptID"></xsl:param>
<xsl:param name="scriptData"></xsl:param>
<xsl:param name="fileType"></xsl:param>
<xsl:param name="language">en-US</xsl:param>
<xsl:param name="voxDir"></xsl:param>

<xsl:template match="/">
<xsl:variable name="label" select="main/label"/>
<xsl:variable name="nReturn" select="main/return"/>
<xsl:variable name="nextPage" select="main/nextPage"/>
<xsl:variable name="throwException" select="main/error"/>

<vxml version='2.1' xmlns='http://www.w3.org/2001/vxml'>
<xsl:attribute name="application"><xsl:value-of select="$application"/></xsl:attribute>
<xsl:attribute name="xml:lang"><xsl:value-of select="$language"/></xsl:attribute>
<form>


<xsl:choose>
<xsl:when test="$scriptID = 'PlayAnnounce'">
    <block>
        <xsl:call-template name="messageBuilderForTab1">
            <xsl:with-param name="data" select="$scriptData"/>
        </xsl:call-template> 
        <xsl:call-template name="messageBuilderForTab2">
            <xsl:with-param name="data" select="$scriptData"/>
        </xsl:call-template>
    </block>
    <block>
        <goto>
             <xsl:attribute name="next">
              <xsl:choose>
                  <xsl:when test="$nReturn = 0"><xsl:value-of select="$nextPage"/><xsl:value-of select="$fileType"/></xsl:when>
                  <xsl:otherwise><xsl:value-of select="$label"/><xsl:value-of select="$fileType"/>?RETURN_TO_STRATEGY=TRUE</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
        </goto>
    </block>
</xsl:when>
<xsl:when test="$scriptID = 'PlayAnnounceAndDigits'">
    <!-- extract the max digits info. -->
    <xsl:variable name="maxDigits">
	    <xsl:call-template name="getParameter">
	    	<xsl:with-param name="scriptData" select="$scriptData"/>
	    	<xsl:with-param name="paramName">MAX_DIGITS</xsl:with-param>
	    	<xsl:with-param name="defaultValue">1</xsl:with-param>
	    </xsl:call-template>
    </xsl:variable>
    <!-- extract inter-digit timeout info. -->
    <xsl:variable name="interDigitTimeout">
	    <xsl:call-template name="getParameter">
	    	<xsl:with-param name="scriptData" select="$scriptData"/>
	    	<xsl:with-param name="paramName">DIGIT_TIMEOUT</xsl:with-param>
	    	<xsl:with-param name="defaultValue">1</xsl:with-param>
	    </xsl:call-template>
	    <xsl:text>s</xsl:text>
    </xsl:variable>
    <property name="interdigittimeout">
    <xsl:attribute name="value"><xsl:value-of select="$interDigitTimeout"/></xsl:attribute>
    </property>
    <!-- extract termination digits info. -->
    <xsl:variable name="terminationDigits">
	    <xsl:call-template name="getParameter">
	    	<xsl:with-param name="scriptData" select="$scriptData"/>
	    	<xsl:with-param name="paramName">TERM_DIGITS</xsl:with-param>
	    </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$terminationDigits != ''">
        <property name="termchar">
        <xsl:attribute name="value"><xsl:value-of select="$terminationDigits"/></xsl:attribute>
        </property>
    </xsl:if>
    <!-- extract total timeout info. -->
    <xsl:variable name="pauseTime">
	    <xsl:call-template name="getParameter">
	    	<xsl:with-param name="scriptData" select="$scriptData"/>
	    	<xsl:with-param name="paramName">TOTAL_TIMEOUT</xsl:with-param>
	    	<xsl:with-param name="defaultValue">5</xsl:with-param>
	    </xsl:call-template>
	    <xsl:text>s</xsl:text>
    </xsl:variable>
    	<field>
        		<xsl:attribute name="name"><xsl:value-of select="$label"/></xsl:attribute>
  				<xsl:variable name="url" select="concat('builtin:dtmf/digits?minlength=1;maxlength=',$maxDigits)"/>
		        <grammar mode="dtmf">
		        	<xsl:attribute name="xml:lang"><xsl:value-of select="$language"/></xsl:attribute>
					<xsl:attribute name="src"><xsl:value-of select="$url"/></xsl:attribute>
		        </grammar>

                <xsl:call-template name="messageBuilderForTab1">
                    <xsl:with-param name="data" select="$scriptData"/>
                </xsl:call-template>
                <xsl:call-template name="messageBuilderForTab2">
                    <xsl:with-param name="data" select="$scriptData"/>
                </xsl:call-template>
        		<prompt>
                    <xsl:attribute name="timeout"><xsl:value-of select="$pauseTime"/></xsl:attribute>
                </prompt>
				
				<noinput><reprompt/></noinput>
				<nomatch><prompt>Sorry.  I did not understand.</prompt><reprompt/></nomatch>
        </field>
        <filled>
            <submit method="post">
            <xsl:attribute name="namelist"><xsl:value-of select="$label"/></xsl:attribute>
            <xsl:attribute name="next">
            <xsl:choose>
                <xsl:when test="$nReturn = 0"><xsl:value-of select="$nextPage"/><xsl:value-of select="$fileType"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="$label"/><xsl:value-of select="$fileType"/>?RETURN_TO_STRATEGY=TRUE</xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
        </submit>
        </filled>
</xsl:when>
<xsl:when test="$scriptID = 'Music'">
    <block>
        <prompt>
        <xsl:if test="normalize-space($voxDir) != ''"> 
            <xsl:variable name="musicFile">
			    <xsl:call-template name="getParameter">
			    	<xsl:with-param name="scriptData" select="$scriptData"/>
			    	<xsl:with-param name="paramName">MUSIC_DN</xsl:with-param>
			    </xsl:call-template>
            </xsl:variable>
            <xsl:if test="$scriptData != ''">
            <xsl:if test="normalize-space($musicFile) != ''">
            <audio>
            <xsl:choose>
                <xsl:when test="contains($musicFile,'http://')">
                   <xsl:attribute name="src"><xsl:value-of select="$musicFile"/></xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                   <xsl:attribute name="src"><xsl:value-of select="concat($voxDir,'/',$musicFile)"/></xsl:attribute>
                </xsl:otherwise>
            </xsl:choose>
            </audio>
            </xsl:if>
            </xsl:if>
        </xsl:if>
        </prompt>
        <goto>
             <xsl:attribute name="next">
              <xsl:choose>
                  <xsl:when test="$nReturn = 0"><xsl:value-of select="$nextPage"/><xsl:value-of select="$fileType"/></xsl:when>
                  <xsl:otherwise><xsl:value-of select="$label"/><xsl:value-of select="$fileType"/>?RETURN_TO_STRATEGY=TRUE</xsl:otherwise>
              </xsl:choose>
            </xsl:attribute>
        </goto>
    </block>
</xsl:when>
<!-- the following is thrown in-case of un-supported TREATMENT -->
<xsl:otherwise>
    <xsl:choose>
        <xsl:when test="$throwException = 1">
            <block><throw event="error.unsupported" message="Treatment not supported"/></block>
        </xsl:when>
        <xsl:otherwise>
           <xsl:if test="$nReturn = 0">
            <block>
                <goto>
                     <xsl:attribute name="next">
                     <xsl:value-of select="$nextPage"/><xsl:value-of select="$fileType"/>
                    </xsl:attribute>
                </goto>
            </block>
           </xsl:if>
        </xsl:otherwise>
    </xsl:choose>
</xsl:otherwise>
</xsl:choose>
</form>
</vxml>
</xsl:template>
<xsl:template name="messageBuilderForTab1">
<xsl:param name="data"></xsl:param>
    <xsl:if test="$data != ''">
        <xsl:if test="contains($data,'MSGTXT:')">
            <xsl:choose>
                <xsl:when test="contains(substring-after($data,'MSGTXT:'),',')">
                    <prompt xmlns='http://www.w3.org/2001/vxml'><xsl:value-of select="substring-before(substring-after($data,'MSGTXT:'),',')"/></prompt>
                    <xsl:if test="contains(substring-after($data,'MSGTXT:'),'MSGTXT:')">
                        <xsl:call-template name="messageBuilderForTab1">
                            <xsl:with-param name="data" select="substring-after($data,'MSGTXT:')"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    <prompt xmlns='http://www.w3.org/2001/vxml'><xsl:value-of select="substring-after($data,'MSGTXT:')"/></prompt>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:if>
</xsl:template>
<xsl:template name="messageBuilderForTab2">
<xsl:param name="data"></xsl:param>
    <xsl:if test="$data != ''">
        <xsl:if test="contains($data,'TEXT:')">
            <xsl:choose>
                <xsl:when test="contains(substring-after($data,'TEXT:'),',')">
                    <prompt xmlns='http://www.w3.org/2001/vxml'><xsl:value-of select="substring-before(substring-after($data,'TEXT:'),',')"/></prompt>
                    <xsl:if test="contains(substring-after($data,'TEXT:'),'TEXT:')">
                        <xsl:call-template name="messageBuilderForTab2">
                            <xsl:with-param name="data" select="substring-after($data,'TEXT:')"/>
                        </xsl:call-template>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    <prompt xmlns='http://www.w3.org/2001/vxml'><xsl:value-of select="substring-after($data,'TEXT:')"/></prompt>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:if>
</xsl:template>

<xsl:template name="getParameter">
	<xsl:param name="scriptData"></xsl:param>
	<xsl:param name="paramName"></xsl:param>
	<xsl:param name="defaultValue"></xsl:param>
    <xsl:choose>
    <xsl:when test="contains($scriptData, concat($paramName, ':'))">
        <xsl:choose>
            <xsl:when test="contains(substring-after($scriptData,concat($paramName, ':')),',')"><xsl:value-of select="substring-before(substring-after($scriptData,concat($paramName, ':')),',')"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="normalize-space(substring-after($scriptData,concat($paramName, ':')))"/></xsl:otherwise>
        </xsl:choose>
    </xsl:when>
    <xsl:when test="string-length($defaultValue) > 0"><xsl:value-of select="$defaultValue"/></xsl:when>
    </xsl:choose>
</xsl:template>
</xsl:stylesheet>
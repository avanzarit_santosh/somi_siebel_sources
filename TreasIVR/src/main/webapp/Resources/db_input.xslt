<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
	<xsl:output omit-xml-declaration="yes" method="xml"/>
	<xsl:param name="language">en-US</xsl:param>
	<xsl:param name="dtmf">0</xsl:param>
	<xsl:param name="mappingType">0</xsl:param>
	
	<xsl:template match="/RECORDSET">
		<xsl:for-each select="/RECORDSET/RECORD">
			<xsl:variable name="Column1" select="child::*[position() = 1]"/>
			<xsl:variable name="Column2" select="child::*[position() = 2]"/>
			<option>
				<xsl:call-template name="getDTMF">
					<xsl:with-param name="Column1" select="$Column1" />
				</xsl:call-template>
				<xsl:call-template name="getMapping">
					<xsl:with-param name="Column1" select="$Column1" />
					<xsl:with-param name="Column2" select="$Column2" />
				</xsl:call-template>
				<xsl:call-template name="getPhrase">
					<xsl:with-param name="Column1" select="$Column1" />
				</xsl:call-template>
			</option>
			
		</xsl:for-each>
	</xsl:template>
	
	<xsl:template name="getMapping">
		<xsl:param name="Column1"/>
		<xsl:param name="Column2"/>
		<!-- check for mapping type -->
		<xsl:if test="$mappingType = 1">
			<!-- If column2 is empty return value = Column1 else return = Column2 -->
			<xsl:choose>
				<xsl:when test="$Column2 != ''">
					<xsl:attribute name="value">
						<xsl:call-template name="replaceString">
							<xsl:with-param name="haystack" select="$Column2"/>
							<xsl:with-param name="needle" select="'&amp;apos;'" />
							<xsl:with-param name="value" select="''" />
						</xsl:call-template>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="value">
						<xsl:call-template name="replaceString">
							<xsl:with-param name="haystack" select="$Column1"/>
							<xsl:with-param name="needle" select="'&amp;apos;'" />
							<xsl:with-param name="value" select="''" />
						</xsl:call-template>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="getDTMF">
		<xsl:param name="Column1"/>
		<!-- check for DTMF -->
		<xsl:if test="$dtmf = 1">
			<!-- Generate only if valid DTMF -->
			<xsl:if test="string(number($Column1)) != 'NaN' and number($Column1) = floor($Column1)">
				<xsl:attribute name="dtmf">
					<xsl:value-of select="$Column1" />
				</xsl:attribute>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="getPhrase">
		<xsl:param name="Column1"/>
		<!-- If DTMF split characters for Speech input -->
		<xsl:choose>
			<xsl:when test="$dtmf = 1 and string(number($Column1)) != 'NaN' and number($Column1) = floor($Column1)">
				<xsl:call-template name="splitCharacters">
					<xsl:with-param name="string" select="$Column1" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="replaceString">
					<xsl:with-param name="haystack" select="$Column1"/>
					<xsl:with-param name="needle" select="'&amp;apos;'" />
					<xsl:with-param name="value" select="''" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template name="splitCharacters">
		<xsl:param name="string" />
		<xsl:if test="$string">
			<xsl:value-of select="substring($string, 1, 1)" />
			<xsl:text disable-output-escaping="yes"> </xsl:text>
			<xsl:call-template name="splitCharacters">
				<xsl:with-param name="string"
                         select="substring($string, 2)" />
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
	<xsl:template name="replaceString">
		<xsl:param name="haystack" />
		<xsl:param name="needle" />
		<xsl:param name="value" />
		<xsl:choose>
			<xsl:when test="substring-before($haystack, $needle) != '' or substring-after($haystack, $needle) != '' ">
				<xsl:value-of select="substring-before($haystack, $needle)" />
				<xsl:if test="string-length($haystack) != string-length(substring-before($haystack, $needle))" >
					<xsl:value-of select="$value" />
					<xsl:call-template name="replaceString">
						<xsl:with-param name="haystack" select="substring-after($haystack, $needle)" />
						<xsl:with-param name="needle" select="$needle" />
						<xsl:with-param name="value" select="$value" />
					</xsl:call-template>
				</xsl:if>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$haystack" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
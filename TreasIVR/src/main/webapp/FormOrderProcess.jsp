<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"Form1040ConfirmMsg.jsp","FormCR7ConfirmMsg.jsp","FormBothConfirmMsg.jsp","FormCR7NAMsg.jsp","FormBothNAMsg.jsp","FormsSystemDown.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.	  
	//                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext) {

	}

    Logger logger = Logger.getLogger(getClass());

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.				
	//See the help file for more details		
	public void processAnywhere(PageContext localPageContext) {
        final int FORM_1040_CONFIRM = 1;
        final int FORM_CR7_CONFIRM = 2;
        final int FORM_BOTH_CONFIRM = 3;
        final int FORM_CR7_NA = 4;
        final int FORM_BOTH_NA = 5;
        final int EXCEPTION = 6;
        int resultPort = 1;


        // connect to DB
        Connection con = null;
        String ssn = lookUp(localPageContext, "FormsSSNInput");
        String formChoice = lookUp(localPageContext, "FormChoicesMenu");
        try {
            Calendar cal = Calendar.getInstance();
            boolean orderValid = false;
            boolean form1040 = false;
            boolean formCR7 = false;
            boolean octOrNov = (cal.get(Calendar.MONTH) == Calendar.OCTOBER 
            		|| cal.get(Calendar.MONTH) == Calendar.NOVEMBER);
            if("3".equals(formChoice)) {
                // confirm order
                form1040 = true;
                formCR7 = true;
               	orderValid = !octOrNov;
               	resultPort = orderValid ? FORM_BOTH_CONFIRM : FORM_BOTH_NA;
            }
            else if("2".equals(formChoice)) {
            	form1040 = false;
            	formCR7 = true;
                orderValid = !octOrNov;
                resultPort = orderValid ? FORM_CR7_CONFIRM : FORM_CR7_NA;
            }
            else if("1".equals(formChoice)) {
            	// confirm order
            	form1040 = true;
            	formCR7 = false;
            	orderValid = true;
            	resultPort = FORM_1040_CONFIRM;
            }
                        
            // place order by insert request into forms_2_me table
            if(orderValid) {
                Class.forName(AppContext.INSTANCE.getProperty("forms2me.db.driver"));
                con = DriverManager.getConnection(AppContext.INSTANCE.getProperty("forms2me.db.url")
                        , AppContext.INSTANCE.getProperty("forms2me.db.user")
                        , AppContext.INSTANCE.getProperty("forms2me.db.pass"));
                
            	PreparedStatement stm = null;
            	PreparedStatement insertStm = null;
            	String sql = "insert into forms_2_me(account_no, form_type, form_quantitiy, created_date, source, updated_date) values(?, ?, 1, sysdate, 'I', sysdate)";
           	    insertStm = con.prepareStatement("insert into forms_2_me(account_no, form_type, form_quantity, created_date, source, updated_date) values(?, ?, 1, sysdate, 'I', sysdate)");
           	    insertStm.setString(1, ssn);

           	    int orderStatus = 0;
           	    String notes = "";
            	if(form1040) {
            		insertStm.setString(2, "00");
                    orderStatus = insertStm.executeUpdate();
                    notes = "1040";
                }
                if(formCR7) {
                    insertStm.setString(2, "02");
                    orderStatus = insertStm.executeUpdate();
                    notes += IvrUtils.isEmpty(notes) ? "CR7" : ", CR7";
                }
                if(orderStatus > 0) {
                	setPageItem(localPageContext, "ContactReason", "Forms Request");
                	setPageItem(localPageContext, "ContactResolution", "Request Processed");
                	setPageItem(localPageContext, "ContactNotes", notes);
                }
            }
        } catch(Exception e) {
            resultPort = EXCEPTION;
            e.printStackTrace();
        } finally {
            try {con.close();}catch(Exception e){}
        }
        
        localPageContext.setAttribute("nAllowType", new Integer(resultPort), PageContext.PAGE_SCOPE);

	}


	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext) {

	}%>
<%@ page import="java.util.*"%>
<%@ page import="org.apache.log4j.Logger"%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.*"%>
<%@ page import="java.sql.*"%>
<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*"%>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/reportingsupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();
    String label = "";
    StringBuffer data = new StringBuffer();

    session.setAttribute("PAGEID", "TransferCallSub");

    headerVxml.append(getVxmlHeader(pageContext));

    String action = request.getParameter("ACTION");

    if ("ReturnEvent".equals(action))
    {
        out.print(headerVxml);
        headerVxml.setLength(0);
%>
    
    <form><block><throw event="<%
    	String eventName = lookUp(pageContext, "LAST_EVENT_NAME");
    	out.print(eventName);
    %>" message="<%
    	String eventMsg = lookUp(pageContext, "LAST_EVENT_MSG");
    	out.print(eventMsg);
    %>"/></block></form>

<%

	if (eventName.startsWith("connection.disconnect"))
	{
		setInSession(pageContext, "ReportItem_last_scf", "1");
	}

	sendSubcallflowEndEvent(pageContext);
    	removeReportItemNode(pageContext);
    }
    else
    {
    
    
        out.print(headerVxml);
        headerVxml.setLength(0);
%><form><var name="ROOTDOCUMENT" expr="'<%
            data.append(lookUp(pageContext,"ROOTDOCUMENT"));
            out.print(data);
            data.setLength(0);
        %>'"/><%	
	setPageItem(pageContext, "SUBDIALOGTRANSITION", "TRUE");
	%><%
data.append("<var name=\"");
data.append("APP_LANGUAGE");
data.append("\" expr=\"'");


data.append(lookUp(pageContext,"APP_LANGUAGE"));

data.append("'\"/>");
out.print(data);
data.setLength(0);
%><%
data.append("<var name=\"");
data.append("VOXFILEDIR");
data.append("\" expr=\"'");


data.append(lookUp(pageContext,"VOXFILEDIR"));

data.append("'\"/>");
out.print(data);
data.setLength(0);
%><%
data.append("<var name=\"");
data.append("GRAMMARDIR");
data.append("\" expr=\"'");


data.append(lookUp(pageContext,"GRAMMARDIR"));

data.append("'\"/>");
out.print(data);
data.setLength(0);
%><subdialog name="TransferCallSub" src="TransferStart.jsp"><filled>
      <var name="CALL_PATH"/>
      <if cond="TransferCallSub.CALL_PATH != &#34;undefined&#34;">
         <assign name="CALL_PATH" expr="TransferCallSub.CALL_PATH"/>
      </if>
      <var name="IVR_CALL_TYPE"/>
      <if cond="TransferCallSub.IVR_CALL_TYPE != &#34;undefined&#34;">
         <assign name="IVR_CALL_TYPE" expr="TransferCallSub.IVR_CALL_TYPE"/>
      </if>
      <var name="SSN"/>
      <if cond="TransferCallSub.SSN != &#34;undefined&#34;">
         <assign name="SSN" expr="TransferCallSub.SSN"/>
      </if>
      <submit method="post" next="GoodBye.jsp" namelist="ROOTDOCUMENT APP_LANGUAGE VOXFILEDIR GRAMMARDIR CALL_PATH IVR_CALL_TYPE SSN"/>
   </filled>
   <catch event=".">
      <var name="LAST_EVENT_NAME" expr="_event"/>
      <var name="LAST_EVENT_MSG" expr="_message"/>
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("TransferCallSub.jsp"));

out.print( "?ACTION=ReturnEvent" );

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='ROOTDOCUMENT APP_LANGUAGE VOXFILEDIR GRAMMARDIR LAST_EVENT_NAME LAST_EVENT_MSG'" );

out.print( "/>" );
%>
</catch></subdialog></form>
   
<%
   }

   processAnywhere(pageContext);

   out.print(getVxmlFooter());

   processEnd(pageContext);

%>
<%!
		  // localPageContext - Contains the reference to the Page Context.	  
	  //					Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
			public void processBegin(PageContext localPageContext){

			}
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
			public void processAnywhere(PageContext localPageContext){

			}
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
			public void processEnd(PageContext localPageContext){

			}
		  %>
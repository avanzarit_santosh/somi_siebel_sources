<?xml version="1.0" encoding="utf-8"?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/recordingsupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();
    StringBuffer data = new StringBuffer();

    headerVxml.append(getVxmlHeader(pageContext));
        
    
	String voxFileName = getNewVoxFileName(pageContext, "","CAPTURE","audio/basic","","");

	//voxFileName = voxFileName.replaceAll("\\\\", "\\\\\\\\\\\\\\\\");
	StringBuffer valueBuffer=new StringBuffer(voxFileName);
	
	valueBuffer = replace(valueBuffer, "\\", "\\\\\\\\");
	voxFileName = new String(valueBuffer);
	
	
	session.setAttribute("AdminInfoMsgRecord", voxFileName);
	
        out.print(headerVxml);
        headerVxml.setLength(0);
%><form><block name="AdminInfoMsgRecordP0" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="false" xml:lang="en-US"><% }
else
 {%><prompt bargein="false" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("805_Record.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><var name="retryCount" expr="1"/><%
        out.print("<var  name=\"recFileName\" expr=\"'" + valueBuffer + "'\" />");
%><%
	
		out.print("<var  name=\"NextBlockName\" expr=\"'AdminInfoConfirmMsg.jsp" + "'\" />");
		out.print("<var  name=\"NewGCTName\" expr=\"'AdminInfoMsgRecord_GVPFileName" + "'\" />");
				
%><record dtmfterm="false" beep="true" name="AdminInfoMsgRecord" type="audio/basic" maxtime="300s" finalsilence="3s">
      <grammar mode="dtmf" version="1.0" root="dtmfSettings">
         <rule scope="public" id="dtmfSettings">
            <one-of>
               <item>#</item>
            </one-of>
         </rule>
      </grammar>
   </record>
   <block>
      <submit xmlns="http://www.telera.com/vxml/2.0/ext/20020430" method="post" mode="sync" enctype="multipart/form-data" next="AutoGenerateCapture.jsp?SESSIONID=$sessionid$" namelist="recFileName NextBlockName NewGCTName AdminInfoMsgRecord$.duration AdminInfoMsgRecord$.size AdminInfoMsgRecord$.maxtime AdminInfoMsgRecord$.termchar AdminInfoMsgRecord"/>
   </block>
</form>
<catch event="connection.disconnect.hangup">
   <submit method="post" enctype="multipart/form-data" next="AutoGenerateCapture.jsp?ACTION=HUP&amp;SESSIONID=$sessionid$" namelist="recFileName NextBlockName NewGCTName AdminInfoMsgRecord$.duration AdminInfoMsgRecord$.size AdminInfoMsgRecord$.maxtime AdminInfoMsgRecord$.termchar AdminInfoMsgRecord"/>
</catch>
<catch event="noinput">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("AdminGoodBye.jsp"));

out.print( "'/>" );
%>
</catch>
<catch event="error">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("AdminSystemDown.jsp"));

out.print( "'/>" );
%>
</catch>
    
<%
    processAnywhere(pageContext);

    out.print(getVxmlFooter());

    processEnd(pageContext);
%>

<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
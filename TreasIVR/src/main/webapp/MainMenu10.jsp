<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*"%>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();
    String label = "";
    String subDlgReturn = "";
    String query="";
    StringBuffer data = new StringBuffer();

    session.setAttribute("PAGEID" ,"MainMenu10");

    headerVxml.append(getASRLangVxmlHeader(pageContext));

    
        out.print(headerVxml);
        headerVxml.setLength(0);
%><form>
   <block name="Flush">
      <prompt bargein="false">
         <audio src="StudioIncludes/silence10ms.wav"/>
      </prompt>
   </block><block name="MainMenu10P0" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("706_SP_Welcome.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><block name="MainMenu10P1" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("206_MainFlow_Press1.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><block name="MainMenu10P2" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("207_MainFlow_Press2.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><block name="MainMenu10P3" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("208_MainFlow_Press3.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><block name="MainMenu10P4" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("VoxServlet?m=SPEC2_PROMPT");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><block name="MainMenu10P5" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("617_Star_Repeat.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><var name="retryCount" expr="1"/><field  name="MainMenu10" ><prompt bargein="true" timeout="3s"/>
   <option dtmf="1"/>
   <option dtmf="2"/>
   <option dtmf="3"/>
   <option dtmf="5"/>
   <option dtmf="6"/>
   <option dtmf="*"/>
   <option dtmf="#"/></field><filled mode="all" namelist="MainMenu10">
      <if cond="MainMenu10 == 1">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("ReturnMenu.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='MainMenu10 MainMenu10$.inputmode MainMenu10$.confidence MainMenu10$.utterance MainMenu10$.markname MainMenu10$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="MainMenu10 == 2">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("OrderFormsSub.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='MainMenu10 MainMenu10$.inputmode MainMenu10$.confidence MainMenu10$.utterance MainMenu10$.markname MainMenu10$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="MainMenu10 == 3">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("OtherOptionSub.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='MainMenu10 MainMenu10$.inputmode MainMenu10$.confidence MainMenu10$.utterance MainMenu10$.markname MainMenu10$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="MainMenu10 == 5">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("PlaySpecialMsg2.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='MainMenu10 MainMenu10$.inputmode MainMenu10$.confidence MainMenu10$.utterance MainMenu10$.markname MainMenu10$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="MainMenu10 == 6">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("SetSpanish.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='MainMenu10 MainMenu10$.inputmode MainMenu10$.confidence MainMenu10$.utterance MainMenu10$.markname MainMenu10$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="MainMenu10 == '*'">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("SpecialMenuBranch.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='MainMenu10 MainMenu10$.inputmode MainMenu10$.confidence MainMenu10$.utterance MainMenu10$.markname MainMenu10$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="MainMenu10 == '#'">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("IvrAdminSub.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='MainMenu10 MainMenu10$.inputmode MainMenu10$.confidence MainMenu10$.utterance MainMenu10$.markname MainMenu10$.marktime '" );

out.print( "/>" );
%>
</if>
   </filled>
   <catch event="noinput" cond="retryCount == 1">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("NoInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="MainMenu10P0 MainMenu10P1 MainMenu10P2 MainMenu10P3 MainMenu10P4 MainMenu10P5 " /></catch>
   <catch event="nomatch" cond="retryCount == 1">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("InvalidInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="MainMenu10P0 MainMenu10P1 MainMenu10P2 MainMenu10P3 MainMenu10P4 MainMenu10P5 " /></catch>
   <catch event="noinput" cond="retryCount == 2">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("NoInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="MainMenu10P0 MainMenu10P1 MainMenu10P2 MainMenu10P3 MainMenu10P4 MainMenu10P5 " /></catch>
   <catch event="nomatch" cond="retryCount == 2">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("InvalidInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="MainMenu10P0 MainMenu10P1 MainMenu10P2 MainMenu10P3 MainMenu10P4 MainMenu10P5 " /></catch>
   <catch event="nomatch" cond="retryCount == 3">
      <throw event="com.genesys.studio.toomanynomatches"/>
   </catch>
   <catch event="noinput" cond="retryCount == 3">
      <throw event="com.genesys.studio.toomanynoinputs"/>
   </catch>
</form>
<catch event="com.genesys.studio.toomanynoinputs">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("GoodBye.jsp"));

out.print( "'/>" );
%>
</catch>
<catch event="com.genesys.studio.toomanynomatches">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("GoodBye.jsp"));

out.print( "'/>" );
%>
</catch>

<%
    processAnywhere(pageContext);

    out.print(getVxmlFooter());
    
    processEnd(pageContext);

%>
<%!
		  // localPageContext - Contains the reference to the Page Context.	  
	  //					Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
			public void processBegin(PageContext localPageContext){

			}
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
			public void processAnywhere(PageContext localPageContext){

			}
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
			public void processEnd(PageContext localPageContext){

			}
		  %>
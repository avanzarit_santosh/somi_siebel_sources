<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"RPTSuccess.jsp","ReturnRPTFail.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.	  
	//                    Can be used o Access the HTTP Objects 
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext) {

	}

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.				
	//See the help file for more details		
	public void processAnywhere(PageContext localPageContext) {
		final int CRM_PROCESS_DATE = 1;
		final int CRM_DOWN = 2;
		
		
		int resultPort = CRM_PROCESS_DATE;
		
		
		String callPath = IvrUtils.appendCallPath(lookUp(localPageContext, "CallPath"), CallPathConstants.RETURN_STATUS );
		setPageItem(localPageContext, "CallPath", callPath);


		Calendar cal = Calendar.getInstance();
		Date efileDate = null;
		Date paperDate = null;
		
		
		// retrieve rpt dates from ws
		try {
		    String endpoint = AppContext.INSTANCE.getProperty("siebel.webservice.endpoint");

	        ReturnProcessDateProxy proxy = new ReturnProcessDateProxy(endpoint);
	        MIIvrCurrReturnProcessDateQueryByExampleInput input = proxy.createRequest();
	        MIIvrCurrReturnProcessDateQueryByExampleOutput output = proxy.sendRequestCacheResult(input);
	        efileDate = proxy.getEfileReturnProcessDate(output);
	        paperDate = proxy.getPaperReturnProcessDate(output);
	        callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.RETURN_PROCESSING_TIME);
            if(efileDate != null && paperDate != null) { 
		        setPageItem(localPageContext, "EfileReturnProcessDate", IvrUtils.getDateString(efileDate));
		        setPageItem(localPageContext, "PaperReturnProcessDate", IvrUtils.getDateString(paperDate));
            }
            else {
                setPageItem(localPageContext, "EfileReturnProcessDate", "");
                setPageItem(localPageContext, "PaperReturnProcessDate", "");
            	resultPort = CRM_DOWN;
            }
	        

		}catch(Exception e) { 
        	setLog(localPageContext, 3, "Error retrieving ReturnProcessDates. " + e.getMessage()); 
        	resultPort = CRM_DOWN;
        }
		
        setPageItem(localPageContext, "ContactReason", "Where is My Refund");
        setPageItem(localPageContext, "ContactResolution", "Customer End Call");

		setPageItem(localPageContext, "CallPath", callPath);
        setPageItem(localPageContext, "ReportItem_Application Result", "SUCCESS");  
        setPageItem(localPageContext, "ReportItem_Application Result Reason", callPath);

        localPageContext.setAttribute("nAllowType", new Integer(resultPort), PageContext.PAGE_SCOPE);
	}

	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext) {

	}%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.IvrConstants.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.*"%>
<%@ page import="gov.mi.state.treas.ivr.ws.rpt.*"%>
<?xml version="1.0" encoding="utf-8"?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/reportingsupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String label = "";
    StringBuffer data = new StringBuffer();
    StringBuffer headerVxml = new StringBuffer();
	String g_useDynaLang = "";
	String appSettingName = "";
	String appSettingValue = "";
	
    g_useDynaLang = lookUp(pageContext,"UseDynamicLanguage");
	
    session.setAttribute("PAGEID" ,"CallTransferStart");

    headerVxml.append(getMultiLangRootVxmlHeader(pageContext));
    // ****************************************************
    // If this is a new session, ROOTDOCUMENT will be undefined
    // and the session should be updated.  Otherwise, this is
    // a session continuing in a new callflow, and we should not
    // update the session.
    // ****************************************************

    boolean isRootDocumentFetch = false;
    
    if (session.getAttribute("ROOTDOCUMENT") == null ||
        "VOID".equals(session.getAttribute("ROOTDOCUMENT"))) 
    {
        session.setAttribute("ROOTDOCUMENT",
        encodeUtf8Path(getBaseURL(pageContext) + "/CallTransferStart.jsp"));
    }
    else
    {
	String tempRoot = (session.getAttribute("ROOTDOCUMENT")).toString();
	if (tempRoot.endsWith("CallTransferStart.jsp"))
	{
		isRootDocumentFetch = true;
	}
	else
	{
		session.setAttribute("ROOTDOCUMENT","CallTransferStart.jsp");
	}
    }

    
        out.print(headerVxml);
        headerVxml.setLength(0);

	appSettingName = "APP_LANGUAGE";
	appSettingValue = "en-US";
	
	if (!isRootDocumentFetch)
	{
	   if ("1".equals(g_useDynaLang))
	   {
	      if (("APP_LANGUAGE".equals(appSettingName)) || ("VOXFILEDIR".equals(appSettingName)) || ("GRAMMARDIR".equals(appSettingName)))
	      {
	         // do nothing
	      }
	      else
	      {
      	         setPageItem(pageContext, appSettingName, appSettingValue);
	      }
	   }
	   else
	   {
	      setPageItem(pageContext, appSettingName, appSettingValue);
	   }
	}
	

	appSettingName = "EWT";
	appSettingValue = "";
	
	if (!isRootDocumentFetch)
	{
	   if ("1".equals(g_useDynaLang))
	   {
	      if (("APP_LANGUAGE".equals(appSettingName)) || ("VOXFILEDIR".equals(appSettingName)) || ("GRAMMARDIR".equals(appSettingName)))
	      {
	         // do nothing
	      }
	      else
	      {
      	         setPageItem(pageContext, appSettingName, appSettingValue);
	      }
	   }
	   else
	   {
	      setPageItem(pageContext, appSettingName, appSettingValue);
	   }
	}
	

	appSettingName = "IVR_CALL_TYPE";
	appSettingValue = "";
	
	if (!isRootDocumentFetch)
	{
	   if ("1".equals(g_useDynaLang))
	   {
	      if (("APP_LANGUAGE".equals(appSettingName)) || ("VOXFILEDIR".equals(appSettingName)) || ("GRAMMARDIR".equals(appSettingName)))
	      {
	         // do nothing
	      }
	      else
	      {
      	         setPageItem(pageContext, appSettingName, appSettingValue);
	      }
	   }
	   else
	   {
	      setPageItem(pageContext, appSettingName, appSettingValue);
	   }
	}
	

	appSettingName = "ReturnResult";
	appSettingValue = "0";
	
	if (!isRootDocumentFetch)
	{
	   if ("1".equals(g_useDynaLang))
	   {
	      if (("APP_LANGUAGE".equals(appSettingName)) || ("VOXFILEDIR".equals(appSettingName)) || ("GRAMMARDIR".equals(appSettingName)))
	      {
	         // do nothing
	      }
	      else
	      {
      	         setPageItem(pageContext, appSettingName, appSettingValue);
	      }
	   }
	   else
	   {
	      setPageItem(pageContext, appSettingName, appSettingValue);
	   }
	}
	

	appSettingName = "SubCallFlowID";
	appSettingValue = "CallTransfer";
	
	if (!isRootDocumentFetch)
	{
	   if ("1".equals(g_useDynaLang))
	   {
	      if (("APP_LANGUAGE".equals(appSettingName)) || ("VOXFILEDIR".equals(appSettingName)) || ("GRAMMARDIR".equals(appSettingName)))
	      {
	         // do nothing
	      }
	      else
	      {
      	         setPageItem(pageContext, appSettingName, appSettingValue);
	      }
	   }
	   else
	   {
	      setPageItem(pageContext, appSettingName, appSettingValue);
	   }
	}
	
%><property name="com.telera.speechenabled" value="false"/>
<form><var name="IVR_CALL_TYPE" expr="''"/><block>
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("TransferProcess.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist=' IVR_CALL_TYPE'" );

out.print( "/>" );
%>
</block>
</form>
<catch event=".">
   <return eventexpr="_event" messageexpr="_message"/>
</catch><%
	if (!isRootDocumentFetch)
	{
		String subcallflowId = lookUp(pageContext, "SubCallFlowID");
		createReportItemNode(pageContext, "0", subcallflowId);
	}		


   processAnywhere(pageContext);
	
   out.print(getVxmlFooter());
   
   processEnd(pageContext);
	
   if (!isRootDocumentFetch)
	sendSubcallflowStartEvent(pageContext);	
%>
<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
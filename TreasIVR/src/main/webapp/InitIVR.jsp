<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"Welcome.jsp","PlayEmergencyMsg.jsp","TransferCallSub.jsp","SystemDown.jsp","IvrAdminSub.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!
	// localPageContext - Contains the reference to the Page Context.	  
	//                    Can be used o Access the HTTP Objects 
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext){

	}


    Logger logger = Logger.getLogger(getClass());

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.				
	//See the help file for more details		
	public void processAnywhere(PageContext localPageContext){
		final int NORMAL = 1;

		final int EMERGENCY = 2;
		
		final int CENTER_CLOSED = 3;
		final int QUEUE_FULL = 3;
		final int HOLIDAY = 3;
		final int TRANSFER_CALL = 3;
		
		final int EXCEPTION = 4;
        final int SYSTEM_DOWN = 4;
        final int ADMIN_MODULE = 5;
	    
		int resultPort = NORMAL;
		String emergencyFlag = null;
		String specialFlag = null;
		String forceCloseFlag = null;

	    boolean crmDown = false;
	    
        String ivrCallType = lookUp(localPageContext, "IVR_CALL_TYPE");
        String forcedDisc = lookUp(localPageContext , "FORCED_DISC");
        String ewt = lookUp(localPageContext, "EWT");
        String callPath = lookUp(localPageContext, "CallPath");
        String configFilePath = lookUp(localPageContext, "CONFIG_FILE_PATH");
        String dnis = lookUp(localPageContext, "DID");
        logger.debug("InitIVR");
        
        try {
	        int year = Calendar.getInstance().get(Calendar.YEAR);
	        setPageItem(localPageContext, "TaxYear", "IIT" + Integer.toString(year - 1));
	        
	        
	        // pull emergency, special, close and info flags from DB
	        List<IvrConfigItem> configItems = AppContext.INSTANCE.getAllConfigItems();

	        for(IvrConfigItem item : configItems) {
                if("INFO".equals(item.getMessageName())) {
                    setPageItem(localPageContext, "infoFlag", item.getFlag());
                } else if("EMRG".equals(item.getMessageName())) {
                    setPageItem(localPageContext, "EmergencyFlag", item.getFlag());
                    emergencyFlag = item.getFlag();
                } else if("SPEC".equals(item.getMessageName())) {
                    setPageItem(localPageContext, "SpecialFlag", item.getFlag());
                } else if("SPEC2".equals(item.getMessageName())) {
                    setPageItem(localPageContext, "SpecialFlag2", item.getFlag());
                } else if("CLOSE".equals(item.getMessageName())) {
                    setPageItem(localPageContext, "ForceCloseFlag", item.getFlag());
                }
	        }


            boolean spanishCaller = "es-MX".equalsIgnoreCase(lookUp(localPageContext, "APP_LANGUAGE")); 
            boolean isExecCaller = "EO_CALLER".equalsIgnoreCase(lookUp(localPageContext, "IVR_CALL_TYPE"));
            boolean isHoliday = "YES".equalsIgnoreCase(lookUp(localPageContext, "HOLIDAY"));
            boolean isOfficeClosed = "YES".equalsIgnoreCase(lookUp(localPageContext, "OFFICE_CLOSED"));
            
            boolean isForceClosed = "Y".equalsIgnoreCase(forceCloseFlag);

            String emergencyClosed = isForceClosed ? "YES" : "NO";
            setPageItem(localPageContext, "EMERGENCY_CLOSED", emergencyClosed);
            
            if("1100".equals(lookUp(localPageContext, "ScriptID"))) {
            	resultPort = TRANSFER_CALL;	
            }       
            else if("Y".equalsIgnoreCase(emergencyFlag)) {
            	resultPort = EMERGENCY;
            }
			else if(isExecCaller) {
				callPath = IvrUtils.appendCallPath(callPath, "EO");
		                setPageItem(localPageContext, "CallPath", callPath);
		                setPageItem(localPageContext, "ReportItem_Application Result", "SUCCESS");  
		                setPageItem(localPageContext, "ReportItem_Application Result Reason", callPath);
				resultPort = TRANSFER_CALL;
			}


		} catch(Exception e) {
			logger.debug("Error initializing IVR. " + e.getMessage());
			resultPort = SYSTEM_DOWN;
		}

		
		MDC.put("ANI", lookUp(localPageContext, "ANI"));

		logger.debug("script id: " + lookUp(localPageContext, "ScriptID"));
		logger.debug("office closed : " + lookUp(localPageContext, "OFFICE_CLOSED"));
		logger.debug("ivr call type: " + ivrCallType);
        logger.debug("foced disc: " + forcedDisc);
        logger.debug("app id: " + lookUp(localPageContext, "APPID"));
        logger.debug("main ewt: " + lookUp(localPageContext, "EWT"));
        logger.debug("result port: " + resultPort);
        
        localPageContext.setAttribute("nAllowType", new Integer(resultPort), PageContext.PAGE_SCOPE);
	}
	
	


	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext){

	}
%>
<%@ page import="java.util.*"%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.TreasIVRConfig.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.TreasIVRConfig.Admin.*"%>
<%@ page import="org.apache.log4j.Logger"%>
<%@ page import="org.apache.log4j.MDC"%>
<%@ page import="org.apache.commons.lang.StringUtils"%>
<%@ page import="org.apache.commons.io.IOUtils"%>
<%@ page import="java.lang.Math"%>
<%@ page import="org.w3c.dom.*"%>
<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" %>
<%@ page import="java.util.*,java.lang.Throwable,java.lang.String" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%
    String vXml = "";

    try {
        request.setCharacterEncoding("UTF-8");
    } catch (java.io.UnsupportedEncodingException e) {
        e.printStackTrace();
    }
    if (request.getMethod().equalsIgnoreCase("HEAD")) {
        return;
    }

    session.setAttribute("PAGEID", "TrnsfrNoEWTBranch");
    java.util.Enumeration params = request.getParameterNames();
    String paramName;
    String paramValue;
    while (params.hasMoreElements()) {
        paramName = (String) params.nextElement();
        paramValue = request.getParameter(paramName);
        session.setAttribute(paramName, paramValue);
    }
    processBegin(pageContext);

    String strLabel = lookUp(pageContext,"ROOTDOCUMENT");
    String strQuery = getQueryString(pageContext).trim();


    //''''' Header Section '''''
    vXml += getVxmlHeader(pageContext);
    vXml += "<form><block>";
    //''''' Header Section End --- Contd... '''''

    //''''' Body Section '''''
    
	String nodeArray[] = {"_","NoEWTBranch"};
	String variableArray[] = {"_","NoEWT"};
	String valueArray[] = {"_","true"};
String linkArray[] = {"TrnsfrSetSsnValid.jsp","TrnsfrMsgNoEWT.jsp"};
	String linkLabel[] = {"Continue","NoEWTBranch"};
	

    processAnywhere(pageContext);

	// Check for condition match in all rows
	// Set the matched node name
	
	int i = 0;
	int j = 0;
	String nodeName = "";
	String variable = "";
	String value = "";
	String gctPrefix = "GCT:";
	String trtPrefix = "TRT:";	
	boolean matched = false;
		
	for (i = 1; i < nodeArray.length; i++){
		variable = lookUp(pageContext, variableArray[i]);

		value = valueArray[i];		

		if (value.startsWith(gctPrefix) == true){
			value = lookUp(pageContext, value.substring(4));
		}

		if (value.startsWith(trtPrefix) == true){
			value = value.substring(4);
		}
				
		if (variable.equals(value)) {
			nodeName = nodeArray[i];
			break;
		}
	}

	// get the actual output link

	if (nodeName.length() == 0 ){
		matched = false;
	}else{
		for (j = 1; j < linkLabel.length; j++){
			if (linkLabel[j].equals(nodeName)){
				matched = true;
				break;
			}
		}
	}
	
	if (matched == true){
    	vXml += "<goto next=\"" + linkArray[j]  + "\"/>\n";
	}
	else{
    	vXml += "<goto next=\"" + linkArray[0]  + "\"/>\n";
	}

    vXml += "</block>\n";
    vXml += "</form>\n";
    vXml += "</vxml>\n";

    out.print(vXml);

    processEnd(pageContext);

%>
      <%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
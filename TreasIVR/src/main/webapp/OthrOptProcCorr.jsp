<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"OthrOptCorrInProgressMsg.jsp","OthrOptCorrCompletedMsg.jsp","OthrOptCorrDtInput.jsp","OthrOptCorrDtInput.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.	  
	//                    Can be used o Access the HTTP Objects 
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext) {

	}

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.				
	//See the help file for more details		
	public void processAnywhere(PageContext localPageContext) {
        final int EXCEPTION = 4;
        final int HAS_CORR = 1;
        final int CORR_IN_PROGRESS = 1;
        final int CORR_COMPLETE = 2;
        final int NO_CORR = 3;
       
        int resultPort = NO_CORR;     
       
        String voxFileDir = lookUp(localPageContext, "VOXFILEDIR");
        String callPath = lookUp(localPageContext, "CallPath");
        setPageItem(localPageContext, "ContactReason", "Core Information");
        String callerSsn = lookUp(localPageContext, "CallerSsn");
        String authYear = lookUp(localPageContext, "AuthYear");
        String corrYear = authYear.substring(3);
        

        try {
            // get endpoint from config file
            String endpoint = AppContext.INSTANCE.getProperty("siebel.webservice.endpoint");

            // query siebel for account correspondences
            ServiceRequestProxy corrProxy = new ServiceRequestProxy(endpoint);
            MIIvrServiceRequestQueryByExampleInput input = corrProxy.createQueryRequest();
            input.getListOfMiservicerequest().getServiceRequest().get(0).setAccount(callerSsn);

            MIIvrServiceRequestQueryByExampleOutput output = corrProxy.sendQueryRequest(input); 
 
            if(!output.getListOfMiservicerequest().getServiceRequest().isEmpty()) {
                // has corr
                ServiceRequest sr = getMostRecentSR(output, corrYear);

                // set call path
                callPath = IvrUtils.callPathAppend(callPath, CallPathConstants.CORE_FOUND);
                
                // retrieve estimated corr completion date
                Date corrCreateDate = IvrUtils.parseDate(sr.getCreated());
                Date corrCompDate = IvrUtils.parseDate(sr.getClosedDate());
                
                if(corrCompDate != null) {
                    // play 075 message
                    resultPort = CORR_COMPLETE;

                    setPageItem(localPageContext, "ContactResolution", "Core Complete");
                    
                    Calendar cal = Calendar.getInstance();
                    Date now = cal.getTime();
                    cal.setTime(corrCompDate);
                    cal.add(Calendar.DAY_OF_MONTH, 14);
                    Date comp14 = cal.getTime();
                    setPageItem(localPageContext, "CorrEstCompleteDate", IvrUtils.getDateString(comp14));
                    setPageItem(localPageContext, "EstimatedCompletionDate", IvrUtils.getDateString(comp14));
                    
                    // check if (completion date + 14) has passed
                    if(now.after(comp14)) {
                        // set transfer option message on menu
                        setPageItem(localPageContext, "OtherOptCsrMenuItem", "619_CSR_Press3.vox");
                    }
                }
                else {
                    // play 076 message
                    resultPort = CORR_IN_PROGRESS;

                    setPageItem(localPageContext, "ContactResolution", "Core Being Processed");
                    
                    // get estimate completion date from Web Service
                    String estDateStr = sr.getEstimateDate();
                    Date estDate = IvrUtils.parseDate(estDateStr);
                    
                    Calendar cal = Calendar.getInstance();
                    Date now = cal.getTime();
                    cal.setTime(estDate);
                    cal.add(Calendar.DAY_OF_MONTH, 14);
                    Date comp14 = cal.getTime();
                    
                    setPageItem(localPageContext, "CorrEstCompleteDate", IvrUtils.getDateString(comp14));
                    setPageItem(localPageContext, "EstimatedCompletionDate", IvrUtils.getDateString(comp14));
                    
                    // check if (completion date + 14) has passed
                    if(now.after(comp14)) {
                        // set transfer option message on menu
                        setPageItem(localPageContext, "OtherOptCsrMenuItem", "619_CSR_Press3.vox");
                    }                    

                }
            }
            else {
                // has no corr
                resultPort = NO_CORR;
                callPath = IvrUtils.callPathAppend(callPath, CallPathConstants.CORE_NOT_FOUND);
                setPageItem(localPageContext, "ContactResolution", "No Record of Correspondence");
            }
        } catch(Exception e) {
            resultPort = EXCEPTION;
            setLog(localPageContext, 3, "Error retrieving Correspondence. " + e.getMessage());
            e.printStackTrace();
        }
        
        setPageItem(localPageContext, "CallPath", callPath);
        setPageItem(localPageContext,"ReportItem_Application Result", "SUCCESS");  
        setPageItem(localPageContext,"ReportItem_Application Result Reason", callPath);



        localPageContext.setAttribute("nAllowType", new Integer(resultPort), PageContext.PAGE_SCOPE);

    
	}

	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext) {
	}
	
    ServiceRequest getMostRecentSR(MIIvrServiceRequestQueryByExampleOutput srResponse, String corrYear) {
    	List<ServiceRequest> srList = srResponse.getListOfMiservicerequest().getServiceRequest();
    	Iterator<ServiceRequest> itor = srList.iterator();
    	
    	int year = IvrUtils.parseInt(corrYear) + 1;
    	Calendar cal = Calendar.getInstance();
        Date maxDate = cal.getTime();
    	cal.clear();
    	cal.set(year, Calendar.JANUARY, 1);
    	Date minDate = cal.getTime();

    	Date closeDate = null;
    	Date createdDate = null;
    	ServiceRequest resultSr = null;
    	for(ServiceRequest sr : srList) {
            closeDate = IvrUtils.parseDate(sr.getClosedDate());
            createdDate = IvrUtils.parseDate(sr.getCreated());
            
            if(createdDate != null && createdDate.before(maxDate) && createdDate.after(minDate)) {
                minDate = createdDate;
                resultSr = sr;
            }    		
    	}
    	return resultSr;
    }
	
	%>
<%@ page import="java.util.*"%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.IvrConstants.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.*"%>
<%@ page import="gov.mi.state.treas.ivr.ws.corr.*"%>
<%@ page import="gov.mi.state.treas.ivr.ws.corr.time.*"%>
<%@ page import="org.w3c.dom.*"%>
<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*"%>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();
    String label = "";
    String subDlgReturn = "";
    String query="";
    StringBuffer data = new StringBuffer();

    session.setAttribute("PAGEID" ,"EstPaymentAuthFailMenu");

    headerVxml.append(getASRLangVxmlHeader(pageContext));

    
        out.print(headerVxml);
        headerVxml.setLength(0);
%><form>
   <block name="Flush">
      <prompt bargein="false">
         <audio src="StudioIncludes/silence10ms.wav"/>
      </prompt>
   </block><block name="EstPaymentAuthFailMenuP0" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("504_EstPymt_Loop.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><var name="retryCount" expr="1"/><field  name="EstPaymentAuthFailMenu" ><prompt bargein="true" timeout="3s"/>
   <option dtmf="1"/>
   <option dtmf="2"/>
   <option dtmf="*"/></field><filled mode="all" namelist="EstPaymentAuthFailMenu">
      <if cond="EstPaymentAuthFailMenu == 1">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("EstPymntMainMenu.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='EstPaymentAuthFailMenu EstPaymentAuthFailMenu$.inputmode EstPaymentAuthFailMenu$.confidence EstPaymentAuthFailMenu$.utterance EstPaymentAuthFailMenu$.markname EstPaymentAuthFailMenu$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="EstPaymentAuthFailMenu == 2">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("EstPymntTransfer.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='EstPaymentAuthFailMenu EstPaymentAuthFailMenu$.inputmode EstPaymentAuthFailMenu$.confidence EstPaymentAuthFailMenu$.utterance EstPaymentAuthFailMenu$.markname EstPaymentAuthFailMenu$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="EstPaymentAuthFailMenu == '*'">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("EstRptMenu3.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='EstPaymentAuthFailMenu EstPaymentAuthFailMenu$.inputmode EstPaymentAuthFailMenu$.confidence EstPaymentAuthFailMenu$.utterance EstPaymentAuthFailMenu$.markname EstPaymentAuthFailMenu$.marktime '" );

out.print( "/>" );
%>
</if>
   </filled>
   <catch event="noinput" count="1">
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("NoInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></catch>
   <catch event="nomatch" count="1">
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("InvalidInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></catch>
   <catch event="noinput" count="2">
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("NoInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></catch>
   <catch event="nomatch" count="2">
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("InvalidInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></catch>
   <catch event="nomatch" count="3">
      <throw event="com.genesys.studio.toomanynomatches"/>
   </catch>
   <catch event="noinput" count="3">
      <throw event="com.genesys.studio.toomanynoinputs"/>
   </catch>
</form>
<catch event="com.genesys.studio.toomanynoinputs">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("EstPymntGoodBye.jsp"));

out.print( "'/>" );
%>
</catch>
<catch event="com.genesys.studio.toomanynomatches">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("EstPymntGoodBye.jsp"));

out.print( "'/>" );
%>
</catch>

<%
    processAnywhere(pageContext);

    out.print(getVxmlFooter());
    
    processEnd(pageContext);

%>
<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
<?xml version="1.0" encoding="utf-8"?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/reportingsupport.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();

    try {
        request.setCharacterEncoding("UTF-8");
    } catch (java.io.UnsupportedEncodingException e) {
        e.printStackTrace();
    }
    if ((request.getMethod()).equalsIgnoreCase("HEAD")) {
        return;
    }

    session.setAttribute("PAGEID" ,"START");

    session.setAttribute("GVP_VoiceServerIP", request.getRemoteAddr());
	
	java.util.Enumeration headerEnum = request.getHeaders("user-agent");
    
    
    session.setAttribute("GVP_VxmlBrowser","0");
    if (headerEnum.hasMoreElements())  
    {
		 String VxmlBrowserVer="";
    
    	String userAgent = (headerEnum.nextElement()).toString();
    	if (userAgent.indexOf ("Telera _ND_C") != -1){
    	int nStartindex = userAgent.indexOf ("Telera _ND_C")+ 13;
    	userAgent = userAgent.substring(nStartindex,nStartindex+3);
    	VxmlBrowserVer = "GVP";
    	VxmlBrowserVer = VxmlBrowserVer + userAgent;
    	// System.out.println(userAgent);
		session.setAttribute("GVP_VxmlBrowser",VxmlBrowserVer);
	}
    }
    processBegin(pageContext);

    //Header VXML is handled inside XSLT

    
if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"APPID", "TREASURY_IVR");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"APP_LANGUAGE", "en-US");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"CALL_PATH", "IIT");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"CONFIG_FILE_PATH", "/WEB-INF/conf/ivrconfig.xml");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"EWT", "0");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"GRAMMARDIR", "Grammar/en-US");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"HOLIDAY", "NO");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"IVR_CALL_TYPE", "DEFAULT");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"OFFICE_CLOSED", "0");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"Q_CAP", "NO");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReturnResult", "0");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"SSN", "0");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"UseDynamicLanguage", "1");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"VOXFILEDIR", "Voxfiles/en-US");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"YearInput", "0");
}

 headerVxml.append(getRootVxmlHeader(pageContext));
 
        out.print(headerVxml);
        headerVxml.setLength(0);
%><property name="com.telera.speechenabled" value="false"/>
<script src="StudioIncludes/Common.js"/>
<form>
   <var name="ScriptID" expr="session.genesys.sid"/>
   <var name="ScriptData" expr="session.genesys.scriptdata"/>
   <var name="SessionID" expr="session.genesys.sessionid"/>
   <var name="ConnectionID" expr="session.genesys.connid"/>
   <var name="GVP_IVRProfileID" expr="session.genesys.application_id"/><%
	if(session.getAttribute("GVP_VxmlBrowser") != null && !session.getAttribute("GVP_VxmlBrowser").equals("GVP7.2"))
	{

%>
<var name="OCSApplicationFlag" expr="session.genesys.OCSFlag"/>
   <var name="OCSVoiceDN" expr="session.genesys.voice_dn_name"/>
   <var name="CPARESULT" expr="session.genesys._cparesult"/>
   <var name="SIPHeader_PAssertedID" expr="getSipHeaderValue(&#34;p-asserted-identity&#34;)"/>
   <var name="SIPHeader_CallID" expr="getSipHeaderValue(&#34;call-id&#34;)"/>
   <block>
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("MainReportsData.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='ScriptID ScriptData SessionID ConnectionID GVP_IVRProfileID OCSApplicationFlag CPARESULT OCSVoiceDN SIPHeader_PAssertedID SIPHeader_CallID'" );

out.print( "/>" );
%>
</block><% }
else
 {%><block>
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("MainReportsData.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='ScriptID ScriptData SessionID ConnectionID GVP_IVRProfileID'" );

out.print( "/>" );
%>
</block><% } %></form>
<catch event="connection.disconnect.hangup">
   <script src="StudioIncludes/Common.js"/>
   <var name="LAST_EVENT_NAME" expr="_event"/>
   <var name="LAST_EVENT_MSG_TEMP" expr="_message"/>
   <var name="LAST_EVENT_MSG" expr="replaceXMLChars(LAST_EVENT_MSG_TEMP)"/>
   <var name="LAST_EVENT_URL" expr="telera.error.currenturl"/>
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("STOP.jsp"));

out.print( "?ACTION=HUP&amp;agent-leg-flag=$agent-leg-flag$" );

out.print( "'" );

out.print( " method='get'" );

out.print( " namelist='LAST_EVENT_NAME LAST_EVENT_MSG LAST_EVENT_URL'" );

out.print( "/>" );
%>
</catch>
<catch event="error">
   <script src="StudioIncludes/Common.js"/>
   <var name="LAST_EVENT_NAME" expr="_event"/>
   <var name="LAST_EVENT_MSG_TEMP" expr="_message"/>
   <var name="LAST_EVENT_MSG" expr="replaceXMLChars(LAST_EVENT_MSG_TEMP)"/>
   <var name="LAST_EVENT_URL" expr="telera.error.currenturl"/>
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("STOP.jsp"));

out.print( "'" );

out.print( " method='get'" );

out.print( " namelist='LAST_EVENT_NAME LAST_EVENT_MSG LAST_EVENT_URL'" );

out.print( "/>" );
%>
</catch>
<catch event=".">
   <script src="StudioIncludes/Common.js"/>
   <var name="LAST_EVENT_NAME" expr="_event"/>
   <var name="LAST_EVENT_MSG_TEMP" expr="_message"/>
   <var name="LAST_EVENT_MSG" expr="replaceXMLChars(LAST_EVENT_MSG_TEMP)"/>
   <var name="LAST_EVENT_URL" expr="telera.error.currenturl"/>
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("STOP.jsp"));

out.print( "'" );

out.print( " method='get'" );

out.print( " namelist='LAST_EVENT_NAME LAST_EVENT_MSG LAST_EVENT_URL'" );

out.print( "/>" );
%>
</catch><%
if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_ReportingEnabled", "1");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_Call Abandoned Threshold Time in sec", "15");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_EnableVARLogs", "0");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_VARClientPort", "9815");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_NumCustomVar", "8");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar1_Name", "CustomVar1");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar1_Value", "");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar2_Name", "CustomVar2");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar2_Value", "");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar3_Name", "CustomVar3");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar3_Value", "");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar4_Name", "CustomVar4");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar4_Value", "");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar5_Name", "CustomVar5");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar5_Value", "");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar6_Name", "CustomVar6");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar6_Value", "");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar7_Name", "CustomVar7");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar7_Value", "");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar8_Name", "CustomVar8");
}

if (session.getAttribute("ROOTDOCUMENT") == null ||
    "".equals(session.getAttribute("ROOTDOCUMENT")))
{
    setPageItem(pageContext,"ReportItem_CustomVar8_Value", "");
}


    // ****************************************************
    // If this is a new session, ROOTDOCUMENT will be undefined
    // and the session should be updated.  Otherwise, this is
    // a session continuing in a new callflow, and we should not
    // update the session.
    // ****************************************************
    
    boolean isRootDocumentFetch = false;
    
    if (session.getAttribute("ROOTDOCUMENT") == null ||
        "".equals(session.getAttribute("ROOTDOCUMENT"))) 
    {
        session.setAttribute("ROOTDOCUMENT",
        encodeUtf8Path(getBaseURL(pageContext) + "/START.jsp"));
    }
    else
    {
		String tempRoot = (session.getAttribute("ROOTDOCUMENT")).toString();
		if (tempRoot.endsWith("START.jsp"))
		{
			isRootDocumentFetch = true;
		}
		else
		{
			//session.setAttribute("ROOTDOCUMENT","START.jsp");
			session.setAttribute("ROOTDOCUMENT",encodeUtf8Path(getBaseURL(pageContext) + "/START.jsp"));
		}
    }

    java.util.Enumeration params = request.getParameterNames();
    String name;
    String value;
    while (params.hasMoreElements()) {
        name = (String) params.nextElement();
        value = request.getParameter(name);
        session.setAttribute(name, value); 
    }

    processAnywhere(pageContext);

    out.print(getVxmlFooter());

    processEnd(pageContext);
	
        
    if (!isRootDocumentFetch)
    {
	createReportItemStack(pageContext);
	sendCallStartReportData(pageContext);
    }
%>
<%!// localPageContext - Contains the reference to the Page Context.	  
	//					Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext) {

	}

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
	public void processAnywhere(PageContext localPageContext) {

        
        try {

            AppContext appContext = AppContext.INSTANCE;
            String serverName = localPageContext.getRequest().getServerName();
            String vrp = appContext.getProperty("ivr.routePoint");

            setPageItem(localPageContext, "APPID", appContext.getProperty("app.id"));
            setPageItem(localPageContext, "RoutePoint", vrp);
            
        } catch(Exception e) {
        	setLog(localPageContext, 3, "Error initializing IVR");
        }
	}

	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	public void processEnd(PageContext localPageContext) {

	}%>
<%@ page import="java.util.*"%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.TreasIVRConfig.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.TreasIVRConfig.WebServices.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.TreasIVRConfig.Databases.*"%>
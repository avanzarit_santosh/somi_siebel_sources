<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*"%>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();
    String label = "";
    String subDlgReturn = "";
    String query="";
    StringBuffer data = new StringBuffer();

    session.setAttribute("PAGEID" ,"OthrOptCorrMsgRpt");

    headerVxml.append(getASRLangVxmlHeader(pageContext));

    
        out.print(headerVxml);
        headerVxml.setLength(0);
%><form>
   <block name="Flush">
      <prompt bargein="false">
         <audio src="StudioIncludes/silence10ms.wav"/>
      </prompt>
   </block><block name="OthrOptCorrMsgRptP0" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("605_Repeat_Main_Return.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><block name="OthrOptCorrMsgRptP1" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append((String)session.getAttribute("OtherOptCsrMenuItem"));
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><var name="retryCount" expr="1"/><field  name="OthrOptCorrMsgRpt" ><prompt bargein="true" timeout="3s"/>
   <option dtmf="1"/>
   <option dtmf="2"/>
   <option dtmf="3"/>
   <option dtmf="*"/></field><filled mode="all" namelist="OthrOptCorrMsgRpt">
      <if cond="OthrOptCorrMsgRpt == 1">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("OthrOptCorrMenuVerify.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='OthrOptCorrMsgRpt OthrOptCorrMsgRpt$.inputmode OthrOptCorrMsgRpt$.confidence OthrOptCorrMsgRpt$.utterance OthrOptCorrMsgRpt$.markname OthrOptCorrMsgRpt$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="OthrOptCorrMsgRpt == 2">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("OthrOptCorrMenuVerify.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='OthrOptCorrMsgRpt OthrOptCorrMsgRpt$.inputmode OthrOptCorrMsgRpt$.confidence OthrOptCorrMsgRpt$.utterance OthrOptCorrMsgRpt$.markname OthrOptCorrMsgRpt$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="OthrOptCorrMsgRpt == 3">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("OthrOptCorrMenuVerify.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='OthrOptCorrMsgRpt OthrOptCorrMsgRpt$.inputmode OthrOptCorrMsgRpt$.confidence OthrOptCorrMsgRpt$.utterance OthrOptCorrMsgRpt$.markname OthrOptCorrMsgRpt$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="OthrOptCorrMsgRpt == '*'">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("OthrOptCorrMenuVerify.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='OthrOptCorrMsgRpt OthrOptCorrMsgRpt$.inputmode OthrOptCorrMsgRpt$.confidence OthrOptCorrMsgRpt$.utterance OthrOptCorrMsgRpt$.markname OthrOptCorrMsgRpt$.marktime '" );

out.print( "/>" );
%>
</if>
   </filled>
</form>
<catch event="noinput">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("OthrOptCorrMenuVerify.jsp"));

out.print( "'/>" );
%>
</catch>
<catch event="nomatch">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("OthrOptCorrMenuVerify.jsp"));

out.print( "'/>" );
%>
</catch>

<%
    processAnywhere(pageContext);

    out.print(getVxmlFooter());
    
    processEnd(pageContext);

%>
<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
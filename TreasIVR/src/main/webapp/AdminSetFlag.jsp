<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"AdminFlagOnMsg.jsp","AdminFlagOffMsg.jsp","AdminSystemDown.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.	  
	//                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext) {

	}

	Logger logger = Logger.getLogger(getClass());

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.				
	//See the help file for more details		
	public void processAnywhere(PageContext localPageContext) {
        // get SPECIAL flag from db
        final int SPECIAL_FLAG_ON = 1;
        final int SPECIAL_FLAG_OFF = 2;
        final int EXCEPTION = 3;
        final String MSG_NAME = lookUp(localPageContext, "msgName");
        int resultPort = SPECIAL_FLAG_OFF;

        
        try {
            
            String menuChoice = lookUp(localPageContext, "AdminSpecialMsgMenu");
            
            String flag = null;
            if("3".equals(menuChoice)) {
            	flag = "Y";
            	resultPort = SPECIAL_FLAG_ON;
            }
            else if("4".equals(menuChoice)) {
            	flag = "N";
                resultPort = SPECIAL_FLAG_OFF;
            }

            IvrConfigItem configItem = AppContext.INSTANCE.getConfigItem(MSG_NAME);
            configItem = IvrConfigItem.newBuilder(configItem)
                    .withIvrName("TREAS_IIT_IVR")
                    .withMessageName(MSG_NAME)
                    .withFlag(StringUtils.defaultIfEmpty(flag, "N"))
                    .build();

            AppContext.INSTANCE.putConfigItem(MSG_NAME, configItem);

        }catch(Exception e) {
            e.printStackTrace();
            resultPort = EXCEPTION;
        }

        localPageContext.setAttribute("nAllowType", new Integer(resultPort), PageContext.PAGE_SCOPE);

	}

	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext) {
	    
	}%>
<%@ page import="java.util.*"%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.*"%>
<%@ page import="org.apache.log4j.Logger"%>
<%@ page import="org.apache.commons.lang.StringUtils"%>
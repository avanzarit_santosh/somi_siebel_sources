<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();
    StringBuffer data = new StringBuffer();

    session.setAttribute("PAGEID", "RtnFileDateInput");

    headerVxml.append(getASRLangVxmlHeader(pageContext));
    
    
        out.print(headerVxml);
        headerVxml.setLength(0);
%><form>
   <property name="com.telera.speechenabled" value="false"/>
   <property name="inputmodes" value="dtmf"/>
   <block name="MainInputFlush">
      <prompt bargein="false">
         <audio src="StudioIncludes/silence10ms.wav"/>
      </prompt>
   </block><block name="RtnFileDateInputP0" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("037_NoRecRtnPress1step2.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><var name="retryCount" expr="1"/>
   <var name="confirmCount" expr="0"/>
   <var name="IsConfirmMode" expr="false"/><field  name="RtnFileDateInput" type="digits?minlength=6;maxlength=6" ><property name="interdigittimeout" value="3s"/>
   <prompt bargein="true" timeout="3s"/></field><filled mode="all">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("RtnFileDateValidate.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='RtnFileDateInput RtnFileDateInput$.inputmode RtnFileDateInput$.confidence RtnFileDateInput$.utterance RtnFileDateInput$.interpretation RtnFileDateInput$.markname RtnFileDateInput$.marktime '" );

out.print( "/>" );
%>
</filled>
</form>
<catch event="noinput">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("RtnFileDateValidate.jsp"));

out.print( "'/>" );
%>
</catch>
<catch event="nomatch">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("RtnFileDateValidate.jsp"));

out.print( "'/>" );
%>
</catch>

<%
    processAnywhere(pageContext);

    out.print(getVxmlFooter());

    processEnd(pageContext);

%>

<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
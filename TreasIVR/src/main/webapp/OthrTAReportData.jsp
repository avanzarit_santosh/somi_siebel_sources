<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*"%>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/reportingsupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String root = "";
    StringBuffer vXml = new StringBuffer();
	StringBuffer fragmentBuffer = new StringBuffer();
	
    vXml.append(getVxmlHeader(pageContext));
    vXml.append("<form>");

    
	addReportItem(pageContext, "Subcallflow Result", "SUCCESS");

	addReportItem(pageContext, "Subcallflow Result Reason", lookUp(pageContext, "CALL_PATH"));

	vXml.append("<block>\n");
	vXml.append("<goto next=\"OthrOptTnsfr.jsp\"/>\n");
	vXml.append("</block>\n");


    processAnywhere(pageContext);

    vXml.append("</form></vxml>");

    out.print(vXml);

    processEnd(pageContext);

%>
<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){
			String callPath = IvrUtils.callPathAppend(lookUp(localPageContext, "CallPath"), "TA");
			setPageItem(localPageContext, "CALL_PATH", callPath);
			setPageItem(localPageContext, "CallPath", callPath);
            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
          <%@ page import="gov.mi.state.treas.ivr.*"%>
/* 
 * PlayBuiltInTypeTTS.js
 *
 * JavaScript functions for playing dynamic prompts in VoiceXML on
 * the Telera Platform
 *
 * Author: Scott Atwood <scott.atwood@telera.com>
 * Date: $Date: 3/11/03 2:39p $
 * Copyright � 2002 Telera Inc.
 */

/*
 * Problem comments:
 *
 * What are the names and organizational structure of the vox files
 * which I can refer to?
 *
 * Is this standardized across locales, or each each new locale author
 * supposed to come up with one himself or herself?
 *
 * I need at least the following:
 *   days of the week
 *   months of the year
 *   cardinals 0-99, 100, 200, 300, 400, 500, 600, 700, 800, 900, thousand,
 *      million, billion, trillion
 *   ordinals 1st-99th, 100th, 200th, 300th, 400th, 500th, 600th, 700th,
 *      800th, 900th, thousandth, millionth, billionth, trillionth
 *   "true", "false"
 *   "dollar", "dollars", "euro", "euros", "Swiss franc", "Swiss francs",
 *       "pound", "pounds", "yen", etc.
 *   "cent", "cents", "centime", "centimes", "pence", "centavo",
 *       "centavos", "sen", etc.
 *   "the" pronounced D@ (thuh)
 *   "o'clock"
 *   500ms silence
 *   "and"
 *
 * How do I know whether I should speak the day of the week or not?
 */
 
/**
 * The base URL that is prepended to the URLs for all vox files.
 */
var promptBaseUrl = "Languages/en-US/";
// var promptBaseUrl = "http://10.10.10.234/vxml/indexfile/VoxFiles/en-US/";

/**
 * Dummy class to hold constants
 */
function Format() {}
new Format();

/*
 * These flags may be bitwise OR'd together to specfy the date output format
 */

/**
 * Flag to speak the month
 */ 
Format.prototype.SPEAK_MONTH = 1;

/**
 * Flag to speak the day
 */
Format.prototype.SPEAK_DAY = 2;

/**
 * Flag to speak both month and day.  This is a shortcut for
 * f.SPEAK_MONTH | f.speak_DAY
 */
Format.prototype.SPEAK_MONTH_AND_DAY = 3;

/**
 * Flag to speak the year
 */
Format.prototype.SPEAK_YEAR = 4;

/**
 * Flag to speak the day of the week
 */
Format.prototype.SPEAK_DAY_OF_WEEK = 8;

/**
 * Translates the value of the specified type into an array of
 * vox file URLs.  The array is a list of vox files that when
 * played in succession will represent the value of the specified
 * type in a locale-specific manner.  The following types are
 * supported:
 *
 * <table>
 * <1--<tr><td>"boolean"</td><td>"true" or "false"</td></tr>-->
 * <tr><td>"date"</td><td>YYYYMMDD<br>Unspecified fields may
 *                                   be replaced by "??" or "????"</td></tr>
 * <tr><td>"digits"</td><td>A string of 0 or more characters [0-9]</td></tr>
 * <tr><td>"currency"</td><td>UUUMM.NN<br>Where UUU is the ISO4217 currency
 *                            code.  The currency code may be omitted, in
 *                            which case, the default currency for the
 *                            current locale will be used.</td></tr>
 * <tr><td>"number"</td><td>Positive or negative, integer or decimal
 *                          number</td></tr>
 * <tr><td>"phone"</td><td>Sequence of digits [0-9], optionally followed
 *                         by an "x" and extension digits [0-9]</td></tr>
 * <tr><td>"time"</td><td>hhmm[aph?]<br>"a" means AM, "p" means PM,
 *                        h means 24 hour, and ? means ambiguous</td></tr>
 * <tr><td>"ordinal"</td><td>A positive integer</td></tr>
 * <tr><td>"alphanumeric"</td><td>A string of digits [0-9] and US-ASCII
 *                                characters [A-Za-z]</td></tr>
 * <!--<tr><td>"dtmf"</td><td>A string of zero or more DTMF characters
 *                        [0-9A-D*#]</td></tr>-->
 *
 * @param value The value to translate.
 * @param type The type of the value.
 * @param format The output format [optional].
 * @return An array of URL strings.
 */
function PlayBuiltinType(value, type, format)
{
    type = type.toLowerCase();
    var format;
    
    switch (type) {        
        case 'date':
            return datePrompts(value, format);        
        case 'currency':
            return currencyPrompts(value);
        case 'number':
            return cardinalPrompts(value);
        case 'time':
            return timePrompts(value);
        case 'ordinal':
            return ordinalPrompts(value);
        case 'alphanumeric':
            return alphanumericPrompts(value);       
        default:
            return void 0;
    }
}


/**
 * Translates the date value into an array of vox file URLs
 * for playing the date.  The date must be in the ISO-8601
 * condensed format of YYYYMMDD.  One or more fields may be
 * left unspecified by substituting question mark characters
 * ("?") for a numeric value.  Output format will be in a
 * locale-specific ordering.   The format specification can
 * modify which fields are spoken.  If no format specification
 * parameter is given, the month and day are spoken.  Otherwise,
 * the date fields specified in the format specification will be
 * spoken.  Each output field is represented by a constant in the
 * Format class.  Multiple output fields are specified by 
 * bitwise OR'ing the appropriate constants.
 *
 * <pre>
 * Format f = new Format();
 * PlayBuiltinType("20020823", "date", f.SPEAK_MONTH |
 *                                     f.SPEAK_DAY |
 *                                     f.SPEAK_YEAR |
 *                                     f.SPEAK_DAY_OF_WEEK);
 * </pre>
 *
 * @param value The date in YYYYMMDD format.
 * @param format An optional format specification
 * @return An array of URL strings.
 */
function datePrompts(value, format)
{
    var year = value.substring(0, 4);
    var month = value.substring(4, 6);
    var day = value.substring(6, 8);
    var speakMonth;
    var speakDay;
    var speakYear;
    var speakDayOfWeek;
    var f = new Format();

    if (format != undefined) {
        speakMonth = format & f.SPEAK_MONTH;
        speakDay = format & f.SPEAK_DAY;
        speakYear = format & f.SPEAK_YEAR;
        speakDayOfWeek = format & f.SPEAK_DAY_OF_WEEK;
    } else {
        speakYear = false;
        speakDayOfWeek = false;
        speakDay = true;
        speakMonth = true;
    }

    if (day.substring(0, 1) == "0") {
        day = day.substring(1, 2);
    }

    var yearKnown = !(year == "????");
    var monthKnown = !(month == "??");
    var dayKnown = !(day == "??");

    if ((!yearKnown && !monthKnown && !dayKnown) ||
        (yearKnown && !monthKnown && dayKnown)) {
        return void 0;
    }

    var promptsArray;
    if (!yearKnown && !monthKnown && dayKnown) {
        promptsArray = new Array();

        /* In US English, a date phrase containing only the
         * day of the month begins with a definite article.
         */

        /* In US English, if the word following a definite article
         * begins with a vowel, the definite article is pronounced
         * "THEE".  If the word following the definite article 
         * begins with a consonant, the definite article is
         * pronounced "THUH".
         */
        if (speakDay) {
            if (day == 8 || day == 11 || day == 18) {
                promptsArray.push(getPhrase("MISC_THE1"));
            } else {
                promptsArray.push(getPhrase("MISC_THE2"));
            }
            promptsArray = promptsArray.concat(ordinalPrompts(day));
            return promptsArray;
        } else {
            return void 0;
        }
    }

    if (yearKnown && !monthKnown && !dayKnown) {
        if (speakYear) {
            return yearPrompts(year);
        } else {
            return void 0;
        }
    }
    
    if (!yearKnown && monthKnown && !dayKnown) {
        if (speakMonth) {
            return monthPrompts(month);
        } else {
            return void 0;
        }
    }

    if (yearKnown && monthKnown && !dayKnown) {
        promptsArray = new Array();
        if (speakMonth) {
            promptsArray = promptsArray.concat(monthPrompts(month));
        }
        if (speakYear) {
            promptsArray = promptsArray.concat(yearPrompts(year));
        }
        if (promptsArray.length > 0) {
            return promptsArray;
        } else {
            return void 0;
        }
    }

    if (!yearKnown && monthKnown && dayKnown) {
        promptsArray = new Array();
        if (speakMonth) {
            promptsArray = promptsArray.concat(monthPrompts(month));
        }
        if (speakDay) {
            promptsArray = promptsArray.concat(ordinalPrompts(day));
        }
        if (promptsArray.length > 0) {
            return promptsArray;
        } else {
            return void 0;
        }
    }

    if (yearKnown && monthKnown && dayKnown) {
        promptsArray = new Array();
        if (speakDayOfWeek) {
            // JavaScript months are 0-indexed.
            var date = new Date(year, month - 1, day);
            var dayOfWeek = date.getDay();
            promptsArray = promptsArray.concat(dayOfWeekPrompts(dayOfWeek));
        }
        if (speakMonth) {
            promptsArray = promptsArray.concat(monthPrompts(month));
        }
        if (speakDay) {
            promptsArray = promptsArray.concat(ordinalPrompts(day));
        }
        if (speakYear) {
            promptsArray = promptsArray.concat(yearPrompts(year));
        }
        if (promptsArray.length > 0) {
            return promptsArray;
        } else {
            return void 0;
        }
    }
}

/**
 * Translates the currency value into an array of vox file URLs
 * for playing the currency.  The currency amount must be a number
 * which may contain either a whole number part, or a decimal part, or
 * both, and is optionally preceeded by a currency specifier.  The
 * currency specifier may either be an ISO-4217 currency code, e.g.
 * USD, EUR, JPY, CHF, CAD, GBP, or MXN, or it may be one of the
 * following currency symbols: $ = USD, &#163; = GBP, &#8364; = EUR,
 * &#165; = JPY.  If no currency is specified, the currency of the
 * current locale will be used, e.g. USD for en-US, and EUR for fr-FR.
 *
 * @param value The currency amount.
 * @return An array of URL strings.
 */
function currencyPrompts(value)
{

    var currency;
    var valueStart;
    var decimalPoint;
    var fraction;
    var amount;
    var supportedFormat;

	if (value.substring(0, 3).match(/^[A-Za-z][A-Za-z][A-Za-z]$/)) {
       currency = value.substring(0, 3);
       valueStart = 3;
       supportedFormat = 0;
   	}
    else if (!value.substring(0,1).match(/^[A-Za-z]$/) && (!value.charAt(0).match(/^[0-9]$/)))
     {
		currency = value;
		valueStart = 1;
    } 
    else {
       currency = "USD";
       valueStart = 0;
    }
	/* Pass the whole value if it starts with symbol
	If value = $45.45, send it to TTS without preprocessing  */
	if(valueStart != 1)
	{
	  decimalPoint = value.indexOf(".");
	    if (decimalPoint == -1) {
	        amount = value.substring(valueStart);
	        fraction = 0;
	    } else if (decimalPoint == 3) {
	        if (valueStart < decimalPoint) {
	            amount = value.substring(valueStart, decimalPoint);
	        } else {
	            amount = 0;
		    }
	        fraction = value.substring(decimalPoint + 1);
		} else {
	        amount = value.substring(valueStart, decimalPoint);
		    fraction = value.substring(decimalPoint + 1);
	    }

	    var promptsArray = new Array();
	    if (amount > 0) {
		    
		    /* check for the supported currency
		       Send the whole string as it is, if its not a supported currency.
		       If the value is LBP25.34 (unsupported currency)send the whole value
		       with out processing
		    */
		     var returnedValue;
		     returnedValue = currencyNamePrompts(currency, amount != 1);
		    if(!returnedValue)
		    {
				supportedFormat = 1;
				promptsArray = promptsArray.concat(value);
		    }
		    else
		    {
		    promptsArray = promptsArray.concat(cardinalPrompts(amount));
			promptsArray = promptsArray.concat(currencyNamePrompts(currency, amount != 1));
			if (fraction > 0) {
			    promptsArray.push(getPhrase("MISC_AND"));
			}
			}
		}
		if(supportedFormat !=1)
		{
			if (fraction > 0) 
			{
    			/* added by mamata on 12/09/2003 for fixing ER 70913 */
			if (fraction.length == 1)
				fraction += '0';
			  	/* done adding */
			    promptsArray = promptsArray.concat(cardinalPrompts(fraction));
			    promptsArray = promptsArray.concat(subcurrencyNamePrompts(currency, fraction != 1));
			 }

		    if (amount == 0 && fraction == 0) {
		        promptsArray = promptsArray.concat(cardinalPrompts(0));
		        promptsArray = promptsArray.concat(currencyNamePrompts(currency, true));
		    }
	    }
	}
	else{
		promptsArray = new Array();
		promptsArray = promptsArray.concat(currencyNamePrompts(currency, amount != 1));
	}
	    return promptsArray;
}

function currencyNamePrompts(value, isPlural)
{
    value = value.toUpperCase();
    switch(value) {
        case "USD":
            if (isPlural) {
                return new Array(getPhrase("MISC_DOLLARS"));
            } else {
                return new Array(getPhrase("MISC_DOLLAR"));
            }
        case "GBP":
            if (isPlural) {
                return new Array(getPhrase("MISC_POUNDSSTERLING"));
            } else {
                return new Array(getPhrase("MISC_POUNDSTERLING"));
            }
        case "EUR":
            if (isPlural) {
                return new Array(getPhrase("MISC_EUROS"));
            } else {
                return new Array(getPhrase("MISC_EURO"));
            }
        case "JPY":
            return new Array(getPhrase("MISC_YEN"));
        case "DEM":
            if (isPlural) {
                return new Array(getPhrase("MISC_MARKS"));
            } else {
                return new Array(getPhrase("MISC_MARK"));
            }
        case "ITL":
            if (isPlural) {
                return new Array(getPhrase("MISC_LIRA"));
            } else {
                return new Array(getPhrase("MISC_LIRE"));
            }
        case "FRF":
            if (isPlural) {
                return new Array(getPhrase("MISC_FRANCS"));
            } else {
                return new Array(getPhrase("MISC_FRANC"));
            }
        case "AUD":
            if (isPlural) {
                return new Array(getPhrase("MISC_AUD_DOLLARS"));
            } else {
                return new Array(getPhrase("MISC_AUD_DOLLAR"));
            }
        case "BRL":
            if (isPlural) {
                return new Array(getPhrase("MISC_REAIS"));
            } else {
                return new Array(getPhrase("MISC_REAL"));
            } 
       case "CAD":
            if (isPlural) {
                return new Array(getPhrase("MISC_CAD_DOLLARS"));
            } else {
                return new Array(getPhrase("MISC_CAD_DOLLAR"));
            }
      case "CNY":
            if (isPlural) {
                return new Array(getPhrase("MISC_YUANS"));
            } else {
                return new Array(getPhrase("MISC_YUAN"));
            }
      case "CUP":
            if (isPlural) {
                return new Array(getPhrase("MISC_PESOS"));
            } else {
                return new Array(getPhrase("MISC_PESO"));
            }
      case "DKK":
            if (isPlural) {
                return new Array(getPhrase("MISC_KRONERS"));
            } else {
                return new Array(getPhrase("MISC_KRONER"));
            }
      case "HKD":
            if (isPlural) {
                return new Array(getPhrase("MISC_HKD_DOLLARS"));
            } else {
                return new Array(getPhrase("MISC_HKD_DOLLAR"));
            }
       case "INR":
            if (isPlural) {
                return new Array(getPhrase("MISC_IND_RUPEES"));
            } else {
                return new Array(getPhrase("MISC_IND_RUPEE"));
            }
       case "IRR":
           if (isPlural) {
                return new Array(getPhrase("MISC_RIYALS"));
            } else {
                return new Array(getPhrase("MISC_RIYAL"));
            }
       case "MXN":
            if (isPlural) {
                return new Array(getPhrase("MISC_PESOS"));
            } else {
                return new Array(getPhrase("MISC_PESO"));
            }
       case "PHP":
            if (isPlural) {
                return new Array(getPhrase("MISC_PESOS"));
            } else {
                return new Array(getPhrase("MISC_PESO"));
            }
       case "PKR":
            if (isPlural) {
                return new Array(getPhrase("MISC_PKR_RUPEES"));
            } else {
                return new Array(getPhrase("MISC_PKR_RUPEE"));
            }
       case "RUB":
            if (isPlural) {
                return new Array(getPhrase("MISC_RUBLES"));
            } else {
                return new Array(getPhrase("MISC_RUBLE"));
            }
       case "SAR":
            if (isPlural) {
                return new Array(getPhrase("MISC_RIYALS"));
            } else {
                return new Array(getPhrase("MISC_RIYAL"));
            }
       case "SGD":
            if (isPlural) {
                return new Array(getPhrase("MISC_SGD_DOLLARS"));
            } else {
                return new Array(getPhrase("MISC_SGD_DOLLAR"));
            }
        default:
			return void 0;
            
    }
}

function subcurrencyNamePrompts(value, isPlural)
{
    value = value.toUpperCase();
    switch(value) {
        case "USD":
            if (isPlural) {
                return new Array(getPhrase("MISC_CENTS"));
            } else {
                return new Array(getPhrase("MISC_CENT"));
            }
        case "GBP":
            if (isPlural) {
                return new Array(getPhrase("MISC_PENCE"));
            } else {
                return new Array(getPhrase("MISC_PENNY"));
            }
        case "EUR":
            if (isPlural) {
                return new Array(getPhrase("MISC_CENTS"));
            } else {
                return new Array(getPhrase("MISC_CENT"));
            }
        case "JPY":
            if (isPlural) {
                return new Array(getPhrase("MISC_CEN"));
            } else {
                return new Array(getPhrase("MISC_CEN"));
            }
        case "DEM":
            if (isPlural) {
                return new Array(getPhrase("MISC_PFENNIGE"));
            } else {
                return new Array(getPhrase("MISC_PFENNIG"));
            }
        case "ITL":
            return void 0;
        case "FRF":
            if (isPlural) {
                return new Array(getPhrase("MISC_CENTIMES"));
            } else {
                return new Array(getPhrase("MISC_CENTIME"));
            }
       case "AUD":
            if (isPlural) {
                return new Array(getPhrase("MISC_CENTS"));
            } else {
                return new Array(getPhrase("MISC_CENT"));
            }
        case "BRL":
            if (isPlural) {
                return new Array(getPhrase("MISC_CENTAVOS"));
            } else {
                return new Array(getPhrase("MISC_CENTAVO"));
            } 
       case "CAD":
            if (isPlural) {
                return new Array(getPhrase("MISC_CENTS"));
            } else {
                return new Array(getPhrase("MISC_CENT"));
            }
      case "CNY":
            if (isPlural) {
                return new Array(getPhrase("MISC_FENS"));
            } else {
                return new Array(getPhrase("MISC_FEN"));
            }
      case "CUP":
            if (isPlural) {
                return new Array(getPhrase("MISC_CENTAVOS"));
            } else {
                return new Array(getPhrase("MISC_CENTAVO"));
            }
      case "DKK":
            if (isPlural) {
                return new Array(getPhrase("MISC_ORES"));
            } else {
                return new Array(getPhrase("MISC_ORE"));
            }
      case "HKD":
            if (isPlural) {
                return new Array(getPhrase("MISC_CENTS"));
            } else {
                return new Array(getPhrase("MISC_CENT"));
            }
       case "INR":
            if (isPlural) {
                return new Array(getPhrase("MISC_PAISE"));
            } else {
                return new Array(getPhrase("MISC_PAISE"));
            }
       case "IRR":
            if (isPlural) {
                return new Array(getPhrase("MISC_DIRHAMS"));
            } else {
                return new Array(getPhrase("MISC_DIRHAM"));
            }
       case "MXN":
            if (isPlural) {
                return new Array(getPhrase("MISC_CENTAVOS"));
            } else {
                return new Array(getPhrase("MISC_CENTAVO"));
            }
       case "PHP":
            if (isPlural) {
                return new Array(getPhrase("MISC_CENTAVOS"));
            } else {
                return new Array(getPhrase("MISC_CENTAVO"));
            }
       case "PKR":
            if (isPlural) {
                return new Array(getPhrase("MISC_PAISA"));
            } else {
                return new Array(getPhrase("MISC_PAISA"));
            }
       case "RUB":
            if (isPlural) {
                return new Array(getPhrase("MISC_COPECKS"));
            } else {
                return new Array(getPhrase("MISC_COPECK"));
            }
       case "SAR":
            if (isPlural) {
                return new Array(getPhrase("MISC_DIRHAMS"));
            } else {
                return new Array(getPhrase("MISC_DIRHAM"));
            }
       case "SGD":
            if (isPlural) {
                return new Array(getPhrase("MISC_CENTS"));
            } else {
                return new Array(getPhrase("MISC_CENT"));
            }
        default:
            return void 0;
    }
}

function timePrompts(value)
{
    var hours;
    var minutes;
    var seconds;
    var format;
    if (value.match(/^[0-9]+$/)) {
        if (value.length == 4) {
            hours = value.substring(0, 2);
            minutes = value.substring(2, 4);
        } else if (value.length == 6) {
            hours = value.substring(0, 2);
            minutes = value.substring(2, 4);
            seconds = value.substring(4, 6);
        } else {
            return void 0;
        }
        if (hours >= 1 && hours <= 12) {
            format = "?";
        } else if (hours == 0 || (hours >= 13 && hours <= 24)) {
            format = "h";
        } else {
            return void 0;
        }
    } else if (value.match(/^[0-9]+[?hap]$/)) {
        if (value.length == 5) {
            hours = value.substring(0, 2);
            minutes = value.substring(2, 4);
            format = value.substring(4, 5);
        } else if (value.length == 7) {
            hours = value.substring(0, 2);
            minutes = value.substring(2, 4);
            seconds = value.substring(4, 6);
            format = value.substring(6, 7);
        } else {
            return void 0;
        }
    } else {
        return void 0;
    }

    var promptsArray = new Array();
    promptsArray = promptsArray.concat(cardinalPrompts(hours));
    if (minutes == "00") {
        if (format == "a" || format == "p" || format == "?") {
            promptsArray.push(getPhrase("MISC_OCLOCK"));
        } else if (format == "h") {
            //promptsArray.push(getPhrase("CARDINALS_HUNDRED"));
            promptsArray.push(getPhrase("MISC_HOURS"));
        }
    } else if (minutes > 0 && minutes < 10) {
        promptsArray = promptsArray.concat(alphanumericPrompts("O"));
        promptsArray = promptsArray.concat(cardinalPrompts(minutes));
    } else {
        promptsArray = promptsArray.concat(cardinalPrompts(minutes));
    }

    if (format == "a") {
        promptsArray.push(getPhrase("MISC_A"));
        promptsArray.push(getPhrase("MISC_M"));
    } else if (format == "p") {
        promptsArray.push(getPhrase("MISC_PM"));
    }

    if (seconds != undefined)  {
        promptsArray.push(getPhrase("MISC_AND"));
        promptsArray.push(cardinalPrompts(seconds));
        if (seconds == 1) {
            promptsArray.push(getPhrase("MISC_SECOND"));
        } else {
            promptsArray.push(getPhrase("MISC_SECONDS"));
        }
    }

    return promptsArray;
}

function yearPrompts(year)
{
    var century = year.substring(0,2);
    var rest = year.substring(2,4);

    var promptsArray = new Array();
    if (century == "20") {
        promptsArray = promptsArray.concat(cardinalPrompts(2000));
    } else {
        promptsArray = promptsArray.concat(cardinalPrompts(century));
    }

    if (century != "20") {
        if (rest == "00") {
            promptsArray.push(getPhrase("CARDINALS_HUNDRED"));
        } else if (rest > 0 && rest < 10) {
            promptsArray = promptsArray.concat(alphanumericPrompts("O"));
            promptsArray =
                promptsArray.concat(cardinalPrompts(rest.substring(1,2)));
        } else {
            promptsArray = promptsArray.concat(cardinalPrompts(rest));
        }
    } else {
        if (rest != "00") {
            promptsArray = promptsArray.concat(cardinalPrompts(rest))
        }
    }
    return promptsArray;
}

function monthPrompts(month)
{
    if (month >= 1 && month <= 12) {
        return new Array(getPhrase(eval("'MONTHS_" + month +"'")));
    } else {
        return void 0;
    }
}

function dayOfWeekPrompts(dayOfWeek)
{
    if (dayOfWeek >= 0 && dayOfWeek <= 6) {
        return new Array(getPhrase(eval("'DAYS_" + dayOfWeek +"'")));
    } else {
        return void 0;
    }
}

function cardinalPrompts(number)
{
    if (number === undefined || !isFinite(number)) {
        return void 0;
    }

    if (number > 999999999999999) {
        return void 0;
    }

    var isNegative;

    if (number < 0) {
        isNegative = true;
        number = Math.abs(number);
    }
    
    if (number == 0) {
        return new Array(getPhrase("CARDINALS_000"));
    }

    var str = new String(number);
    var arr = str.split(".");
	var fractionalPart = 0;
	number  = new Number(arr[0]);
    if (arr[1] != undefined)
    {	    
    	fractionalPart = new Number(arr[1]);
    }
    /*var fractionalPart = number - Math.floor(number);
    number = number - fractionalPart;*/

    var promptsArray = new Array();
    var magnitude = 0;
    while (number > 0) {
        var endDigits = number % 1000;
        if (endDigits != 0) {
            promptsArray = threeDigitsPrompts(endDigits).concat(magnitudePrompts(magnitude), promptsArray);
        }
        number = number - endDigits;
        number = number / 1000;
        magnitude++;
    }
    if (isNegative) {
        promptsArray.unshift(getPhrase("MISC_MINUS"));
    }

    if ( (fractionalPart != undefined) && (fractionalPart != 0) ) {
        fractionalPart = fractionalPart.toString();//.substring(2);
        promptsArray.push(getPhrase("MISC_POINT")); 
        promptsArray = promptsArray.concat(alphanumericPrompts(fractionalPart));
    }
    return promptsArray;
} 

function threeDigitsPrompts(number)
{
    // assert number >=0 && number <= 999
    if (number < 0 || number > 999) {
        return void 0;
    }

    var hundreds = Math.floor(number / 100);
    var tensAndOnes = number % 100;

    var promptsArray = new Array();
    if (hundreds > 0) {
        promptsArray.push(getPhrase(eval("'CARDINALS_"+hundreds +"00'")));
    }
    if (tensAndOnes > 0) {
        if (tensAndOnes < 10) {
            promptsArray.push(getPhrase(eval("'CARDINALS_00"+tensAndOnes+"'")));
        } else {
            promptsArray.push(getPhrase(eval("'CARDINALS_0"+tensAndOnes+"'")));
        }
    }
    return promptsArray;
}

function ordinalThreeDigitsPrompts(number)
{
    // assert number >=0 && number <= 999
    if (number < 0 || number > 999) {
        return void 0;
    }

    var hundreds = Math.floor(number / 100);
    var tensAndOnes = number % 100;

    var promptsArray = new Array();
    if (hundreds > 0) {
        if (tensAndOnes > 0) {
            promptsArray.push(getPhrase(eval("'CARDINALS_"+hundreds +"00'")));
        } else {
            promptsArray.push(getPhrase(eval("'CARDINALS_00"+hundreds +"'")));
            promptsArray.push(getPhrase("ORDINALS_"));
        }
    }

    if (tensAndOnes > 0) {
        if (tensAndOnes < 10) {
            promptsArray.push(getPhrase(eval("'ORDINALS_00"+tensAndOnes+"'")));
        } else {
            promptsArray.push(getPhrase(eval("'ORDINALS_0"+tensAndOnes+"'")));
        }
    }
    return promptsArray;
}

/**
 * Returns an array of vox file URLs corresponding to the
 * number 10^(3*number).
 *
 * Note the differences between the American and European systems:
 * <pre>
 *       Order of
 *       Magnitude      American    European
 *         10^0            -           -
 *         10^3         thousand    thousand
 *         10^6          million     million
 *         10^9          billion    thousand million or milliard
 *         10^12        trillion     billion
 * </pre>
 *
 * @param number The order of magnitude.
 * @return An array of URL strings.
 */
function magnitudePrompts(number)
{
    switch (number) {
        case 0:
            return new Array();
        case 1:
            return new Array(getPhrase("CARDINALS_THOUSAND")); 
        case 2:
            return new Array(getPhrase("CARDINALS_MILLION"));
        case 3:
            return new Array(getPhrase("CARDINALS_BILLION"));
        case 4:
            return new Array(getPhrase("CARDINALS_TRILLION"));
        default:
            return void 0;
    }
}

function ordinalMagnitudePrompts(number)
{
    switch (number) {
        case 0:
            return new Array();
        case 1:
            return new Array(getPhrase("ORDINALS_THOUSANDTH")); 
        case 2:
            return new Array(getPhrase("ORDINALS_MILLIONTH")); 
        case 3:
            return new Array(getPhrase("ORDINALS_BILLIONTH")); 
        case 4:
            return new Array(getPhrase("ORDINALS_TRILLIONTH")); 
        default:
            return void 0;
    }
}

function ordinalPrompts(number)
{
    if (number === undefined || !isFinite(number) || number <= 0 ||
        ((number - Math.floor(number)) != 0) || number > 999999999999999 ) {
        return void 0;
    }

    var promptsArray = new Array();
    var magnitude = 0;
    var ordinal = true;
    while (number > 0) {
        var endDigits = number % 1000;
        if (endDigits != 0) {
            if (magnitude == 0 && ordinal) {
                promptsArray =
                    ordinalThreeDigitsPrompts(endDigits).concat(promptsArray);
                ordinal = false;
            } else {
                if (ordinal) {
                    promptsArray = threeDigitsPrompts(endDigits).concat(ordinalMagnitudePrompts(magnitude), promptsArray);
                    ordinal = false;
                } else {
                    promptsArray = threeDigitsPrompts(endDigits).concat(magnitudePrompts(magnitude), promptsArray);
                }
            }
        }
        number = number - endDigits;
        number = number / 1000;
        magnitude++;
    }
    return promptsArray;
}

function alphanumericPrompts(string)
{
    var i;
    var ch;
    var promptsArray = new Array();
    string = string.toLowerCase();
    for (i = 0; i < string.length; i++) {
        ch = string.charAt(i);
        ch = ch.toLowerCase();
        if (ch.match(/\d/)) {
            promptsArray.push(getPhrase(eval("'CARDINALS_00" + ch + "'")));
        } else if (ch.match(/[a-z]/)) {
            promptsArray.push(getPhrase(eval("'LETTERS_" + ch.toUpperCase() + "'")));
        } else if (ch.match(/\+/)) {
            promptsArray.push(getPhrase("MISC_PLUS"));
        } else if (ch.match(/\</)) {
            promptsArray.push(getPhrase("MISC_LESSTHAN"));
        } else if (ch.match(/\=/)) {
            promptsArray.push(getPhrase("MISC_EQUALS"));
        } else if (ch.match(/\%/)) {
            promptsArray.push(getPhrase("MISC_PERCENT"));
        } else if (ch.match(/\-/)) {
            promptsArray.push(getPhrase("MISC_MINUS"));
        } else if (ch.match(/\>/)) {
            promptsArray.push(getPhrase("MISC_GREATERTHAN"));
        } else if (ch.match(/\&/)) {
            promptsArray.push(getPhrase("MISC_AND"));
        } else if (ch.match(/\./)) {
            promptsArray.push(getPhrase("MISC_DOT"));
        } else if (ch.match(/\#/)) {
            promptsArray.push(getPhrase("MISC_POUND"));
        } else if (ch.match(/\*/)) {
            promptsArray.push(getPhrase("MISC_STAR"));
        } else if (ch.match(/\@/)) {
            promptsArray.push(getPhrase("MISC_AT"));
        }
        else {
            promptsArray.push(getPhrase("MISC_SPECIALCHAR"));
        }
        
    }
    return promptsArray;
}


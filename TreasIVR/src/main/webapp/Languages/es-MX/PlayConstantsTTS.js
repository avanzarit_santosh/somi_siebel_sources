function getPhrase(param)
{
	/* cardinals */
	
	var CARDINALS_000 = "zero";
	var CARDINALS_001 = "one";
	var CARDINALS_002 = "two";
	var CARDINALS_003 = "three";
	var CARDINALS_004 = "four";	
	var CARDINALS_005 = "five";
	var CARDINALS_006 = "six";
	var CARDINALS_007 = "seven";
	var CARDINALS_008 = "eight";
	var CARDINALS_009 = "nine";
	var CARDINALS_010 = "ten";
	var CARDINALS_011 = "eleven";
	var CARDINALS_012 = "twelve";
	var CARDINALS_013 = "thirteen";
	var CARDINALS_014 = "fourteen";	
	var CARDINALS_015 = "fifteen";
	var CARDINALS_016 = "sixteen";
	var CARDINALS_017 = "seventeen";
	var CARDINALS_018 = "eighteen";
	var CARDINALS_019 = "nineteen";
	var CARDINALS_020 = "twenty";
	var CARDINALS_021 = "twenty one";
	var CARDINALS_022 = "twenty two";
	var CARDINALS_023 = "twenty three";
	var CARDINALS_024 = "twenty four";	
	var CARDINALS_025 = "twenty five";
	var CARDINALS_026 = "twenty six";
	var CARDINALS_027 = "twenty seven";
	var CARDINALS_028 = "twenty eight";
	var CARDINALS_029 = "twenty nine";
	var CARDINALS_030 = "thirty";
	var CARDINALS_031 = "thirty one";
	var CARDINALS_032 = "thirty two";
	var CARDINALS_033 = "thirty three";
	var CARDINALS_034 = "thirty four";	
	var CARDINALS_035 = "thirty five";
	var CARDINALS_036 = "thirty six";
	var CARDINALS_037 = "thirty seven";
	var CARDINALS_038 = "thirty eight";
	var CARDINALS_039 = "thirty nine";
	var CARDINALS_040 = "forty";
	var CARDINALS_041 = "forty one";
	var CARDINALS_042 = "forty two";
	var CARDINALS_043 = "forty three";
	var CARDINALS_044 = "forty four";	
	var CARDINALS_045 = "forty five";
	var CARDINALS_046 = "forty six";
	var CARDINALS_047 = "forty seven";
	var CARDINALS_048 = "forty eight";
	var CARDINALS_049 = "forty nine";
	var CARDINALS_050 = "fifty";
	var CARDINALS_051 = "fifty one";
	var CARDINALS_052 = "fifty two";
	var CARDINALS_053 = "fifty three";
	var CARDINALS_054 = "fifty four";	
	var CARDINALS_055 = "fifty five";
	var CARDINALS_056 = "fifty six";
	var CARDINALS_057 = "fifty seven";
	var CARDINALS_058 = "fifty eight";
	var CARDINALS_059 = "fifty nine";
	var CARDINALS_060 = "sixty";
	var CARDINALS_061 = "sixty one";
	var CARDINALS_062 = "sixty two";
	var CARDINALS_063 = "sixty three";
	var CARDINALS_064 = "sixty four";	
	var CARDINALS_065 = "sixty five";
	var CARDINALS_066 = "sixty six";
	var CARDINALS_067 = "sixty seven";
	var CARDINALS_068 = "sixty eight";
	var CARDINALS_069 = "sixty nine";
	var CARDINALS_070 = "seventy";
	var CARDINALS_071 = "seventy one";
	var CARDINALS_072 = "seventy two";
	var CARDINALS_073 = "seventy three";
	var CARDINALS_074 = "seventy four";	
	var CARDINALS_075 = "seventy five";
	var CARDINALS_076 = "seventy six";
	var CARDINALS_077 = "seventy seven";
	var CARDINALS_078 = "seventy eight";
	var CARDINALS_079 = "seventy nine";
	var CARDINALS_080 = "eighty";
	var CARDINALS_081 = "eighty one";
	var CARDINALS_082 = "eighty two";
	var CARDINALS_083 = "eighty three";
	var CARDINALS_084 = "eighty four";	
	var CARDINALS_085 = "eighty five";
	var CARDINALS_086 = "eighty six";
	var CARDINALS_087 = "eighty seven";
	var CARDINALS_088 = "eighty eight";
	var CARDINALS_089 = "eighty nine";
	var CARDINALS_090 = "ninety";
	var CARDINALS_091 = "ninety one";
	var CARDINALS_092 = "ninety two";
	var CARDINALS_093 = "ninety three";
	var CARDINALS_094 = "ninety four";	
	var CARDINALS_095 = "ninety five";
	var CARDINALS_096 = "ninety six";
	var CARDINALS_097 = "ninety seven";
	var CARDINALS_098 = "ninety eight";
	var CARDINALS_099 = "ninety nine";
	var CARDINALS_100 = "one hundred";
	var CARDINALS_200 = "two hundred";
	var CARDINALS_300 = "three hundred";
	var CARDINALS_400 = "four hundred";
	var CARDINALS_500 = "five hundred";
	var CARDINALS_600 = "six hundred";
	var CARDINALS_700 = "seven hundred";
	var CARDINALS_800 = "eight hundred";
	var CARDINALS_900 = "nine hundred";
	var CARDINALS_BILLION = "billion";
	var CARDINALS_BILLIONS = "billions";
	var CARDINALS_HUNDRED = "hundred";
	var CARDINALS_HUNDREDS = "hundreds";
	var CARDINALS_MILLION = "million";
	var CARDINALS_MILLIONS = "millions";
	var CARDINALS_THOUSAND = "thousand";
	var CARDINALS_THOUSANDS = "thousands";
	var CARDINALS_TRILLION = "trillion";
	var CARDINALS_TRILLIONS = "trillions";
	
	/* end of cardinals */
	
	/*	days	*/
	
	var DAYS_0 = "sunday";
	var DAYS_1 = "monday";
	var DAYS_2 = "tuesday";
	var DAYS_3 = "wednesday";
	var DAYS_4 = "thursday";
	var DAYS_5 = "friday";
	var DAYS_6 = "saturday";
	
	/*	end of days	*/
	
	
	/*	dtmf	*/
	
		//vox file play
		
	/*	end of dtmf	*/
	
	
	/*	letters	*/
	
	var LETTERS_A = "a";
	var LETTERS_B = "b";
	var LETTERS_C = "c";
	var LETTERS_D = "d";
	var LETTERS_E = "e";
	var LETTERS_F = "f";
	var LETTERS_G = "g";
	var LETTERS_H = "h";
	var LETTERS_I = "i";
	var LETTERS_J = "j";
	var LETTERS_K = "k";
	var LETTERS_L = "l";
	var LETTERS_M = "m";
	var LETTERS_N = "n";
	var LETTERS_O = "o";
	var LETTERS_P = "p";
	var LETTERS_Q = "q";
	var LETTERS_R = "r";
	var LETTERS_S = "s";
	var LETTERS_T = "t";
	var LETTERS_U = "u";
	var LETTERS_V = "v";
	var LETTERS_W = "w";
	var LETTERS_X = "x";
	var LETTERS_Y = "y";
	var LETTERS_Z = "z";
	
	/*	end of letters	*/
	
	
	/*	miscellaneous	*/
	
	//350ms.vox
	var MISC_A = "a";	
	var MISC_M = "m";
	var MISC_AND = "and";
	var MISC_AT = "at";
	var MISC_AUD_DOLLARS = "australian dollars";
	var MISC_AUD_DOLLAR = "australian dollar";
	var MISC_CAD_DOLLARS = "canadian dollars";
	var MISC_CAD_DOLLAR = "canadian dollar";
	var MISC_CEN = "cen";
	var MISC_CENT = "cent";
	var MISC_CENTIME = "centime";
	var MISC_CENTIMES = "centimes";
	var MISC_CENTS = "cents";
	var MISC_CENTAVOS = "centavos";
	var MISC_CENTAVO = "centavo";
	var MISC_COPECKS = "copecks";
	var MISC_COPECK = "copeck";
	var MISC_DOLLAR = "dollar";
	var MISC_DOLLARS = "dollars";
	var MISC_DIRHAMS = "dirhams";
	var MISC_DIRHAM = "dirham";
	var MISC_DOT = "dot";
	var MISC_EQUALS = "equals";
	var MISC_EURO = "euro";
	var MISC_EUROS = "euros";
	var MISC_EXTENSION = "extension";
	var MISC_FENS = "fens";
	var MISC_FEN = "fen";
	var MISC_FALSE = "false";
	var MISC_FRANC = "franc";
	var MISC_FRANCS = "francs";
	var MISC_GREATER = "greater than";
	var MISC_HKD_DOLLARS = "hong kong dollars";
	var MISC_HKD_DOLLAR = "hong kong dollar";
	var MISC_HOUR = "hour";
	var MISC_HOURS = "hours";
	var MISC_KRONERS = "kroners";
	var MISC_KRONER = "kroner";
	var MISC_IND_RUPEES ="indian rupees";
	var MISC_IND_RUPEE ="indian rupee";
	var MISC_LESS = "less than";
	var MISC_LIRA = "lira";
	var MISC_LIRE = "lire";
	var MISC_MARK = "mark";
	var MISC_MARKS = "marks";
	var MISC_MIDNIGHT = "mid night";
	var MISC_MINUS = "minus";
	var MISC_MINUTE = "minute";
	var MISC_MINUTES = "minutes";
	var MISC_NOON = "noon";
	var MISC_OCLOCK = "o clock"; //check with yi
	var MISC_ORES = "ores";
	var MISC_ORE = "ore";
	var MISC_PAISE = "paisae";
	var MISC_PAISA = "paisa";
	var MISC_PENCE = "pence";		
	var MISC_PENNY = "penny";
	var MISC_PERCENT = "percent";
	var MISC_PESOS = "pesos";
	var MISC_PESO = "peso";
	var MISC_PFENNIG = "pfennig"
	var MISC_PFENNIGE = "pfennige";
	var MISC_PKR_RUPEES = "pakistani rupees";
	var MISC_PKR_RUPEES = "pakistani rupee";
	var MISC_PLUS = "plus";
	var MISC_POINT = "point";
	var MISC_POUND = "pound";
	var MISC_POUNDS = "pounds";
	var MISC_POUNDSSTR = "pounds";
	var MISC_POUNDSTR = "pound";
	var MISC_PM = "p m";
	var MISC_REAIS = "reais";
	var MISC_REAL = "real";
	var MISC_RIYALS = "riyals";
	var MISC_RIYAL = "riyal";
	var MISC_RUBLES = "rubles";
	var MISC_RUBLE = "ruble";
	var MISC_SGD_DOLLARS = "singapore dollars";
	var MISC_SGD_DOLLAR = "singapore dollar";
	var MISC_SECOND = "second";
	var MISC_SECONDS = "seconds";
	var MISC_STAR = "star";
	var MISC_TRUE = "true";
	var MISC_THE1 = "thee";
	var MISC_THE2 = "tha"; ///check with yi
	var MISC_YEN = "yen";
	var MISC_YUANS = "Yuan renminbis";
	var MISC_YUAN = "Yuan renminbi";
	var MISC_LESSTHAN = "less than";
	var MISC_GREATERTHAN = "greater than";
	var MISC_SPECIALCHAR = "special character";
	
	/*	miscellaneous	*/
	
	/*	months	*/
	
	var MONTHS_01 = "january";
	var MONTHS_02 = "february";
	var MONTHS_03 = "march";
	var MONTHS_04 = "april";
	var MONTHS_05 = "may";
	var MONTHS_06 = "june";
	var MONTHS_07 = "july";
	var MONTHS_08 = "august";
	var MONTHS_09 = "september";
	var MONTHS_10 = "october";
	var MONTHS_11 = "november";
	var MONTHS_12 = "december";		
		
	/*	end of months	*/
	
	
	/*	ordinals	*/
	
	var ORDINALS_001 = "first";
	var ORDINALS_002 = "second";
	var ORDINALS_003 = "third";
	var ORDINALS_004 = "fourth";
	var ORDINALS_005 = "fifth";
	var ORDINALS_006 = "sixth";
	var ORDINALS_007 = "seventh";
	var ORDINALS_008 = "eighth";
	var ORDINALS_009 = "ninth";
	var ORDINALS_010 = "tenth";
	var ORDINALS_011 = "eleventh";
	var ORDINALS_012 = "twelfth";
	var ORDINALS_013 = "thirteenth";
	var ORDINALS_014 = "fourteenth";
	var ORDINALS_015 = "fifteenth";
	var ORDINALS_016 = "sixteenth";
	var ORDINALS_017 = "seventeenth";
	var ORDINALS_018 = "eighteenth";
	var ORDINALS_019 = "nineteenth";
	var ORDINALS_020 = "twentieth";
	var ORDINALS_021 = "twenty first";
	var ORDINALS_022 = "twenty second";
	var ORDINALS_023 = "twenty third";
	var ORDINALS_024 = "twenty fourth";
	var ORDINALS_025 = "twenty fifth";
	var ORDINALS_026 = "twenty sixth";
	var ORDINALS_027 = "twenty seventh";
	var ORDINALS_028 = "twenty eighth";
	var ORDINALS_029 = "twenty ninth";
	var ORDINALS_030 = "thirtieth";
	var ORDINALS_031 = "thirty first";
	var ORDINALS_032 = "thirty second";
	var ORDINALS_033 = "thirty third";
	var ORDINALS_034 = "thirty fourth";
	var ORDINALS_035 = "thirty fifth";
	var ORDINALS_036 = "thirty sixth";
	var ORDINALS_037 = "thirty seventh";
	var ORDINALS_038 = "thirty eighth";
	var ORDINALS_039 = "thirty ninth";
	var ORDINALS_040 = "fortieth";
	var ORDINALS_041 = "forty first";
	var ORDINALS_042 = "forty second";
	var ORDINALS_043 = "forty third";
	var ORDINALS_044 = "forty fourth";
	var ORDINALS_045 = "forty fifth";
	var ORDINALS_046 = "forty sixth";
	var ORDINALS_047 = "forty seventh";
	var ORDINALS_048 = "forty eighth";
	var ORDINALS_049 = "forty ninth";
	var ORDINALS_050 = "fiftieth";
	var ORDINALS_051 = "fifty first";
	var ORDINALS_052 = "fifty second";
	var ORDINALS_053 = "fifty third";
	var ORDINALS_054 = "fifty fourth";
	var ORDINALS_055 = "fifty fifth";
	var ORDINALS_056 = "fifty sixth";
	var ORDINALS_057 = "fifty seventh";
	var ORDINALS_058 = "fifty eighth";
	var ORDINALS_059 = "fifty ninth";
	var ORDINALS_060 = "sixtieth";
	var ORDINALS_061 = "sixty first";
	var ORDINALS_062 = "sixty second";
	var ORDINALS_063 = "sixty third";
	var ORDINALS_064 = "sixty fourth";
	var ORDINALS_065 = "sixty fifth";
	var ORDINALS_066 = "sixty sixth";
	var ORDINALS_067 = "sixty seventh";
	var ORDINALS_068 = "sixty eighth";
	var ORDINALS_069 = "sixty ninth";
	var ORDINALS_070 = "seventieth";
	var ORDINALS_071 = "seventy first";
	var ORDINALS_072 = "seventy second";
	var ORDINALS_073 = "seventy third";
	var ORDINALS_074 = "seventy fourth";
	var ORDINALS_075 = "seventy fifth";
	var ORDINALS_076 = "seventy sixth";
	var ORDINALS_077 = "seventy seventh";
	var ORDINALS_078 = "seventy eighth";
	var ORDINALS_079 = "seventy ninth";
	var ORDINALS_080 = "eightieth";
	var ORDINALS_081 = "eighty first";
	var ORDINALS_082 = "eighty second";
	var ORDINALS_083 = "eighty third";
	var ORDINALS_084 = "eighty fourth";
	var ORDINALS_085 = "eighty fifth";
	var ORDINALS_086 = "eighty sixth";
	var ORDINALS_087 = "eighty seventh";
	var ORDINALS_088 = "eighty eighth";
	var ORDINALS_089 = "eighty ninth";
	var ORDINALS_090 = "ninetieth";
	var ORDINALS_091 = "ninety first";
	var ORDINALS_092 = "ninety second";
	var ORDINALS_093 = "ninety third";
	var ORDINALS_094 = "ninety fourth";
	var ORDINALS_095 = "ninety fifth";
	var ORDINALS_096 = "ninety sixth";
	var ORDINALS_097 = "ninety seventh";
	var ORDINALS_098 = "ninety eighth";
	var ORDINALS_099 = "ninety ninth";
	var ORDINALS_BILLIONTH = "billionth";
	var ORDINALS_MILLIONTH = "millionth";
	var ORDINALS_HUNDREDTH = "hundredth";
	var ORDINALS_TRILLIONTH = "trillionth";
		
	/*	end of ordinals	*/
	return eval(param);
}

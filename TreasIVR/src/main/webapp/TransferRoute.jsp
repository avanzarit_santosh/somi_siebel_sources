<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ page import="java.util.*" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%
	StringBuffer data = new StringBuffer();
    try {
        request.setCharacterEncoding("UTF-8");
    } catch (java.io.UnsupportedEncodingException e) {
        e.printStackTrace();
    }
    if (request.getMethod().equalsIgnoreCase("HEAD")) {
        return;
    }


    session.setAttribute("PAGEID", "TransferRoute");
    java.util.Enumeration params = request.getParameterNames();
    String name;
    String value;
    while (params.hasMoreElements()) {
        name = (String) params.nextElement();
        value = request.getParameter(name);
        session.setAttribute(name, value);
    }

    processBegin(pageContext);

    



if ("SET_AGENT_LEG_FLAG".equals(request.getParameter("ACTION")))
{
	%><XMLPage TYPE="IVR" PAGEID="" SESSIONID="" HREF="<%
out.print(getAbsoluteURL(pageContext, "", ""));
%>"><SET VARNAME="$agent-leg-flag$" VALUE="true"/></XMLPage><%

	return;
}





	String dataReturn = request.getParameter("IDataReturn");

	if (dataReturn == null || "".equals(dataReturn))
	{
		String vxmlHeader = getVxmlHeader(pageContext);
		out.print(vxmlHeader);

		%><form>
   <object name="myUData" classid="CRData:genericAction">
      <param name="IServer_Action" value="UData"/><param name='IVR_CALL_TYPE' value='<%
            data.append(getXMLEncodedString(lookUp(pageContext,"IVR_CALL_TYPE")));
            out.print(data);
            data.setLength(0);
        %>'/><param name='SSN' value='<%
            data.append(getXMLEncodedString(lookUp(pageContext,"SSN")));
            out.print(data);
            data.setLength(0);
        %>'/><param name='CREDIT_CALLER' value='<%
            data.append(getXMLEncodedString(lookUp(pageContext,"CREDIT_CALLER")));
            out.print(data);
            data.setLength(0);
        %>'/><param name='SSN_VALID' value='<%
            data.append(getXMLEncodedString(lookUp(pageContext,"SSN_VALID")));
            out.print(data);
            data.setLength(0);
        %>'/></object>
   <block>
      <goto next="TransferRoute.jsp?IDataReturn=TRUE"/>
   </block>
</form><%

		String vxmlFooter = getVxmlFooter();
		out.print(vxmlFooter);
		
		return;
	}

%><XMLPage TYPE="IVR" PAGEID="" SESSIONID="">
<SET VARNAME="$scripturl$" VALUE="<%
out.print(lookUp(pageContext,"ROOTDOCUMENT"));
%>?ScriptID=$sid$&amp;ScriptData=$scriptdata$"/>
<QUEUE_CALL USR_PARAMS="GenesysRouteDN:<%
            data.append(lookUp(pageContext,"ROUTE_POINT"));
            out.print(data);
            data.setLength(0);
        %>"/><%
out.print(getLegWaitCode(pageContext, "-1","TransferRoute"));
%></XMLPage><%

    processAnywhere(pageContext);

    processEnd(pageContext);

%>
     <%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
<?xml version="1.0" encoding="utf-8" ?>

<%@ page language="java" import="java.util.*,java.lang.*,java.io.*,org.w3c.dom.*,org.w3c.dom.traversal.*,javax.xml.parsers.*,org.xml.sax.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%

    String label = "";
    String query = "";
    String strHeaderVXML = "";
    String strVXML = "";
    String strFooterVXML = "";

    try {
        request.setCharacterEncoding("UTF-8");
    } catch (java.io.UnsupportedEncodingException e) {
        e.printStackTrace();
    }
    if (request.getMethod().equalsIgnoreCase("HEAD")) {
        return;
    }

    session.setAttribute("PAGEID", "TransferUserData");

    java.util.Enumeration params = request.getParameterNames();
    String paramName;
    String paramValue;
    while (params.hasMoreElements()) {
        paramName = (String) params.nextElement();
        paramValue = request.getParameter(paramName);
        session.setAttribute(paramName, paramValue);
    }
    label = lookUp(pageContext, "ROOTDOCUMENT");
    query = getQueryString(pageContext).trim();

    processBegin(pageContext);

            
        // Header Section
        strHeaderVXML += getVxmlHeader(pageContext);
        out.print(strHeaderVXML);

        // Header Section End --- Contd...

        String responseURL = request.getParameter("udGetResponseURL");
        if (responseURL != null && !"".equals(responseURL)) {
            // put all the XML data in the session 
            // and then go to the next block...... 
            // Create a DOM node and load the XML  
            // from the session object.    		  
            DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = null;
            try {
                builder = factory.newDocumentBuilder();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
                throw new ServletException(e);
            }
            String userdata = request.getParameter("getuserdata");
            StringReader userdataReader = new StringReader(userdata);
            InputSource inputSource = new InputSource(userdataReader);
            Document doc = null;
            try {
                doc = builder.parse(inputSource);
            } catch (SAXException e) {
                e.printStackTrace();
                throw new ServletException(e);
            } catch (IOException e) {
                e.printStackTrace();
                throw new ServletException(e);
            }

            // Now that we have loaded XML, let us 
            // get each key and save it in session 
            
            
            Element recordSet = doc.getDocumentElement();
             if (recordSet != null) 
             {
               NodeList records = recordSet.getChildNodes();
                for (int i = 0; i < records.getLength(); i++) 
                {
                    Node record = records.item(i);
                    if (!(record instanceof Element)) 
                    {
                        continue;
                    }
                    NamedNodeMap keyAttributes = record.getAttributes();
                    String name;
                    String value;
                    name = keyAttributes.getNamedItem("name").getNodeValue();
                    value = keyAttributes.getNamedItem("value").getNodeValue();
                    
                    value = getXMLEncodedString(value);
                    
                    setPageItem(pageContext, name, value);
                }
            }
            
        %>

        <form>
        <block>
        <goto next='<%= responseURL %>' />
        </block>
        </form>

        <%

        } else {
            // Body Section 
		%>
<form id="TransferUserData">
   <object name="getuserdata" classid="CRData:get">
      <param name="EWT" value=""/>
      <param name="FORCED_DISC" value=""/>
      <param name="IVR_CALL_TYPE" value=""/>
      <param name="ScriptID" value=""/>
      <filled>
         <submit method="post" namelist="getuserdata" next="TransferUserData.jsp?udGetResponseURL=TransferProcess.jsp"/>
      </filled>
   </object>
</form>
		<%
        }

        processAnywhere(pageContext);
        //strFooterVXML += "</vxml>\n";
        // Footer Section - End 
        %>
        </vxml>
        <%

       // out.print(strVXML + strFooterVXML);
        processEnd(pageContext);
%>
<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
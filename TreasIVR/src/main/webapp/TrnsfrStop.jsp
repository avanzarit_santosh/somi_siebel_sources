<?xml version='1.0' encoding='utf-8'?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/reportingsupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    session.setAttribute("PAGEID", "TrnsfrStop");

    StringBuffer headerVxml = new StringBuffer();

    headerVxml.append(getVxmlHeader(pageContext));
    
    String action = lookUp(pageContext, "ACTION");
    
    if (!"HUP".equals(action))
    {
    	setInSession(pageContext, "ReportItem_IVRHangup", "true"); 

	StringBuffer vxmlCode = new StringBuffer();

	vxmlCode.append(headerVxml);
	
	vxmlCode.append("<form><block><disconnect/></block></form>");
	
	vxmlCode.append(getVxmlFooter());

	out.print(vxmlCode);
	
	return;
    }

    
        out.print(headerVxml);
        headerVxml.setLength(0);
%><form>
   <var name="GS_callrecording_filename"/><%
        out.print("<var name=\"GS_callerleg_callrecording_filename\" expr=\"'" + lookUp(pageContext, "GS_callerleg_callrecording_filename") + "'\" />");
        out.print("<var name=\"GS_agentleg_callrecording_filename\" expr=\"'" + lookUp(pageContext, "GS_agentleg_callrecording_filename") + "'\" />");
%><block>
      <script>
			if (session.genesys.agent_leg_flag == 'true')
				GS_callrecording_filename = 'GS_agentleg_callrecording_filename';
			else
				GS_callrecording_filename = 'GS_callerleg_callrecording_filename';
			</script>
   </block>
   <object name="StopTrxnRecording" classid="transactionalrecord:stop" cond="(GS_callerleg_callrecording_filename != '')||(GS_agentleg_callrecording_filename != '')">
      <param name="namelist" expr="GS_callrecording_filename"/>
   </object>
   <block>
      <disconnect/>
   </block>
</form>
<catch event="error.semantic.transrec.oneallowed">
   <disconnect/>
</catch>
<catch event="error.semantic.transrec.notstarted">
   <disconnect/>
</catch>
<catch event="error.unsupported.object.transrec">
   <disconnect/>
</catch>
<catch event="error.semantic.transrec.notenabled">
   <disconnect/>
</catch>

<%
    processAnywhere(pageContext);

    out.print(getVxmlFooter());

    processEnd(pageContext);
   
    // Unwind report stack which will send subcallflow end events if not already sent
    // Then send call end and abandon the session.
	
    String agentLegFlag = lookUp(pageContext, "agent-leg-flag");
    
    if (("HUP".equals(action)) && (!"true".equals(agentLegFlag)))
    {
	setInSession(pageContext, "ReportItem_last_scf", "1");
    	unwindReportItemStack(pageContext);
	sendCallEndReportData(pageContext);
        
        String agentRecording = lookUp(pageContext, "GS_agentleg_callrecording_filename");
        
        if ("".equals(agentRecording))
        {
        	pageContext.getSession().invalidate();
        }
    }

%>
    <%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
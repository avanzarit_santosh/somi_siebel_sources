<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"TrnsfrSetSsnValid.jsp","TrnsfrSSNInput.jsp","TrnsfrSSNInput.jsp","TrnsfrSSNInput.jsp","TrnsfrSSNInput.jsp","TrnsfrSetSsnValid.jsp","TrnsfrHolidayMsg.jsp","TrnsfrAfterHrMsg.jsp","TrnsfrCallCntrClosedMsg.jsp","TrnsfrCallerWithEWT.jsp","Script_Result1.jsp","TrnsfrSetSsnValid.jsp","TrnsfrSpEo.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.	  
	//                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext) {

	}

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.				
	//See the help file for more details		
	public void processAnywhere(PageContext localPageContext) {

		final int GET_EWT_FROM_IRD = 1;
        final int SPANISH_CALLER = 2;
        final int EXECUTIVE_CALLER = 3;
        
        final int TAX_PREP = 4;
        final int TAX_ASSESSMENT = 5;
        final int TAX_CORR = 6;
        final int EST_PAYMENT = 6;
        
        final int HOLIDAY = 7;
        final int AFTER_HOUR = 8;
        final int CALL_CENTER_CLOSED_FLAG = 9;

        final int TRANSFER_CALLER = 10;
        final int TRANSFER_EO_OR_SPANISH = 13;
 
        final int FORCE_DISC_Q_FULL = 11;
        final int FORCE_DISC_CREDIT = 11;
        final int FORCE_DISC_DEFAULT = 11;
        
        final int CRM_DOWN =  12;
        final int ARABIC_CALLER = 2;


		int resultPort = GET_EWT_FROM_IRD;
		String languageCode = lookUp(localPageContext, "APP_LANGUAGE");

		boolean spanishCaller = "es-MX".equalsIgnoreCase(languageCode);
		boolean arabicCaller = "ar".equalsIgnoreCase(languageCode);
		boolean isExecCaller = "EO_CALLER".equalsIgnoreCase(lookUp(localPageContext, "IVR_CALL_TYPE"));
		boolean isHoliday = "YES".equalsIgnoreCase(lookUp(localPageContext, "HOLIDAY"));
		boolean isOfficeClosed = "YES".equalsIgnoreCase(lookUp(localPageContext, "OFFICE_CLOSED"));
		boolean isForceClosed = "Y".equalsIgnoreCase(lookUp(localPageContext, "ForceCloseFlag"));

		boolean emergencyFlag = "Y".equalsIgnoreCase(lookUp(localPageContext, "EmergencyFlag"));
        boolean specialFlag = "Y".equalsIgnoreCase(lookUp(localPageContext, "SpecialFlag"));
        boolean forceCloseFlag = "Y".equalsIgnoreCase(lookUp(localPageContext, "ForceCloseFlag"));

        String callPath = lookUp(localPageContext, "CallPath");

		String ivrCallType = lookUp(localPageContext, "IVR_CALL_TYPE");
		if(ivrCallType == null || "".equals(ivrCallType)) {
			ivrCallType = lookUp(localPageContext, "IvrCallType");
			setPageItem(localPageContext, "IVR_CALL_TYPE", ivrCallType);
		}

        boolean crmDown = "CRM_DOWN".equalsIgnoreCase(ivrCallType);


		String forcedDisc = lookUp(localPageContext , "FORCED_DISC");
		String ewt = lookUp(localPageContext, "EWT");
		String scriptId = lookUp(localPageContext, "ScriptID");
	    String ssn = lookUp(localPageContext, "CallerSsn");
	    String sessionId = lookUp(localPageContext, "SessionID");
	    setPageItem(localPageContext, "SSN", ssn);
	    String routePoint = lookUp(localPageContext, "RoutePoint");
	    setPageItem(localPageContext, "ROUTE_POINT", routePoint);

        // get credit caller status
		String creditCaller = lookUp(localPageContext, "CreditCaller");
        setPageItem(localPageContext, "CREDIT_CALLER", creditCaller);
        boolean isCreditCaller = "YES".equalsIgnoreCase(creditCaller);

        if("1200".equals(scriptId)) {
        	resultPort = TRANSFER_EO_OR_SPANISH;
        }
        else if(crmDown) {
            resultPort = CRM_DOWN;
        }
        else if("Q_CAP_FULL".equalsIgnoreCase(forcedDisc)) {
            resultPort = FORCE_DISC_Q_FULL;
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.QUEUE_FORCE_DISCONNECT);
        }
        else if("CREDIT".equalsIgnoreCase(forcedDisc)) {
            resultPort = FORCE_DISC_CREDIT;
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.QUEUE_CREDIT_DISCONNECT);
        }
        else if("DEFAULT".equalsIgnoreCase(forcedDisc)) {
            resultPort = FORCE_DISC_DEFAULT;
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.QUEUE_DEFAULT_DISCONNECT);
        }
        else if("1100".equals(scriptId)) {
            resultPort = TRANSFER_CALLER;
        }
        else if(isHoliday) {
			resultPort = HOLIDAY;
		}
		else if(isForceClosed) {
			resultPort = CALL_CENTER_CLOSED_FLAG;
		}
		else if(isOfficeClosed) {
			resultPort = AFTER_HOUR;
		}
		else if("TAX_PREP".equalsIgnoreCase(ivrCallType)) {
			resultPort = TAX_PREP;
		}
        else if("TAX_ASSESSMENT".equalsIgnoreCase(ivrCallType)) {
        	resultPort = TAX_ASSESSMENT;
        }
        else if("TAX_CORR".equalsIgnoreCase(ivrCallType)) {
        	resultPort = TAX_CORR;
        }
        else if("EST_PAYMENT".equalsIgnoreCase(ivrCallType)) {
        	resultPort = EST_PAYMENT;
        }
		else if(spanishCaller) {
            setPageItem(localPageContext, "IVR_CALL_TYPE", "SPANISH");
            setPageItem(localPageContext, "NoEWT", "true");
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.QUEUE_SPANISH);
			resultPort = SPANISH_CALLER;
		}
        else if(arabicCaller) {
            setPageItem(localPageContext, "IVR_CALL_TYPE", "ARABIC");
            setPageItem(localPageContext, "NoEWT", "true");
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.QUEUE_ARABIC);
            resultPort = ARABIC_CALLER;
        }
		else if(isExecCaller) {
 			resultPort = EXECUTIVE_CALLER;
            setPageItem(localPageContext, "NoEWT", "true");
            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.QUEUE_EXECUTIVE);
		}
		else {			
	        if(isCreditCaller) {
                setPageItem(localPageContext, "CREDIT_CALLER", "YES");
                callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.QUEUE_CREDIT);
	        }
	        else {
	        	setPageItem(localPageContext, "CREDIT_CALLER", "NO");
	            callPath = IvrUtils.appendCallPath(callPath, CallPathConstants.QUEUE_DEFAULT);
	        }
	        
	        if("PENDING_REVIEW".equalsIgnoreCase(ivrCallType)) {	        	
	        }
	        else if("NO_RECORD".equalsIgnoreCase(ivrCallType)) {
	        }
	        else {
	        	setPageItem(localPageContext, "IVR_CALL_TYPE", "DEFAULT");
	        }
	        resultPort = GET_EWT_FROM_IRD;
		}
	    

        //System.out.println("== TransferProcess Start");
        //System.out.println("session id: " + sessionId);
        //System.out.println("ScriptId: " + scriptId);
        //System.out.println("transfer ivr call type: " + ivrCallType);
	    //System.out.println("EWT: " + ewt);
	    //System.out.println("SSN: " + ssn);
	    //System.out.println("ScriptID: " + lookUp(localPageContext, "ScriptID"));
	    //System.out.println("Call Path: " + callPath);
        //System.out.println("Forced disc: " + forcedDisc);
        //System.out.println("Credit caller: " + creditCaller);
        //System.out.println("== TransferProcess End");
	    
	    setPageItem(localPageContext, "CallPath", callPath);
        setPageItem(localPageContext,"ReportItem_Application Result", "SUCCESS");  
        setPageItem(localPageContext,"ReportItem_Application Result Reason", callPath);
        
        localPageContext.setAttribute("nAllowType", new Integer(resultPort), PageContext.PAGE_SCOPE);

		
	}

	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext) {

	}%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.IvrConstants.*"%>
<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"EstRpt6Less.jsp","EstRpt6Grtr.jsp","EstPaymentAuthFailMenu.jsp","EstPymntSystemDown.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.	  
	//                    Can be used o Access the HTTP Objects 
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext){

	}

    Logger logger = Logger.getLogger(getClass());

	//Can be used for writing the custom code that needs to get
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.				
	//See the help file for more details		
	public void processAnywhere(PageContext localPageContext){
		final int SIX_PAYMENT_OR_LESS = 1;
		final int MORE_THAN_SIX_PAYMENTS = 2;
		final int NO_PAYMENT = 3;
		final int SYSTEM_DOWN = 4;
		
		int resultPort = 1;
		String voxFileDir = lookUp(localPageContext, "VOXFILEDIR");
		String taxYear = lookUp(localPageContext, "TaxYear");
		String accountNum = lookUp(localPageContext, "SSNInput");
		setPageItem(localPageContext, "ContactReason", "Estimate Payments");
        setPageItem(localPageContext, "IvrCallType", "");
		String addStuff = "";

        try {
            // get endpoint from config file
            String endpoint = AppContext.INSTANCE.getProperty("siebel.webservice.endpoint");
        
	        // query siebel for account assets
	        logger.debug("sending asset request to Siebel web service");
	        ReturnAssetProxy proxy = new ReturnAssetProxy(endpoint);
	        MIIvrAssetManagementQueryByExampleInput input = proxy.createRequest();
	        input.getListOfMiIvrAssetManagement().getAssetMgmtAsset().get(0).setAccountName(accountNum);
	        
	        input.getListOfMiIvrAssetManagement().getAssetMgmtAsset().get(0).setComments(taxYear.substring(3));
	        MIIvrAssetManagementQueryByExampleOutput output = proxy.sendRequest(input);
	        logger.debug("successfully received response");



	        logger.debug("retrieving asset for tax year " + taxYear);
            AssetMgmtAsset assetForYear = null;
            for(AssetMgmtAsset asset : output.getListOfMiIvrAssetManagement().getAssetMgmtAsset()) {
                if(taxYear.equals(asset.getTaxYear())) {
                    assetForYear = asset;
	                //logger.debug("retrieved asset for tax year " + taxYear + AppContext.INSTANCE.getGson().toJson(assetForYear) );
                    break;
                }
            }

            List<TransactionAssetMgmtAsset> paymentList = new ArrayList<TransactionAssetMgmtAsset>();
            addStuff = IvrUtils.getVXMLScript();

            if(assetForYear != null) {
                for(TransactionAssetMgmtAsset transaction : assetForYear.getListOfTransactionAssetMgmtAsset().getTransactionAssetMgmtAsset()) {

                    if("Estimate Payment".equals(transaction.getName()) || "Extension".equals(transaction.getName())) {
                        paymentList.add(transaction);
                    }
                    else if("Credit Forward".equals(transaction.getName())) {
                        float val = Float.parseFloat(transaction.getAssetValue2());
                        if(val > 0.001f) {
                            paymentList.add(transaction);
                        }
                    }
                }
            }

            setPageItem(localPageContext, "IvrCallType", "EST_PAYMENT");
            setPageItem(localPageContext, "PaymentCount", String.valueOf(paymentList.size()));

            if(paymentList.size() > 6) {
                addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/503_EstPymt6.vox");
                resultPort = MORE_THAN_SIX_PAYMENTS;
                setPageItem(localPageContext, "ContactResolution", "Referred to CSR");
            }
            else if(paymentList.size() > 0) {
                Collections.sort(paymentList, new Comparator<TransactionAssetMgmtAsset>() {
                    @Override
                    public int compare(TransactionAssetMgmtAsset t1, TransactionAssetMgmtAsset t2) {
                        Date d1 = IvrUtils.parseDate(t1.getInstallDate());
                        Date d2 = IvrUtils.parseDate(t2.getInstallDate());
                        if(d1 == null)
                            return -1;
                        else if(d2 == null)
                            return 1;
                        else
                            return d1.compareTo(d2);
                    }
                });

                for(TransactionAssetMgmtAsset transaction : paymentList) {
                	if("Void".equals(transaction.getStatus())) {
                        continue;
                    }
                    if("Estimate Payment".equals(transaction.getName()) || "Extension".equals(transaction.getName())) {
                        addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/505_EstPymt_Date_Amt01.vox");
                        addStuff += IvrUtils.getVXMLCurrency(transaction.getAssetValue2());
                        addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/505_EstPymt_Date_Amt02.vox");
                        addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(transaction.getInstallDate()));
                    }
                    else if("Credit Forward".equals(transaction.getName())) {
                        addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/506_Credit_Forward01.vox");
                        addStuff += IvrUtils.getVXMLCurrency(transaction.getAssetValue2());
                        addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/506_Credit_Forward02.vox");
                        addStuff += IvrUtils.getVXMLDate(IvrUtils.parseDate(transaction.getInstallDate()));
                    }
                }

                setPageItem(localPageContext, "ContactResolution", "Payment Found");
                resultPort = SIX_PAYMENT_OR_LESS;
            }
            else {
                //play no payment
                addStuff += IvrUtils.getVXMLPrompt(voxFileDir + "/502_Est_NoPymts.vox");
                setPageItem(localPageContext, "ContactResolution", "No Payments Found");
                setPageItem(localPageContext, "IvrCallType", "EST_PAYMENT");
                resultPort = NO_PAYMENT;
            }

        } catch(Exception e) {
            e.printStackTrace();
            resultPort = SYSTEM_DOWN;
            logger.error("Error retrieving estimate payments.", e);
        }
        
		localPageContext.setAttribute("addStuff", addStuff, PageContext.PAGE_SCOPE);
        localPageContext.setAttribute("nAllowType", new Integer(resultPort), PageContext.PAGE_SCOPE);
	}

	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext){
	}

	
	%>
<%@ page import="java.io.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.text.*"%>
<%@ page import="org.xml.sax.*"%>
<%@ page import="org.w3c.dom.*"%>
<%@ page import="javax.xml.xpath.*"%>
<%@ page import="org.apache.log4j.Logger"%>
<%@ page import="com.google.gson.Gson"%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.ws.asset.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.*"%>
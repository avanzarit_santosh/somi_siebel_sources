<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*"%>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/reportingsupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();
    String label = "";
    StringBuffer data = new StringBuffer();

    session.setAttribute("PAGEID", "TransferReturn");

    headerVxml.append(getVxmlHeader(pageContext));
    
    if (("HUP".equals(lookUp(pageContext, "ACTION"))) && (!"true".equals(lookUp(pageContext, "agent-leg-flag"))))
    {
    	headerVxml.append("<form><block><disconnect/></block></form></vxml>");
        out.print(headerVxml);
        headerVxml.setLength(0);    	
	setInSession(pageContext, "ReportItem_last_scf", "1");
    	unwindReportItemStack(pageContext);
	sendCallEndReportData(pageContext);
        pageContext.getSession().invalidate();
	return;
    }

    
        out.print(headerVxml);
        headerVxml.setLength(0);
%><form><var name="CALL_PATH" expr="'<%=lookUp(pageContext,"CALL_PATH")%>'"/><var name="IVR_CALL_TYPE" expr="'<%=lookUp(pageContext,"IVR_CALL_TYPE")%>'"/><var name="ReturnResult" expr="'<%=lookUp(pageContext,"ReturnResult")%>'"/><var name="SSN" expr="'<%=lookUp(pageContext,"SSN")%>'"/><block>
      <return namelist="CALL_PATH IVR_CALL_TYPE ReturnResult SSN "/>
   </block>
</form>
<%
    processAnywhere(pageContext);

    out.print(getVxmlFooter());

    processEnd(pageContext);

    sendSubcallflowEndEvent(pageContext);
    removeReportItemNode(pageContext);    	
%>
<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
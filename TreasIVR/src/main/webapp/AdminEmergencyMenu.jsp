<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*"%>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();
    String label = "";
    String subDlgReturn = "";
    String query="";
    StringBuffer data = new StringBuffer();

    session.setAttribute("PAGEID" ,"AdminEmergencyMenu");

    headerVxml.append(getASRLangVxmlHeader(pageContext));

    
        out.print(headerVxml);
        headerVxml.setLength(0);
%><form>
   <block name="Flush">
      <prompt bargein="false">
         <audio src="StudioIncludes/silence10ms.wav"/>
      </prompt>
   </block><block name="AdminEmergencyMenuP0" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("802_Emerg_Main.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><var name="retryCount" expr="1"/><field  name="AdminEmergencyMenu" ><prompt bargein="true" timeout="3s"/>
   <option dtmf="1"/>
   <option dtmf="2"/>
   <option dtmf="3"/>
   <option dtmf="4"/>
   <option dtmf="9"/></field><filled mode="all" namelist="AdminEmergencyMenu">
      <if cond="AdminEmergencyMenu == 1">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AdminPlayEmMsg.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='AdminEmergencyMenu AdminEmergencyMenu$.inputmode AdminEmergencyMenu$.confidence AdminEmergencyMenu$.utterance AdminEmergencyMenu$.markname AdminEmergencyMenu$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="AdminEmergencyMenu == 2">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AdminEmMsgRecord.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='AdminEmergencyMenu AdminEmergencyMenu$.inputmode AdminEmergencyMenu$.confidence AdminEmergencyMenu$.utterance AdminEmergencyMenu$.markname AdminEmergencyMenu$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="AdminEmergencyMenu == 3">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AdminSetEmFlg.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='AdminEmergencyMenu AdminEmergencyMenu$.inputmode AdminEmergencyMenu$.confidence AdminEmergencyMenu$.utterance AdminEmergencyMenu$.markname AdminEmergencyMenu$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="AdminEmergencyMenu == 4">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AdminSetEmFlg.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='AdminEmergencyMenu AdminEmergencyMenu$.inputmode AdminEmergencyMenu$.confidence AdminEmergencyMenu$.utterance AdminEmergencyMenu$.markname AdminEmergencyMenu$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="AdminEmergencyMenu == 9">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AdminMenu.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='AdminEmergencyMenu AdminEmergencyMenu$.inputmode AdminEmergencyMenu$.confidence AdminEmergencyMenu$.utterance AdminEmergencyMenu$.markname AdminEmergencyMenu$.marktime '" );

out.print( "/>" );
%>
</if>
   </filled>
   <catch event="noinput" cond="retryCount == 1">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("NoInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="AdminEmergencyMenuP0 " /></catch>
   <catch event="nomatch" cond="retryCount == 1">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("InvalidInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="AdminEmergencyMenuP0 " /></catch>
   <catch event="noinput" cond="retryCount == 2">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("NoInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="AdminEmergencyMenuP0 " /></catch>
   <catch event="nomatch" cond="retryCount == 2">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("InvalidInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="AdminEmergencyMenuP0 " /></catch>
   <catch event="nomatch" cond="retryCount == 3">
      <throw event="com.genesys.studio.toomanynomatches"/>
   </catch>
   <catch event="noinput" cond="retryCount == 3">
      <throw event="com.genesys.studio.toomanynoinputs"/>
   </catch>
</form>
<catch event="com.genesys.studio.toomanynoinputs">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("AdminGoodBye.jsp"));

out.print( "'/>" );
%>
</catch>
<catch event="com.genesys.studio.toomanynomatches">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("AdminGoodBye.jsp"));

out.print( "'/>" );
%>
</catch>

<%
    processAnywhere(pageContext);

    out.print(getVxmlFooter());
    
    processEnd(pageContext);

%>
<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
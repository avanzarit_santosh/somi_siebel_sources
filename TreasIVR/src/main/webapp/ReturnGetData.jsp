<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"ReturnListMenu.jsp","NoReturnMenu.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.	  
	//                    Can be used o Access the HTTP Objects 
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext){

	}

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.				
	//See the help file for more details		
	public void processAnywhere(PageContext localPageContext){
		final int HAS_RETURN = 1;
		final int NO_RETURN = 2;
		
		int resultPort = NO_RETURN;

		// Set return list menu
		//get document from session
		String responseXml = lookUp(localPageContext, "ResponseDataXml");
		DOMObject res = null;
		ReturnAsset returnAsset = null;
		
		try {
			res = new DOMObject(new InputSource(new StringReader(responseXml)));
			returnAsset = new ReturnAsset(res);


			String taxYear = lookUp(localPageContext, "TaxYear");
			String assetPath = "//ListOfMiIvrAssetManagement/AssetMgmt-Asset" + "[TaxYear='" + taxYear + "']";
			Node assetNode = res.getXpathNode(assetPath);
	
			setPageItem(localPageContext, "MenuItem1040", "Silence.vox");
			setPageItem(localPageContext, "MenuItem1040CR", "Silence.vox");
			setPageItem(localPageContext, "MenuItem1040CR7", "Silence.vox");
			setPageItem(localPageContext, "MenuItem1040X", "Silence.vox");
	
			if(assetNode != null) {
				Node form1040 = returnAsset.getRecentForm(taxYear, ReturnAsset.FORM_1040); 	
				String hhi = IvrUtils.getXpathValue(form1040, "GrossReceiptsReduction");
				setLog(localPageContext, 0, "form 1040: " + form1040);
				if(form1040 != null) {
					setPageItem(localPageContext, "MenuItem1040", "215_TaxRtnInfo_1040.vox");
					resultPort = HAS_RETURN;
				}
				

				Node form1040CR = IvrUtils.getXpathNode(assetNode, "ListOfFormAssetMgmt-Asset/FormAssetMgmt-Asset" 
						+ "[Name='MI-1040CR' or Name='MI-1040CR2']"
						+ "[GrossReceiptsReduction!='" + hhi + "']"
                        + "[Status!='Not Ret of Rec']"
						);
				setLog(localPageContext, 0, "form 1040CR: " + form1040CR);
				if(form1040CR != null) {
					setPageItem(localPageContext, "MenuItem1040CR", "216_TaxRtnInfo_CR.vox");
	                resultPort = HAS_RETURN;
				}
	
				Node form1040CR7 = returnAsset.getRecentForm(taxYear, ReturnAsset.FORM_1040CR7);
				setLog(localPageContext, 0, "form 1040CR7: " + form1040CR7);
				if(form1040CR7 != null) {
					setPageItem(localPageContext, "MenuItem1040CR7", "217_TaxRtnInfo_CR7.vox");
	                resultPort = HAS_RETURN;
				}
	
				Node form1040X = returnAsset.getRecentForm(taxYear, ReturnAsset.FORM_1040X);
				setLog(localPageContext, 0, "form 1040X: " + form1040X);
				if(form1040X != null) {
					setPageItem(localPageContext, "MenuItem1040X", "218_TaxRtnInfo_1040X.vox");
	                resultPort = HAS_RETURN;
				}
			}
        } catch(Exception e) {
        	setLog(localPageContext, 3, "Error trying to retrieve return asset from CRM");
	        resultPort = NO_RETURN;
        }


        localPageContext.setAttribute("nAllowType", new Integer(resultPort), PageContext.PAGE_SCOPE);

	}

	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext){

	}%>
<%@ page import="java.io.*"%>
<%@ page import="org.xml.sax.*"%>
<%@ page import="org.w3c.dom.*"%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
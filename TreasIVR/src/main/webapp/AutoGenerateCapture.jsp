<?xml version="1.0" encoding="utf-8"?>
<%@ page language="java" import="java.util.*, java.io.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/reportingsupport.inc" %>
<%@ include file="StudioIncludes/recordingsupport.inc" %>
<%
    StringBuffer vxmlCode = new StringBuffer();
    String voxFileName="";

    try {
        request.setCharacterEncoding("UTF-8");
    } catch (java.io.UnsupportedEncodingException e) {
        e.printStackTrace();
    }
    if (request.getMethod().equalsIgnoreCase("HEAD")) {
        return;
    }


    session.setAttribute("PAGEID", "AutoCapture");

    java.util.Enumeration params = request.getParameterNames();
    String name;
    String value;
    String strNextBlockName="";
    String strNewGCTVarName="";
    String strGVPfilename="";
    String strRecordDuration="";
    String strBlockName="";
    String strRecordTermchar ="";
    String strRecordSize ="";
    String strRecordMaxtime ="";
    while (params.hasMoreElements()) 
    {
        name = (String) params.nextElement();
        value = request.getParameter(name);
        session.setAttribute(name, value);
    }

    if ("HUP".equals(lookUp(pageContext, "ACTION")))
    {
    	unwindReportItemStack(pageContext);
        sendCallEndReportData(pageContext);
    }

    if (request.getMethod().equalsIgnoreCase("POST")) {
        voxFileName = serializeVoxFile(pageContext);
        
        if (voxFileName.indexOf ("_GVPFileName##") != -1)
        {
        strNextBlockName = voxFileName.substring(0,voxFileName.indexOf ("++"));
        strNewGCTVarName = voxFileName.substring((voxFileName.indexOf ("++")+2),voxFileName.indexOf ("##"));
        strRecordDuration = voxFileName.substring((voxFileName.indexOf ("##")+2), voxFileName.indexOf ("**"));
        strRecordSize = voxFileName.substring((voxFileName.indexOf ("**")+2), voxFileName.indexOf ("&&"));
        strRecordTermchar = voxFileName.substring((voxFileName.indexOf ("&&")+2), voxFileName.indexOf ("$$"));
        strRecordMaxtime = voxFileName.substring((voxFileName.indexOf ("$$")+2), voxFileName.indexOf ("--"));
        strGVPfilename = voxFileName.substring((voxFileName.indexOf ("--")+2), voxFileName.length());
        strBlockName = strNewGCTVarName.substring(0,strNewGCTVarName.indexOf ("_GVPFileName"));
        session.setAttribute(strNewGCTVarName, strGVPfilename);
        session.setAttribute(strBlockName+"$.duration",strRecordDuration);
        session.setAttribute(strBlockName+"$.size",strRecordSize);
        session.setAttribute(strBlockName+"$.termchar",strRecordTermchar);
        session.setAttribute(strBlockName+"$.maxtime",strRecordMaxtime);
        
       	}
    }

    vxmlCode.append(getVxmlHeader(pageContext));

    vxmlCode.append("<form><block>");
     if (voxFileName.indexOf ("_GVPFileName##") != -1)
    {
    vxmlCode.append("<goto next='");
    vxmlCode.append(strNextBlockName);
    vxmlCode.append("'/>");
    }
    vxmlCode.append("</block></form></vxml>");

    out.print(vxmlCode);
 
%>
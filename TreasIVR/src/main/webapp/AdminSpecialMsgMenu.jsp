<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*"%>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    StringBuffer headerVxml = new StringBuffer();
    String label = "";
    String subDlgReturn = "";
    String query="";
    StringBuffer data = new StringBuffer();

    session.setAttribute("PAGEID" ,"AdminSpecialMsgMenu");

    headerVxml.append(getASRLangVxmlHeader(pageContext));

    
        out.print(headerVxml);
        headerVxml.setLength(0);
%><form>
   <block name="Flush">
      <prompt bargein="false">
         <audio src="StudioIncludes/silence10ms.wav"/>
      </prompt>
   </block><block name="AdminSpecialMsgMenuP0" >
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append((String)session.getAttribute("menuVox"));
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt></block><var name="retryCount" expr="1"/><field  name="AdminSpecialMsgMenu" ><prompt bargein="true" timeout="3s"/>
   <option dtmf="1"/>
   <option dtmf="2"/>
   <option dtmf="3"/>
   <option dtmf="4"/>
   <option dtmf="9"/></field><filled mode="all" namelist="AdminSpecialMsgMenu">
      <if cond="AdminSpecialMsgMenu == 1">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AdminPlaylMsg.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='AdminSpecialMsgMenu AdminSpecialMsgMenu$.inputmode AdminSpecialMsgMenu$.confidence AdminSpecialMsgMenu$.utterance AdminSpecialMsgMenu$.markname AdminSpecialMsgMenu$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="AdminSpecialMsgMenu == 2">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AdminMsgRecord.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='AdminSpecialMsgMenu AdminSpecialMsgMenu$.inputmode AdminSpecialMsgMenu$.confidence AdminSpecialMsgMenu$.utterance AdminSpecialMsgMenu$.markname AdminSpecialMsgMenu$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="AdminSpecialMsgMenu == 3">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AdminSetFlag.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='AdminSpecialMsgMenu AdminSpecialMsgMenu$.inputmode AdminSpecialMsgMenu$.confidence AdminSpecialMsgMenu$.utterance AdminSpecialMsgMenu$.markname AdminSpecialMsgMenu$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="AdminSpecialMsgMenu == 4">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AdminSetFlag.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='AdminSpecialMsgMenu AdminSpecialMsgMenu$.inputmode AdminSpecialMsgMenu$.confidence AdminSpecialMsgMenu$.utterance AdminSpecialMsgMenu$.markname AdminSpecialMsgMenu$.marktime '" );

out.print( "/>" );
%>
</if>
      <if cond="AdminSpecialMsgMenu == 9">
<%
out.print(  "<submit next='" );

out.print(encodeUtf8Path("AdminMenu.jsp"));

out.print( "'" );

out.print( " method='post'" );

out.print( " namelist='AdminSpecialMsgMenu AdminSpecialMsgMenu$.inputmode AdminSpecialMsgMenu$.confidence AdminSpecialMsgMenu$.utterance AdminSpecialMsgMenu$.markname AdminSpecialMsgMenu$.marktime '" );

out.print( "/>" );
%>
</if>
   </filled>
   <catch event="noinput" cond="retryCount == 1">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("NoInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="AdminSpecialMsgMenuP0 " /></catch>
   <catch event="nomatch" cond="retryCount == 1">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("InvalidInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="AdminSpecialMsgMenuP0 " /></catch>
   <catch event="noinput" cond="retryCount == 2">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("NoInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="AdminSpecialMsgMenuP0 " /></catch>
   <catch event="nomatch" cond="retryCount == 2">
      <assign name="retryCount" expr="retryCount + 1"/>
<%
	if(session.getAttribute("UseDynamicLanguage")!= null && !session.getAttribute("UseDynamicLanguage").equals("1"))
	{
%>
<prompt bargein="true" xml:lang="en-US"><% }
else
 {%><prompt bargein="true" xml:lang="<%
            data.append(lookUp(pageContext,"APP_LANGUAGE"));
            out.print(data);
            data.setLength(0);
        %>">
<%
	}
%>
<%
        data.append(lookUp(pageContext, "VOXFILEDIR"));
        data.append("/");
        data.append("InvalidInput.vox");
        
        out.print("<audio src='");
        out.print(data);
        data.setLength(0);
        out.print("'>");
        
        out.print("</audio>");
        %></prompt><clear namelist="AdminSpecialMsgMenuP0 " /></catch>
   <catch event="nomatch" cond="retryCount == 3">
      <throw event="com.genesys.studio.toomanynomatches"/>
   </catch>
   <catch event="noinput" cond="retryCount == 3">
      <throw event="com.genesys.studio.toomanynoinputs"/>
   </catch>
</form>
<catch event="com.genesys.studio.toomanynoinputs">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("AdminGoodBye.jsp"));

out.print( "'/>" );
%>
</catch>
<catch event="com.genesys.studio.toomanynomatches">
<%
out.print( "<goto next='" );

out.print(encodeUtf8Path("AdminGoodBye.jsp"));

out.print( "'/>" );
%>
</catch>

<%
    processAnywhere(pageContext);

    out.print(getVxmlFooter());
    
    processEnd(pageContext);

%>
<%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
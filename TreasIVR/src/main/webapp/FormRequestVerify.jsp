<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" import="java.util.*" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%@ include file="StudioIncludes/CommonInitCode.inc" %>
<%
    String addStuff="";
    String root="";
    String allowType="";
    String GenerateMode="VXML";
    StringBuffer codePage = new StringBuffer();
    int nAllowType=0;

    /******************************
    Here we set the allowed
    variables into the page-scope
    *******************************/

    pageContext.setAttribute("nAllowType",new Integer(nAllowType),PageContext.PAGE_SCOPE);
    pageContext.setAttribute("allowType",allowType,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("addStuff",addStuff,PageContext.PAGE_SCOPE);
    pageContext.setAttribute("GenerateMode",GenerateMode,PageContext.PAGE_SCOPE);
   
	String xHRefs[] = {"FormChoicesMenu.jsp","FormWaitMsg.jsp","FormVisitWebMsg.jsp","FormsSystemDown.jsp"};
	

    processAnywhere(pageContext);

    /******************************
    Here we get the allowed
    variables from the page-scope
    *******************************/

    Integer nTempInt= (Integer)pageContext.getAttribute ("nAllowType",PageContext.PAGE_SCOPE);
    GenerateMode = (String)pageContext.getAttribute ("GenerateMode",PageContext.PAGE_SCOPE);

    nAllowType = nTempInt.intValue();
    allowType = (String)pageContext.getAttribute ("allowType",PageContext.PAGE_SCOPE);

    if (nAllowType != 0) {
        nAllowType = nAllowType - 1;
    }
    if (allowType.equals("") &&  xHRefs.length > 0) {
        allowType = xHRefs[nAllowType];
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
     	codePage.append(getStartXmlPageTag(pageContext, allowType));
    }
    else {
	codePage.append(getVxmlHeader(pageContext));
    	codePage.append("<form>");
    }
    
    /***********************
    This would allow users
    to insert in their own
    code in the form.
    ***********************/
    
    processEnd(pageContext);
    
    addStuff = (String)pageContext.getAttribute ("addStuff",PageContext.PAGE_SCOPE);

    
    if (addStuff != null && !"".equals(addStuff)) {
        codePage.append(addStuff);
    }

    if ("TXML".equals(GenerateMode.toUpperCase())){
    	codePage.append("</XMLPage>");
    }
    else
    {
 	codePage.append("<block><submit next='");
	codePage.append(allowType);
    	codePage.append("' /></block></form></vxml>");
    }

    out.print(codePage);
%>
<%!// localPageContext - Contains the reference to the Page Context.	  
	//                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
	public void processBegin(PageContext localPageContext) {

	}

    Logger logger = Logger.getLogger(getClass());

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends.
	//By default the submit location is the first out port.
	//Use "nAllowType" variable for setting a different submit(out port) location.				
	//See the help file for more details		
	public void processAnywhere(PageContext localPageContext) {

		final int NOT_ON_FILE = 3;
		final int CALLED_WITHIN_X_DAYS = 2;
		final int REQUEST_VALID = 1;
		final int EXCEPTION = 4;
		
		int resultPort = 1;
		Connection con = null;
        String locator = "";
		String ssn = lookUp(localPageContext, "FormsSSNInput");

		setPageItem(localPageContext, "CallerSsn", ssn);
		
        try {
            // connect to DB
            locator = "connecting to Forms2Me database.";
            con = DriverManager.getConnection(AppContext.INSTANCE.getProperty("forms2me.db.url")
                     , AppContext.INSTANCE.getProperty("forms2me.db.user")
                     , AppContext.INSTANCE.getProperty("forms2me.db.pass"));

            locator = "retrieving forms request data.";
            PreparedStatement s=con.prepareStatement("SELECT t3.* FROM FORMS_2_ME T3, SIEBEL.S_ADDR_ORG T1, SIEBEL.S_ORG_EXT T2"
                   + " WHERE T2.NAME = T3.ACCOUNT_NO(+)"  
                   + " AND T1.ROW_ID = T2.PR_ADDR_ID" 
                   + " AND T2.BU_ID = '1-1RZ'"  // verify account is IIT and not BIZ 
                   + " AND t2.NAME = ?"
                   + " ORDER BY t3.created_date DESC");
            s.setString(1, ssn);
            ResultSet rs = s.executeQuery();
            Integer delay = 14;
            delay = (delay != null) ? delay : 0; 
            if(rs.next()) {
            	java.sql.Date reqDate = rs.getDate("CREATED_DATE");
            	Calendar cal = Calendar.getInstance();
            	cal.add(Calendar.DAY_OF_MONTH, 0 - delay);
            	if(reqDate == null) {
            		resultPort = REQUEST_VALID;
            	}
            	else if(cal.getTime().before(reqDate)) {
            		resultPort = CALLED_WITHIN_X_DAYS;
            	}
            	else {
            		resultPort = REQUEST_VALID;
            	}
            }
            else {
            	resultPort = NOT_ON_FILE;
            }
          
        } catch(Exception e) {
            resultPort = EXCEPTION;
            setLog(localPageContext, 3, "Error " + locator + " " + e.getMessage());
        } finally {
        	try {con.close();}catch(Exception e){}
        }
		
        localPageContext.setAttribute("nAllowType", new Integer(resultPort), PageContext.PAGE_SCOPE);
	}


	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
	//Use "addStuff" variable for generating custom VXML code. 
	public void processEnd(PageContext localPageContext) {

	}%>
<%@ page import="java.util.*"%>
<%@ page import="org.apache.log4j.Logger"%>
<%@ page import="gov.mi.state.treas.ivr.*"%>
<%@ page import="gov.mi.state.treas.ivr.config.*"%>
<%@ page import="java.sql.*"%>
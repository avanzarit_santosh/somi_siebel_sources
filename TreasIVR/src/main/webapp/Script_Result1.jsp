<?xml version="1.0" encoding="utf-8" ?>
<%@ page language="java" %>
<%@ page session="true" %>
<%@ page contentType="text/xml; charset=utf-8" %>
<%@ page import="java.util.*" %>
<%@ include file="StudioIncludes/pagesupport.inc" %>
<%
    try {
        request.setCharacterEncoding("UTF-8");
    } catch (java.io.UnsupportedEncodingException e) {
        e.printStackTrace();
    }
    if (request.getMethod().equalsIgnoreCase("HEAD")) {
        return;
    }
    String szUserParams = "";
    String szAdapterType = "";
    String szMapMode = "";
    String szTempUserParams = "";
    String xTData = "";
    String xSData = "";
    String szANI = "";
    String szMusicLink = "";
    String szWhisperLink = "";
    String szLegWait = "";
    String szAddStuff = "";
    String szStartPage = "";
    String szBeginMain  = "";
    String szEndMain = "";
    String szException = "";
    int nLegWait = 1;
    int nWhisper = 0;


    session.setAttribute("PAGEID", "Script_Result1");
    java.util.Enumeration params = request.getParameterNames();
    String name;
    String value;
    while (params.hasMoreElements()) {
        name = (String) params.nextElement();
        value = request.getParameter(name);
        session.setAttribute(name, value);
    }

    processBegin(pageContext);
    

	szAdapterType ="ICM";
	szTempUserParams = "";
	nLegWait =1;
	nWhisper = 0;
	szMapMode = "NONE";

	
	String[] xTypes = null;
	String[] xHRefs = null;
	

    if (szMapMode.length() == 0) {
        szMapMode = "NOMAP";
    }

    if (szAdapterType.equals("ICM")) {
        if (szMapMode.equals("MAP")) {
            szUserParams += "ICMVarMappingMode:MAP_SPECIFICVARS,";
        } else {
            szUserParams += "ICMVarMappingMode:MAP_INORDER,";
        }
    }

    //************************
    //Get the music and Agent
    //data
    //************************
    int intIndex;
    if (xTypes != null) {
        for (intIndex = 0; intIndex < xTypes.length; intIndex++) {
            if (xTypes[intIndex].equals("Caller_before_Transfer")) {
                szMusicLink = xHRefs[intIndex];
            } else if (xTypes[intIndex].equals("Whisper_to_Agent")) {
                szWhisperLink = xHRefs[intIndex];
            }
        }
    }

    //************************
    //Get the user parameters,
    //if there are any
    //***********************
    if ((szTempUserParams != null) && (szTempUserParams.length() > 0)) {
        StringTokenizer st = new StringTokenizer(szTempUserParams, ",");
        while (st.hasMoreTokens()) {

            if (szAdapterType.equals("ICM")) {
                if (szMapMode.equals("MAP")) {
                    xTData = st.nextToken();

                    intIndex = xTData.indexOf(":");
                    if (intIndex != -1) {
                        //mapping ICM mode is used.
                        if (xTData.substring(0, intIndex).equals("ECC")) {
                            szUserParams += queueAdapterEncode(xTData.substring(0, intIndex));
                            szUserParams += ".";
                            szUserParams += queueAdapterEncode(xTData.substring(intIndex+1));
                            szUserParams += ":";
                            szUserParams += queueAdapterEncode(lookUp(pageContext, xTData.substring(intIndex+1)));
                        } else {
                            szUserParams += queueAdapterEncode(xTData.substring(0, intIndex));
                            szUserParams += ":";
                            szUserParams += queueAdapterEncode(lookUp(pageContext, xTData.substring(intIndex+1)));

                        }
                    }

                } else {
                    //non-mapping ICM mode is used.
                    xTData = st.nextToken();
                    if (xTData.startsWith("ECC")) {
                        szUserParams += queueAdapterEncode(xTData);
                        szUserParams += ":";
                        szUserParams += queueAdapterEncode(lookUp(pageContext, xTData.substring(4)));
                    }else{
                        szUserParams += queueAdapterEncode(xTData);
                        szUserParams += ":";
                        szUserParams += queueAdapterEncode(lookUp(pageContext, xTData));
                    }
                }//end MAP
            } else {
                //case where other adapter is used.
                xTData = st.nextToken();
                szUserParams += queueAdapterEncode(xTData);
                szUserParams += ":";
                szUserParams += queueAdapterEncode(lookUp(pageContext, xTData));
            }//end ICM

            szUserParams += ",";
        }//end while
        szUserParams = szUserParams.substring(0, szUserParams.length() - 1);

    }//end if


    //*********************
    // Check for music node
    //*********************
    if (nLegWait == 0) {
        szStartPage= getStartXmlPageTag(pageContext, szMusicLink);
    } else {
        szStartPage= getStartXmlPageTag(pageContext, "");
    }

    //***********************************
    // This allows user to
    // change the nType/nLegWait/nWhisper
    // dynamically
    //***********************************
    processAnywhere(pageContext);

    //********************
    szAddStuff = "<SET VARNAME=\"$scripturl$\" VALUE=\"$start-ivr-url$\" />";
    szBeginMain = "<SCRIPT_RESULT";
    szEndMain = "</SCRIPT_RESULT>";

    if (szTempUserParams.length() > 0) {
        szBeginMain += " USR_PARAMS=\"";
        szBeginMain += szUserParams;
        szBeginMain += "\"";
    }
    szBeginMain += ">";

    if (nLegWait == 1) {
        szLegWait = "<LEG_WAIT/>";
    }

    szAddStuff += szBeginMain;
    szAddStuff += szEndMain;
    szAddStuff += szLegWait;

    //****************************
    //Now write the response to
    //the client browser
    //****************************
    out.print(szStartPage);
    out.print(szException);
    out.print(szAddStuff);
    out.print("</XMLPage>");

    // ***********************
    // Clean up all the values
    processEnd(pageContext);
%>
     <%!
          // localPageContext - Contains the reference to the Page Context.	  
	  //                    Can be used o Access the HTTP Objects 

	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page starts
            public void processBegin(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed before the vxml page ends
            public void processAnywhere(PageContext localPageContext){

            }
	//Can be used for writing the custom code that needs to get 
	//executed after the vxml page ends.can be used for doing cleanup
            public void processEnd(PageContext localPageContext){

            }
          %>
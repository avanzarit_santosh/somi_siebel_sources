
<%--
*******************************************************************
 FILENAME       :	RecordingSupport Include file

 DESCRIPTION    :	This file has java code methods that are used by
			other jsp files for recording support functions.

 AUTHORS		:		1.	Shantanu Sinha	
 
 METHODS		:		1.	serializeVoxFile
					
 *******************************************************************
 --%>

<%!


/* 
**************************************
serializeVoxFile
***************************************
*/

   String serializeVoxFile(PageContext pageContext) throws IOException 
    {
        	FileOutputStream out;
        	ServletInputStream in;
        	int j;
        	HttpServletRequest request =(HttpServletRequest) pageContext.getRequest();
		String filename = "";
		String nextblockname = "";
		String newGCTname = "";
		String GVPfilename = "";
		String GVPpath = "";
		String strResult= "";
		String strNextBlockPresent = "FALSE";
		String studioFileName ="";
		String shadowRecordDuration = "";
		String shadowRecordsize = "";
		String shadowRecordMaxtime = "";
		String shadowRecordTermchar = "";
		byte b[] = new byte[4096];
		int len = -1;
		ByteArrayOutputStream bstream = new ByteArrayOutputStream();
 
        
		in = request.getInputStream();
            while ((len=in.readLine(b, 0, 4096)) != -1)
            	bstream.write(b, 0, len);
            
            byte bcontent[] = bstream.toByteArray();
            bstream.close();
            
            String strContent = new String(bcontent);
            String matchPattern;
            String NmatchPattern;
            String endPattern; 
            String contentType = "" + request.getHeader ("Content-type");
            String boundary = "--" + contentType.substring (contentType.indexOf ("boundary=\"")+10, contentType.length()-2);
            int startIndex = 0;
            int GStartIdx = 0;
            int GEndIdx = 0;
            
		// get the filename - assumption there is only one text data (filename) as part of the multi-part post
		matchPattern = "Content-Type: text/plain"; 
		int fStartIdx = strContent.indexOf (matchPattern) + matchPattern.length() + 4;
		int fEndIdx = strContent.indexOf (boundary, fStartIdx) - 2;

		// retrieve the path
		filename = strContent.substring(fStartIdx, fEndIdx);
	//	studioFileName = filename;
	//	int fLastIdx = studioFileName.lastIndexOf("\\") + 1;
	//	studioFileName =  studioFileName.substring(fLastIdx,studioFileName.length());
		
		//Get next block name if it presents in the content
		
		NmatchPattern = "name=\"NextBlockName\"";
		if (strContent.indexOf (NmatchPattern) != -1)
		{
		int nStartIdx = strContent.indexOf (NmatchPattern) + NmatchPattern.length() + 30;
		int nEndIdx = strContent.indexOf (boundary, nStartIdx) - 2;
		nextblockname = strContent.substring(nStartIdx, nEndIdx);
		nextblockname = nextblockname + "++" ;
		strNextBlockPresent = "TRUE";
	
		// get new gct name if exists in the content
		
		matchPattern = "name=\"NewGCTName\"";
		if (strContent.indexOf (matchPattern) != -1){
		GStartIdx = strContent.indexOf (matchPattern) + matchPattern.length() + 30;
		GEndIdx = strContent.indexOf (boundary, GStartIdx) - 2;
		newGCTname = strContent.substring(GStartIdx, GEndIdx);
		newGCTname = newGCTname + "##";
		}
		
		// get the shadow variables if present in the list
		GStartIdx = 0;
		GEndIdx = 0;
		matchPattern = "$.duration\"";
		if (strContent.indexOf (matchPattern) != -1){
		GStartIdx = strContent.indexOf (matchPattern) + matchPattern.length() + 30;
		GEndIdx = strContent.indexOf (boundary, GStartIdx) - 2;
		shadowRecordDuration = strContent.substring(GStartIdx, GEndIdx);
		shadowRecordDuration = shadowRecordDuration + "**";
		}
		
		// get the shadow variables if present in the list
		GStartIdx = 0;
		GEndIdx = 0;
		matchPattern = "$.size\"";
		if (strContent.indexOf (matchPattern) != -1){
		GStartIdx = strContent.indexOf (matchPattern) + matchPattern.length() + 30;
		GEndIdx = strContent.indexOf (boundary, GStartIdx) - 2;
		shadowRecordsize = strContent.substring(GStartIdx, GEndIdx);
		shadowRecordsize = shadowRecordsize + "&&";
		}
		
		// get the shadow variables if present in the list
		GStartIdx = 0;
		GEndIdx = 0;
		matchPattern = "$.termchar\"";
		if (strContent.indexOf (matchPattern) != -1){
		GStartIdx = strContent.indexOf (matchPattern) + matchPattern.length() + 30;
		GEndIdx = strContent.indexOf (boundary, GStartIdx) - 2;
		shadowRecordTermchar = strContent.substring(GStartIdx, GEndIdx);
		shadowRecordTermchar = shadowRecordTermchar + "$$";
		}
		
		// get the shadow variables if present in the list
		GStartIdx = 0;
		GEndIdx = 0;
		matchPattern = "$.maxtime\"";
		if (strContent.indexOf (matchPattern) != -1){
		GStartIdx = strContent.indexOf (matchPattern) + matchPattern.length() + 30;
		GEndIdx = strContent.indexOf (boundary, GStartIdx) - 2;
		shadowRecordMaxtime = strContent.substring(GStartIdx, GEndIdx);
		shadowRecordMaxtime = shadowRecordMaxtime + "--";
		}
		
		
		
		}
		// Get the GVP file name if it presents in the content
		
		matchPattern = "filename=\"";
		endPattern = "Content-Type: application/octet-stream";
		if (strContent.indexOf (matchPattern) != -1){
		int GfStartIdx = strContent.indexOf (matchPattern) + matchPattern.length();
		int GfEndIdx = strContent.indexOf (endPattern, GfStartIdx) - 3;
		GVPpath = strContent.substring(GfStartIdx, GfEndIdx);
		
		if(GVPpath.indexOf("\\") != -1){
			GVPfilename = GVPpath.substring(GVPpath.lastIndexOf("\\") + 1);
		}
		else{
			GVPfilename = GVPpath.substring(GVPpath.lastIndexOf("/") + 1);
		}
		}
		
		if (strNextBlockPresent == "TRUE")
		{
		strResult = nextblockname + newGCTname + shadowRecordDuration + shadowRecordsize +  shadowRecordTermchar + shadowRecordMaxtime + GVPfilename ;
		}
		else
		{
		strResult = filename + "%%" + GVPpath ;
		}
		
	
			
		// add filename to the localPageContext
		pageContext.setAttribute("szRecordingFileName", filename);


		// get the position of the binary data - for the vox file content
		matchPattern = "Content-Transfer-Encoding: binary";
            int idx = strContent.indexOf(matchPattern);
            int endIndex = bcontent.length;
            if (idx != -1) 
			startIndex = idx + matchPattern.length() + 4;
		

		// write the actual vox file		
        	try {
            	out = new FileOutputStream(filename);
        	} catch (FileNotFoundException e) {
        	   	// This exception is typically thrown if intermediate
            	// directories are non-existant, so we attempt to
           		// create those directories.
            	File file = new File(filename);
            	File parent = new File(file.getParent());
            	parent.mkdirs();
            	out = new FileOutputStream(filename);
        	}
		out.write(bcontent, startIndex, endIndex - startIndex - boundary.length());	
        	out.close();
        	
        	
        	return  strResult;
   }


/* 
**************************************
getNewVoxFileName
***************************************
*/


    String getNewVoxFileName(PageContext pageContext,
                             String directory, String defLocation, String RecFormat, String filename, String fileNamePrefix)
                             throws IOException 
    {
        HttpServletRequest request =
        (HttpServletRequest) pageContext.getRequest();
        String mimeType;
        String extension;
        String constWAV ="wav";
		int len;
        // If no directory name is provided, use a special subdirectory
        // of the application home directory
        if (directory == null || "".equals(directory)) {
            File servletPath =
            new File(request.getRealPath(request.getServletPath()));
            directory = buildPath(servletPath.getParent(), defLocation);
        }

        len = RecFormat.length(); 
        if (RecFormat.indexOf (constWAV) != -1)
        {
			extension = "wav";
	}
		
		else if (RecFormat.equals("audio/x-alaw-basic")){
		   		extension = "alaw";
		     }
		else if (RecFormat.equals("audio/L16")){
		   		extension = "pcm16";
		     }     
		else if (RecFormat.equals("audio/g729")){
				extension = "g729";
		     }
		else if (RecFormat.equals("audio/x-gsm")){
		   		extension = "gsm";
		     }
		     
		else if (RecFormat.equals("audio/x-g726-24")){
		   		extension = "adpcm24";
		     }     
		else if (RecFormat.equals("audio/x-g726-40")){
		   		extension = "adpcm40";
		     }     
		else if (RecFormat.equals("audio/L8")){
		   		extension = "pcm8";
		     }     
		else
		{
			extension = "vox";
		}
         

        if (filename == null || "".equals(filename)) {
            // If no filename is given, generate
            // a random file name based on the sessionid or date.
            Random generator = new Random();
            String sessionid = request.getParameter("SESSIONID");
            String prefix = null;

            java.text.DecimalFormat df10 =
                new java.text.DecimalFormat("0000000000");

            if (sessionid != null && !sessionid.equals("") &&
                !sessionid.equals("$sessionid$")) {
                prefix = sessionid;
            } else {
                java.text.DecimalFormat df2 =
                new java.text.DecimalFormat("00");
                java.text.DecimalFormat df4 =
                new java.text.DecimalFormat("0000");
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());

                prefix = "recording" + "-";
                prefix += df4.format(cal.get(Calendar.YEAR));
                prefix += "-";
                prefix += df2.format(cal.get(Calendar.MONTH) + 1);
                prefix += "-";
                prefix += df2.format(cal.get(Calendar.DAY_OF_MONTH));
                prefix += "-";
                prefix += df2.format(cal.get(Calendar.HOUR_OF_DAY));
                prefix += "-";
                prefix += df2.format(cal.get(Calendar.MINUTE));
                prefix += "-";
                prefix += df2.format(cal.get(Calendar.SECOND));
                prefix += "-";
                prefix += df2.format(cal.get(Calendar.MILLISECOND));
            }
            filename = prefix;
            filename += "-";
            filename += df10.format(Math.abs(generator.nextInt()));
        }

	   if (fileNamePrefix == null)
			fileNamePrefix = "";
			
       return buildPath(directory, fileNamePrefix + filename + "." + extension);

    }// end of getNewVoxFileName
    

/* 
**************************************
buildPath
***************************************
*/

    String buildPath(String basePath, String newpath) 
    {
        StringBuffer path = new StringBuffer();
        int index;

        if (basePath != null) {
            path.append(basePath);
            if (!"".equals(basePath)) {
                if (basePath.charAt(basePath.length()-1) !=
                    java.io.File.separatorChar) {
                    path.append(java.io.File.separatorChar);
                }
            }
        }

        path.append(newpath);

        return path.toString();
    } //end of buildPath

%>


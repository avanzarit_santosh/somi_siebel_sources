/* 
 * Common.js
 *
 * JavaScript functions for common functions needed in VoiceXML on
 * the Genesys Platform
 *
 * Copyright � 2007 Genesys Telecommunications
 */

function getsize(infoArr)
{
	return infoArr.length;
}
					
function getheader(infoArr)
{
	var i;
	var header = "";
	for (i = 0; i < getsize(infoArr.header); i++)
		header += infoArr.header[i] + "\n";

	return header;
}

function getcontenttype(infoArr)
{
	var i;
	var contenttype = "";
	var contenttypeheader = "Content-Type: ";

	for (i = 0; i < getsize(infoArr.header); i++)
	{							
		contenttype = infoArr.header[i];

		if (contenttype.indexOf(contenttypeheader) == 0)
		{
			contenttype = contenttype.substr(contenttypeheader.length);
			break;
		}
	}

	return contenttype;
}

function replaceXMLChars(message)
{
	var msgStr = new String(message);
	var regExp = new RegExp("[<>&]", "g");
	return msgStr.replace(regExp, " ");
}

function getSipHeaderValue(name)

{
           var x;
           try
           {

                       return session.connection.protocol.sip.headers[name];
           }
           catch(x)
           {

                       return undefined;
           }

}

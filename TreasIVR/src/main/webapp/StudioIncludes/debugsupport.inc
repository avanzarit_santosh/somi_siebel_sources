<%@ page import="java.util.Enumeration, java.io.OutputStreamWriter, java.net.UnknownServiceException" %>
<%!
/*
 *******************************************************************
 FILENAME      : debugsupport.inc

 DESCRIPTION   : This is only used for real-time debugging
                            in-conjuction with CodeTracer.

 AUTHOR(S)    :    1. Raja Cherukuri 
                   2. Mamata

 FUNCTION(S)  :
                 1) DebugNow  
 *******************************************************************
*/

String DebugNow(PageContext localPageContext, String fileName, String language) {
    
     String vxml = "";
     
        
    HttpSession session = localPageContext.getSession();


    // Check if this has a debug node.
    String rootDocument = (String)session.getAttribute("ROOTDOCUMENT");
    if (rootDocument != null && !"".equals(rootDocument)) {
        if (rootDocument.indexOf(fileName) < 0) {
            fileName += ".jsp";

          
            int i = CodeTracer.CodeTracerUtil.IsDebug(fileName);
	    if (i == 1) {
                //Now create the dummy VoiceXML file...
                //which keeps the platform busy while
                //the user checks call variables and 
                //other stuff.
                String rtDoc = lookUp(localPageContext, "ROOTDOCUMENT");
                vxml = "<vxml version='2.0' xmlns='http://www.w3.org/2001/vxml' application='" + rtDoc + "'>\n";
                vxml += "<form>\n";
                vxml += "<property name='com.telera.speechenabled' value='false'/>\n";
                vxml += "<field>\n";
                vxml += "<grammar mode='dtmf' version='1.0' root='fundX' type='application/srgs+xml' xml:lang='";
                vxml += language;
                vxml += "'>\n";
                vxml += "<rule id='fundX'><one-of><item>0 0 0 0 0 0 1</item></one-of></rule></grammar>\n";
                vxml += "<prompt timeout='3s'>\n";
                vxml += "<break time='500ms'/>\n";
                vxml += "</prompt>\n";
                vxml += "</field>\n";
                vxml += "<catch event='nomatch noinput'>\n";
                vxml += "<goto next='" + fileName + "'/>\n";
                vxml += "</catch>\n";
                vxml += "</form>\n";
                vxml += "</vxml>\n";
                
                // ****************************************************
                // Now while this is going on...check if it is only the
                // first time we are entering the session and only 
                // send the list of session variables to CodeTracer.
                // (session id RTDebug is empty, signifies first time )
                // ****************************************************
                String rtdebug = (String)session.getAttribute("RTDEBUG");

                if (rtdebug == null || "".equals(rtdebug)) {
                    session.setAttribute("RTDEBUG", "YES");
                    //  now list out all the variables
                    //  and send them to CodeTracer for
                    //  identification.

                    Enumeration names = session.getAttributeNames();

                    while(names.hasMoreElements()) {
                        String name = (String)names.nextElement();
                        Object value = session.getAttribute(name);

                        if (value instanceof String && 
                            !name.equals("RTDEBUG")) {
                            String strValue = (String)value;
                            String strVarVal = (String)name + "," + strValue;
			                                
                            CodeTracer.CodeTracerUtil.AddSessionVariable(strVarVal);
                            
                        }
                    }
                }
                else
                {
                    // now list out all the variables
                    // and get them from CodeTracer the new values.

                    Enumeration names = session.getAttributeNames();

                    while(names.hasMoreElements()) {
                        String name = (String)names.nextElement();

                        if (!name.equals("RTDEBUG")) {
                            String value = CodeTracer.CodeTracerUtil.GetSessionVariable(name);
                            session.setAttribute(name, value);
                        }
                    }
	
                }
            } else {
                // This means that this is not a debug mode...
                 //  so do some other magic.
                //  dim rootDoc1
                 // rootDoc1 = SESSION("ROOTDOCUMENT")
                if (rootDocument.indexOf(fileName) < 0) {
                    session.setAttribute("RTDEBUG", "");
                }

            }  
        }
    }
    
    return vxml;
}

%>

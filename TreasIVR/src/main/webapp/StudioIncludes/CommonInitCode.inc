<%
    try 
    {
        request.setCharacterEncoding("UTF-8");
    } 
    catch (java.io.UnsupportedEncodingException e) 
    {
        e.printStackTrace();
    }
    
    if (request.getMethod().equalsIgnoreCase("HEAD")) 
    {
        return;
    }

    java.util.Enumeration paramsInQuery = request.getParameterNames();
    String nameInQuery;
    String valueInQuery;

    while (paramsInQuery.hasMoreElements()) 
    {
        nameInQuery = (String) paramsInQuery.nextElement();
        valueInQuery = request.getParameter(nameInQuery);
        session.setAttribute(nameInQuery, valueInQuery); 
    }
    
    processBegin(pageContext);  
%>
# Treasury Individual Income Tax (IIT) IVR

|      |                   Test                   |                                     Production                                     |
| ---- | ---------------------------------------- | ---------------------------------------------------------------------------------- |
| Phone | 636-6207| 636-4486|
| IVR Servers  | [HCS084GRPTPA902](rdp://hcs271gnvpta001) | [HCS271GNVPPA001](rdp://HCS271GNVPPA001)<br>[HCS271GNVPPA002](rdp://HCS271GNVPPA002) |
| GVP Servers  |                                          | [HCS271GNSYPA001](rdp://HCS271GNSYPA001)<br>[HCS271GNSYPA002](rdp://HCS271GNSYPA002) |
| IVR Apps | [Test START.jsp](http://hcs271gnvpta001:8080/iit-ivr/START.jsp)|[Prod1 START.jsp](http://HCS271GNVPPA001:8080/iit-ivr/START.jsp)<br>[Prod2 START.jsp](http://HCS271GNVPPA002:8080/iit-ivr/START.jsp)  |
| Tomcat| [Manager](http://hcs271gnvpta001:8080/manager/html)|[Prod1 Manager](http://HCS271GNVPPA001:8080/manager/html)<br>[Prod2 Manager](http://HCS271GNVPPA002:8080/manager/html)  |
| EMPS |[EMPS](http://hcs271gnvpta001.som.ad.state.mi.us:9810/spm/login.php)|[EMPS Prod1](http://hcs271gnvppa001.som.ad.state.mi.us:9810/spm/login.php)<br>[EMPS Prod2](http://hcs271gnvppa002.som.ad.state.mi.us:9810/spm/login.php) |

## PROD
* Phone Number: 636-4486
* [IVR Admin WebUI](https://hcs271gnvpta001.som.ad.state.mi.us:8443/prod-ivr-admin/)
* [Emperix Voice Watch](https://services.empirix.com/dashboard)
* IVR Apps
  * http://HCS271GNVPPA001.som.ad.state.mi.us:8080/iit-ivr/START.jsp
  * http://HCS271GNVPPA002.som.ad.state.mi.us:8080/iit-ivr/START.jsp
* EMPS
  * http://hcs271gnvppa001.som.ad.state.mi.us:9810/spm/login.php
  * http://hcs271gnvppa002.som.ad.state.mi.us:9810/spm/login.php
* IVR Servers
  * [HCS271GNVPPA001](rdp://HCS271GNVPPA001)
  * [HCS271GNVPPA002](rdp://HCS271GNVPPA002)
* GVP Servers
  * [HCS271GNSYPA001](rdp://HCS271GNSYPA001)
  * [HCS271GNSYPA002](rdp://HCS271GNSYPA002)

## TEST
* Phone Number: 636-6207
* [hcs271gnvpta001](rdp://hcs271gnvpta001)
  * IVR Server - http://hcs271gnvpta001:8080/iit-ivr-2.0-test/START.jsp
  * EMPS - http://hcs271gnvpta001.som.ad.state.mi.us:9810/spm/login.php


  
